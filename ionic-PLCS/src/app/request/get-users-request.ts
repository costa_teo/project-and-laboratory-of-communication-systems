import { ROLES } from '../shared/constants';

export class GetUserRequest {
    
    public role: ROLES
    public page: number
    public userEmail: string

    constructor() {
        this.role = undefined
        this.page = undefined
        this.userEmail = undefined
    }
}