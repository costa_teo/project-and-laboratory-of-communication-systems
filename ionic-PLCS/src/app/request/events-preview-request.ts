export class EventsPreviewRequest{

    public city: string
    public date: string
    public page: number
    public eventName:string

    constructor(){
        this.city = ""
        this.date = ""
        this.page = 0
    }


}