import { Location } from 'src/app/entities/location';


export class ManagerLocationRequest {
    location: Location
    managerId: number

    constructor() {
    }
}