import { EventSlot } from '../entities/event-slot';
import { MessageComponent, MESSAGE_TYPE } from '../shared/components/message/message.component';

export class AddNewEventRequest {
    public id: number;
    public title: string;
    public titolo: string;
    public description: string;
    public descrizione: string;
    public city: string
    public locationName: string
    public address: string
    public eventSlots: Array<EventSlot>;

    constructor() {
        this.id = undefined;
        this.title = "";
        this.description = "";      
        this.eventSlots = [];
        this.city = ""
        this.locationName = ""
        this.address = ""
        this.descrizione = ""
        this.titolo = ""
    }



    
    static validEvent(newEvent: AddNewEventRequest, childMessage: MessageComponent): boolean {
        if (newEvent.title == "" || newEvent.title == undefined) {
          childMessage.showMessage(10, MESSAGE_TYPE.ERROR)
        } else if (newEvent.description == "" || newEvent.description == undefined) {
          childMessage.showMessage(11, MESSAGE_TYPE.ERROR)
        } else if (newEvent.titolo == "" || newEvent.titolo == undefined) {
            childMessage.showMessage(10, MESSAGE_TYPE.ERROR)
        } else if (newEvent.descrizione == "" || newEvent.descrizione == undefined) {
            childMessage.showMessage(11, MESSAGE_TYPE.ERROR)
        } else if (newEvent.city == "" || newEvent.city == undefined) {
          childMessage.showMessage(14, MESSAGE_TYPE.ERROR)
        } else if (newEvent.locationName == "" || newEvent.locationName == undefined) {
          childMessage.showMessage(15, MESSAGE_TYPE.ERROR)
        } else if (newEvent.address == "" || newEvent.address == undefined) {
          childMessage.showMessage(16, MESSAGE_TYPE.ERROR)
        } else {
          return true
        }
        return false
      }
}