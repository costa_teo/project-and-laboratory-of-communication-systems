import { DatePipe } from '@angular/common';
import { Routes } from '../core/routes';


export class ROLES {
    static CUSTOMER: string = "CUSTOMER";
    static MANAGER: string = "MANAGER";
    static ADMINISTRATOR: string = "ADMINISTRATOR";
    static TOTEM: string = "TOTEM";
}



export class CONSTANTS {
  static dateFromString(date: string): Date {
    console.log(date)
    var date1 = date.split('/')
    if(date1.length == 1)
      return new Date(date)
    return new Date(parseInt(date1[2]), parseInt(date1[1])-1, parseInt(date1[0]));

}

  static getTermLink(currentLang: string): string {
    let it =  Routes.baseURL + "/documents/it/terms.pdf"
    let en =  Routes.baseURL + "/documents/en/terms.pdf"

      switch (currentLang) {
        case 'it':
          return it
      
        case 'en':
          return en
      
        default:
          return null
      }
  }

  static getTermOfUseLink(currentLang: string): string {
    let it =  Routes.baseURL + "/documents/it/termsOfUse.pdf"
    let en =  Routes.baseURL + "/documents/en/termsOfUse.pdf"

      switch (currentLang) {
        case 'it':
          return it
      
        case 'en':
          return en
      
        default:
          return null
      }
  }
  
    static days = ["Mon", "Thu", "Wed", "Tus", "Fri"]
    
    static validHours = [
      "08:00",
      "09:00",
      "10:00",
      "11:00",
      "12:00",
      "13:00",
    ]

    static SUBJECT_COLORS = [
        "#c70000",
        "#c77100",
        "#c7ba00",
        "#88c700",
        "#35c700",
        "#00c799",
        "#009cc7",
        "#009cc7",
        "#1b00c7",
        "#7b00c7",
        "#c7008f",
      ]
  static TIMETABLE_TEMPLATE_URL: string = "timetable-template.xlsx";
  static CLASS_COMPOSITION_TEMPLATE_URL: string = "class-composition-template.xlsx"; 

    static convertDateToServer(date: any): string {
      
      try {
          var datePipe = new DatePipe("en-UK")
          return datePipe.transform(date, 'yyyy/MM/dd')
      } catch (error) {
        try {
          var date1 = date.split('/')
          return date1[2] + '/' +date1[1] +'/' +date1[0]; 
          
        } catch (error) {
          return ""
        }{
        }
      }
    }

    static convertDateToGUI(date: string): string {
      try {
        var datePipe = new DatePipe("en-UK")
        return datePipe.transform(date, 'dd/MM/yyyy')
    } catch (error) {
      return ""
    }
  }

  static makeCopy<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj))
  }

  static validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


static delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

}


