import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core';
import { ServiceBoxData } from './service-box-data';

@Component({
    selector: 'service-box',
    templateUrl: './service-box.component.html',
    styleUrls: ['./service-box.component.scss']
})
export class ServiceBoxComponent implements OnInit {

    @Input() services: Array<ServiceBoxData>;
    @Output() onClick: EventEmitter<string> = new EventEmitter<string>();

    ngOnInit(): void {
        //
    }

    selectService(route: string) {
        this.onClick.emit(route);
    }
}