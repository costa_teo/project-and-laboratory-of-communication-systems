import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output() private onConfirm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() private close: EventEmitter<boolean> = new EventEmitter<boolean>();

  bodyMessage: string;
  title: string;
  object: any;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
  }

  show(title: number, qrCode: string, object=null) {
    this.bodyMessage = qrCode;
    this.title = "modal." +  title.toString()
    this.object = object
    $("#my-modal").modal('show');
  }

  doConfirm() {
    this.onConfirm.emit(true);
  }

  doClose(){
    this.close.emit(true);
  }

}
