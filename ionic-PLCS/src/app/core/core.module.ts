import { NgModule } from '@angular/core';
import { NavbarComponent } from './navbar/navbar.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ScrollTop } from './scroll-top/scroll-top';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        NavbarComponent,
        ScrollTop,
    ],
    exports: [
        NavbarComponent,
        ScrollTop
    ],
    imports: [
        CommonModule,
        FormsModule,
        
        BrowserModule,
        HttpClientModule,
        TranslateModule
    ],
    providers: []
})
export class CoreModule { }
