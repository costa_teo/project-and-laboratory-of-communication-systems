
import { Component, OnInit, Inject, HostListener } from '@angular/core';
@Component({
    selector: 'app-scroll-top',
    templateUrl: './scroll-top.html',
    styleUrls: ['./scroll-top.scss']
})
export class ScrollTop implements OnInit {
    windowScrolled: boolean;
    @HostListener("window:scroll", [])
    onWindowScroll() {
        if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
            this.windowScrolled = true;
        } 
       else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
            this.windowScrolled = false;
        }
    }
    scrollToTop() {
        (function smoothscroll() {
            var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
            if (currentScroll > 0) {
                window.requestAnimationFrame(smoothscroll);
                window.scrollTo(0, currentScroll - (currentScroll / 4));
            }
        })();
    }
    ngOnInit() {}
}