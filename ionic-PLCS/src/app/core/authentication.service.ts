import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../entities/user';
import { Routes } from './routes';
import { Router } from '@angular/router';
import { ROLES } from '../shared/constants';
import { LoginRequest } from '../request/login-request';
import { RegisterRequest } from '../request/register-request';
import { CustomerService } from '../modules/customer/customer.service';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';

@Injectable()
export class AuthenticationService implements HttpInterceptor {

    public redirectToChoosenEvent(id: number) {
        let user: User = JSON.parse(localStorage.getItem('currentUser'))

        if (user && user.role == ROLES.ADMINISTRATOR) {
            this.router.navigateByUrl(Routes.ADMIN_MODIFY_EVENT + "/" + id)
            return
        }
        if (user && user.role == ROLES.MANAGER) {
            this.router.navigateByUrl(Routes.MANAGER_YOUR_EVENTS)
            return
        }

        this.router.navigateByUrl(Routes.CUSTOMER_EVENT_DETAIL + "/" + id)

    }

    private currentUserSubject: BehaviorSubject<User>;
    private baseURL: string;

    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
        private router: Router,
        private customerService: CustomerService,
        private translate: TranslateService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();

        this.baseURL = Routes.baseURL + '/authentication';
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(request: LoginRequest): Observable<User> {
        return this.http.post<User>(this.baseURL + '/login', request);
    }

    isTotem():boolean{
        return this.currentUserValue && this.currentUserValue.role == ROLES.TOTEM
    }

    
    saveUser(user: User) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        this.redirectToUserHome();
    }

    logout() {

        let user = this.currentUserSubject.value

        this.customerService.saveOfflineTickets([])
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);

        if (user && user.token)
            this.http.post(this.baseURL + '/logout', user, { observe: 'response' }).subscribe(
                ok => {
                },
                err => {
                }
            );
    }

    authenticateUser(): Observable<number> {
        return this.http.get<number>(this.baseURL + '/authenticateUser');
    }


    /**
     * 
     * Do NOT call this method, internal only
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let user: User = JSON.parse(localStorage.getItem('currentUser'))
        let token = ""
        if (user)
            token = user.token


        request = request.clone(
            {
                setHeaders: {
                    token: token,
                    "language": JSON.parse(localStorage.getItem('language')),
                }
            }
        );


        return next.handle(request);
    }

    redirectToUserHome() {
        let user: User = JSON.parse(localStorage.getItem('currentUser'))
        if (!user) {
            this.router.navigateByUrl(Routes.CUSTOMER_EVENTS);
            return
        }


        let role = user.role;
        if (role == ROLES.CUSTOMER) {
            this.router.navigateByUrl(Routes.CUSTOMER_EVENTS);
        } else if (role == ROLES.MANAGER) {
            this.router.navigateByUrl(Routes.MANAGER_YOUR_EVENTS);
        } else if (role == ROLES.ADMINISTRATOR) {
            this.router.navigateByUrl(Routes.ADMIN_EVENTS);

        } else {
            this.router.navigateByUrl(Routes.CUSTOMER_EVENTS);
        }
    }

    public changePassword(newPassword: string): Observable<any> {
        return this.http.post(this.baseURL + '/change-password', newPassword, { observe: 'response' })
    }

    public redirectToLogin(eventSlotId: number = null) {
        this.logout()
        this.router.navigateByUrl(Routes.HOME_LOGIN + "/" + (eventSlotId == null ? '' : eventSlotId));
    }

    public redirectToRegister() {
        this.router.navigateByUrl(Routes.REGISTER);
    }

    public forgotPassword(email: string) {
        return this.http.post(this.baseURL+ '/fotgot-password', email, {observe: 'response'})
    }
    
    public checkEmailAlreadyUsed(email: string) {
        return this.http.post(this.baseURL+ '/check-email', email)
    }
    


}