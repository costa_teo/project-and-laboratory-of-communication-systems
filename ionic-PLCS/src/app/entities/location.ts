export class Location {
    
    public city: string
    public locationName: string
    public address: string
    public valid: boolean

    constructor() {
        
    }
}