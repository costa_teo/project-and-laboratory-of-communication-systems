import { ROLES } from '../shared/constants';

export class User {
    
    public id: number;
    public name: string;
    public surname: string;
    public cellNo: string;
    public role: ROLES;
    public email: string;
    public token: string;

    constructor(){
        this.id = undefined;
        this.name = "";
        this.surname = "";
        this.cellNo = "";
        this.role = "";
        this.email = "";
        this.token = "";
    }

}