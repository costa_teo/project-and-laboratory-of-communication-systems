export class EventSlot {

    public id: number
    public date: string;
    public startTime: string;
    public endTime: string;
    public totalTickets: number
    public remaining: number
    public valid: Boolean = true

    constructor() {
        this.id = undefined      
        this.date = ""
        this.startTime = ""
        this.endTime = ""
        this.totalTickets = undefined
        this.remaining = 0
    }

}