import { Time } from './time'
import { WeekDay } from '@angular/common'
import { EventSlot } from './event-slot'
import { CONSTANTS } from '../shared/constants'

export class EventDate {
    
    date: string
    startTime: string
    numberOfSlots: number
    slotDuration: string
    totalTickets: number

    repeatedDays : Set<WeekDay>
    numberOfRepetition: number
    
    constructor() {
     this.numberOfRepetition = 0
     this.repeatedDays = new Set()    
    }

    public getEventSlots(): Array<EventSlot> {
        let eventSlots: Array<EventSlot> = []
        
        let eventDates: Array<EventDate> = this.getEventSingleDates()
        eventDates.forEach(date => {
      
            let i = date.numberOfSlots
      
            let slotStartTime = Time.newTime(String(date.startTime))
            let slotDuration = Time.newTime(String(date.slotDuration))
      
            while (i > 0) {
              i--
      
              let e = new EventSlot
              e.startTime = slotStartTime.toString()
              e.endTime = Time.sum(slotStartTime, slotDuration).toString()
              e.totalTickets = date.totalTickets
              e.date = date.date
              e.valid = true
      
              eventSlots.push(e)
           
              slotStartTime.add(slotDuration)
            }
          })
        return eventSlots
    }

    private getEventSingleDates(): Array<EventDate> {
        
        let startdate: Date = CONSTANTS.dateFromString(this.date)

        let eventdates: Array<EventDate> = []

        if(this.numberOfRepetition <= 0){ // no repetition
            eventdates.push(this)
            return eventdates 
        }

        //has repetition

        let eventDatesTotalNumber = this.repeatedDays.size * this.numberOfRepetition

        while(eventdates.length < eventDatesTotalNumber ){
            if(this.repeatedDays.has(startdate.getDay())){
                let eventDate = new EventDate
                eventDate.date = CONSTANTS.convertDateToServer(startdate)
                eventDate.startTime = this.startTime
                eventDate.numberOfSlots = this.numberOfSlots
                eventDate.slotDuration = this.slotDuration
                eventDate.totalTickets = this.totalTickets
                
                eventdates.push(eventDate)
            }
            startdate.setDate(startdate.getDate() + 1)
        }
        return eventdates
    }

    public toggleDay(dayNumber: number){
        if(this.repeatedDays.has(dayNumber)){
            this.repeatedDays.delete(dayNumber)
        }else{
            this.repeatedDays.add(dayNumber)
        }
    }

    static validDate(date: EventDate): boolean {
        if(date.numberOfRepetition > 0 && date.repeatedDays.size == 0)
            return false

        if(date.numberOfRepetition > 10 )
            return false

        if (date.date !=""
         && date.numberOfSlots && date.numberOfSlots > 0 &&
        (date.slotDuration == "00:15" || date.slotDuration=="00:30" || date.slotDuration=="00:45" || date.slotDuration=="01:00")
        && date.startTime && date.startTime != ""
        && date.totalTickets && date.totalTickets > 0)
        return true
        return false
    }

    
}