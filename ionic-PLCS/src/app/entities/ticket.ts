export class Ticket {
    
    id: number
    valid: boolean
    ticketCode: string 
    
    city: string
    address: string
    eventTitle: string
    evenTitolo: string
    eventId: number
    date: string
    startTime: string
    locationName: string
    endTime: string

    constructor() {
        this. id = 0
        this. valid = false
        this. ticketCode = ""
        this. city = ""
        this. address = ""
        this. eventTitle = ""
        this. eventId = 0
        this. date = ""
        this. startTime = ""
        this. endTime = ""
        this.locationName = ""
        this.evenTitolo = ""
    }
}