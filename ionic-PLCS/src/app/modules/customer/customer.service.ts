import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Routes } from 'src/app/core/routes';
import { EventPreview } from 'src/app/entities/event-preview';
import { Ticket } from 'src/app/entities/ticket';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BookEventTotemRequest } from 'src/app/request/book-event-totem-request';
import { EventSlot } from 'src/app/entities/event-slot';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  
  baseURL: string;
    
  constructor (private router: Router,
    private http: HttpClient){
      this.baseURL = Routes.baseURL + '/user';    
    }
      
  
  public bookEventSlot(eventSlotId: number): Observable<any> {
    return this.http.post(this.baseURL + '/bookEventSlot', eventSlotId, { observe: 'response' }) 
  }
  
  public bookEventSlotNoAccount(eventSlotId: number, data:string): Observable<Ticket> {
    let req = new BookEventTotemRequest()
    req.eventSlotId = eventSlotId
    req.data = data
    
    return this.http.post<Ticket>(this.baseURL + '/bookEventSlotNoAccount', req) 
  }
  
  public getCustomerTickets(): Observable<Array<Ticket>>{
    return this.http.get<Array<Ticket>>(this.baseURL + '/getCustomerTickets') 
  }

  public cancelTicket(ticketId: number) {
    return this.http.post(this.baseURL + '/cancelTicket', ticketId, { observe: 'response' }) 
  }
  
  
  /* no http */
  public saveOfflineTickets(tickes: Array<Ticket>) {
    localStorage.removeItem('userTickets');
    localStorage.setItem('userTickets', JSON.stringify(tickes));
  }

  public getOfflineTickets(): Array<Ticket>{
    return JSON.parse(localStorage.getItem('userTickets'))
  }

  public goToEventPreview(event: EventPreview) {
    this.router.navigateByUrl(Routes.CUSTOMER_EVENT_DETAIL + "/" + event.id)
  }
 
  public redirectToEvents() {
    this.router.navigateByUrl(Routes.CUSTOMER_EVENTS)
  }

  public redirectToYourTicket() {
    this.router.navigateByUrl(Routes.CUSTOMER_YOUR_TICKET)
  }

  public redirectToTotemBookEventSlot(eventSlot: EventSlot) {
    this.router.navigateByUrl(Routes.TOTEM_BOOK_EVENT_SLOT + "/" + eventSlot.id)
  }

}

