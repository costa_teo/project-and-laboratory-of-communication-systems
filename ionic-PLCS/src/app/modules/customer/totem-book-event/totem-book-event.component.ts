import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CustomerService } from '../customer.service';
import { CONSTANTS } from 'src/app/shared/constants';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ActivatedRoute } from '@angular/router';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { SharedService } from 'src/app/shared/shared.service';
import { Ticket } from 'src/app/entities/ticket';
import { tick } from '@angular/core/testing';
declare var $: any;


@Component({
  selector: 'app-totem-book-event',
  templateUrl: './totem-book-event.component.html',
  styleUrls: ['./totem-book-event.component.scss'],
})
export class TotemBookEventComponent implements OnInit {

  eventSlotId: number
  loaded: boolean;
  email: any;

  constructor(
    private translate: TranslateService,
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private sharedService: SharedService

  ) { }

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;
  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;
  @ViewChild(ZXingScannerComponent, { static: true })
  scanner: ZXingScannerComponent;



  ngOnInit() {
    this.eventSlotId = Number(this.route.snapshot.paramMap.get('slotId'))
    if (this.eventSlotId == undefined || !Number.isSafeInteger(this.eventSlotId)) {
      this.customerService.redirectToEvents()
      return
    }
    this.loaded = true
  }

  public enableCamera() {

    $('#camera-modal').modal('show')

    this.scanner.tryHarder = true
    this.scanner.autofocusEnabled = true
    this.scanner.enable = true
  }



  public bookEventWithEmail() {
    if (CONSTANTS.validateEmail(this.email)) {
      this.loaded = false
      this.customerService.bookEventSlotNoAccount(this.eventSlotId, this.email).subscribe(ticket => {
        this.loaded = true
        this.messageAndRedirect(203)
      }, erro => {
        console.log(erro)
        this.loaded = true
        this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      })
    } else {
      this.loaded = true
      this.childMessage.showMessage(200, MESSAGE_TYPE.ERROR)
    }
  }

  public bookEventWithoutData() {
    this.loaded = false
    this.customerService.bookEventSlotNoAccount(this.eventSlotId, this.email).subscribe(ticket => {
      this.loaded = true
      this.modal.show(1, ticket.ticketCode, ticket)
      
      

    }, erro => {
      console.log(erro)
      this.loaded = true
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
    })
  }


  public scanSuccessHandler($event) {
    if ($event) {
      this.playAudio()
      $("#camera-modal").modal('hide');
      this.loaded = false
      this.customerService.bookEventSlotNoAccount(this.eventSlotId, $event).subscribe(ticket => {
        this.scanner.enable = false
        this.loaded = true
        this.messageAndRedirect();
      }, err => {
        this.loaded = true
        this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      })
    }
  }







  playAudio() {
    let audio = new Audio();
    audio.src = '../../../../assets/sound/beep.mp3';
    audio.load();
    audio.play();
  }



  public async messageAndRedirect(msg: number = 2) {
    this.childMessage.showMessage(msg, MESSAGE_TYPE.SUCCESS)
    await CONSTANTS.delay(2000)
    this.sharedService.redirectToHome()
  }


}

