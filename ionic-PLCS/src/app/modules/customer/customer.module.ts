import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule }  from '../../shared/shared.module'
import { CustomerRoutingModule } from './customer-routing.module';
import { FormsModule } from '@angular/forms';
import { CustomerYourTicketsComponent } from './customer-your-tickets/customer-your-tickets.component';
import { CustomerEventsComponent } from './customer-events/customer-events.component';
import { CustomerEventDetailComponent } from './customer-event-detail/customer-event-detail.component';
import { CoreModule } from 'src/app/core/core.module';
import { QRCodeModule } from 'angular2-qrcode';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { TotemBookEventComponent } from './totem-book-event/totem-book-event.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';


@NgModule({
  declarations: [
    CustomerYourTicketsComponent,
    CustomerEventsComponent,
    CustomerEventDetailComponent,
    TotemBookEventComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    SharedModule,
    QRCodeModule,
    CustomerRoutingModule,
    ZXingScannerModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule
    
  ]
})
export class CustomerModule { }
