import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/core/auth.guard';

import { Routes, RouterModule } from '@angular/router';
import { CustomerEventsComponent } from './customer-events/customer-events.component';
import { CustomerYourTicketsComponent } from './customer-your-tickets/customer-your-tickets.component';
import { CustomerEventDetailComponent } from './customer-event-detail/customer-event-detail.component';
import { TotemBookEventComponent } from './totem-book-event/totem-book-event.component';

const routes: Routes = [
    {
        path: 'customer', canActivate: [AuthGuard], children: [
            { path: '', redirectTo: 'events', pathMatch: 'full' },
            { path: 'events', component: CustomerEventsComponent },
            { path: 'your-tickets', component: CustomerYourTicketsComponent },
            { path: 'totem-book/:slotId', component: TotemBookEventComponent },
            { path: 'event-detail/:id', component: CustomerEventDetailComponent },
            { path: '**', redirectTo: 'events' }
        ]
    }
];
  

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation:'reload'})],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
