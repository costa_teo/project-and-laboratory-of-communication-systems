import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from '../customer.service';
import { EventDetail } from 'src/app/entities/event-detail';
import { EventPreview } from 'src/app/entities/event-preview';
import { EventSlot } from 'src/app/entities/event-slot';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';
import { CONSTANTS, ROLES } from 'src/app/shared/constants';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { mapToMapExpression } from '@angular/compiler/src/render3/util';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';

@Component({
  selector: 'app-customer-event-detail',
  templateUrl: './customer-event-detail.component.html',
  styleUrls: ['./customer-event-detail.component.scss']
})
export class CustomerEventDetailComponent implements OnInit {

  image = undefined

  loaded = false
  event: EventDetail

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;


  choosenEventId: number
  dates: Array<string>;

  eventSlots: Map<string, Array<EventSlot>>;
  showEventSlots: EventSlot[];
  selectedDate: string;


  constructor(private customerService: CustomerService,
    private authenticationService: AuthenticationService,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private sanitizer: DomSanitizer) { }


  ngOnInit(): void {
    this.choosenEventId = Number(this.route.snapshot.paramMap.get('id'))
    if (this.choosenEventId == undefined || !Number.isSafeInteger(this.choosenEventId)) {
      this.customerService.redirectToEvents()
      return
    }


    this.loadEventDetail()
  }

  public loadEventDetail() {
    //load event details
    this.sharedService.getEventDetail(this.choosenEventId).subscribe(data => {
      this.event = data

      this.sharedService.getEventImage(data.imageId).subscribe(
        image => {
          let objectURL = URL.createObjectURL(image)
          this.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        }
      )

      this.event.eventSlots.forEach(e => { e.date = CONSTANTS.convertDateToGUI(e.date) }) //correct the data format

      this.eventSlots = new Map()

      this.event.eventSlots.forEach(e => {
        if (this.eventSlots.has(e.date)) {
          let a: Array<EventSlot> = this.eventSlots.get(e.date)
          a.push(e)
        } else {
          let a: Array<EventSlot> = []
          a.push(e)
          this.eventSlots.set(e.date, a)
        }
      })


      this.dates = Array.from(this.eventSlots.keys())

      this.loaded = true


      let firstElement: any = $('#collapse0')
      firstElement.collapse({
        toggle: false
      })

    }, err => {
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      this.loaded = true
    })
  }

  public bookEventSlot(eventSlot: EventSlot) {
    if (this.authenticationService.isTotem()) {
      this.bookeventTotem(eventSlot)
      return
    }

    this.loaded = false
    this.authenticationService.authenticateUser().subscribe(d => {
      //the user is authenticatetd
      this.customerService.bookEventSlot(eventSlot.id).subscribe(
        data => {
          this.customerService.redirectToYourTicket()
          this.childMessage.showMessage(9, MESSAGE_TYPE.SUCCESS)

        }, err => {
          this.loaded = true
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
        })

    }, e => {
      //the user is not logged
      this.authenticationService.redirectToLogin(this.choosenEventId)
    })

  }

  public bookeventTotem(eventSlot: EventSlot) {
    this.customerService.redirectToTotemBookEventSlot(eventSlot)
  }

  public showSlots(date: string) {
    this.selectedDate = date
    this.showEventSlots = this.eventSlots.get(date)

    var my_element = document.getElementById("target");
    my_element.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }

}
