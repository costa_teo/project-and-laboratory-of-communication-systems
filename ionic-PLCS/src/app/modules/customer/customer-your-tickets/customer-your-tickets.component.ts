import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Ticket } from 'src/app/entities/ticket';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { HTTP_INTERCEPTORS, HttpParams } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { CONSTANTS } from 'src/app/shared/constants';

@Component({
  selector: 'app-customer-your-tickets',
  templateUrl: './customer-your-tickets.component.html',
  styleUrls: ['./customer-your-tickets.component.scss']
})
export class CustomerYourTicketsComponent implements OnInit {

  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;
  @ViewChild(MessageComponent, { static: true }) childMessage: MessageComponent;


  loaded = true

  offline: boolean

  tickets: Array<Ticket>

  constructor(private customerService: CustomerService,
    private authenticationService: AuthenticationService,
    private translate: TranslateService) { }

  ngOnInit(): void {

    this.loaded = false

    if (!this.authenticationService.currentUserValue)
      this.authenticationService.redirectToLogin()


    this.authenticationService.authenticateUser().subscribe(
      logged => {
        //load my tickets
        this.customerService.getCustomerTickets().subscribe(
          data => {
            this.tickets = data
            
            this.tickets.sort((a,b) =>{
              if(a.valid != b.valid)
                return (a.valid < b.valid)?1:-1
              if(a.date != b.date)
                return (a.date > b.date)?1:-1
              if(a.startTime != b.startTime)
                return (a.startTime > b.startTime)?1:-1
              return 0
            })

            this.tickets.forEach(ticket=>{
              ticket.date = CONSTANTS.convertDateToGUI(ticket.date)
            })

            

            this.customerService.saveOfflineTickets(this.tickets)
            this.loaded = true
          }, err => {
            this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
            this.loaded = true
          })
      }, err => {
        this.loaded = true
        if (err.status == 400 || err.status == 400) { //not logged
          this.authenticationService.redirectToLogin()
        } else {
          this.childMessage.showMessage(3, MESSAGE_TYPE.WARNING)
          this.tickets = this.customerService.getOfflineTickets()
          this.offline = true
        }
      })
  }

  showQr(ticket: Ticket) {
    this.modal.show(1,ticket.ticketCode, ticket )
  }

  cancelTicket(ticket: Ticket) {
    this.loaded = false
    let t = CONSTANTS.makeCopy(ticket)
    t.date = CONSTANTS.convertDateToServer(t.date)
    this.customerService.cancelTicket(t.id).subscribe(ok => {
      this.childMessage.showMessage(20, MESSAGE_TYPE.SUCCESS)
      this.tickets.splice(this.tickets.indexOf(ticket), 1)
      this.loaded = true
    }, err => {
      if(err && err.error && err.error.message)
        this.childMessage.showMessage(err.error.message, MESSAGE_TYPE.ERROR)
      else 
        this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
      this.loaded = true
    })
  }


}
