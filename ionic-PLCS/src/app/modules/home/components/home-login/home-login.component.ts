import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { LoginRequest } from 'src/app/request/login-request';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { CustomerService } from 'src/app/modules/customer/customer.service';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/entities/user';
import { ROLES, CONSTANTS } from 'src/app/shared/constants';

@Component({
  selector: 'app-home-login',
  templateUrl: './home-login.component.html',
  styleUrls: ['./home-login.component.scss']
})
export class HomeLoginComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  public username: string;
  public password: string;
  public loaded: boolean;

  constructor(private authenticationService: AuthenticationService,
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private translate: TranslateService) { }

  ngOnInit() {
    this.loaded = false;

    if (this.authenticationService.currentUserValue) {
      this.authenticationService.redirectToUserHome();
    }
    this.loaded = true;
  }

  login() {

    let loginRequest: LoginRequest = new LoginRequest(this.username, this.password)
    if (this.checkTotem(loginRequest)) {
      this.authenticationService.redirectToUserHome()
      return;
    }


    this.loaded = false;


    this.authenticationService.login(loginRequest).subscribe(
      user => {
        this.authenticationService.saveUser(user);
        let choosenEventId = Number(this.route.snapshot.paramMap.get('id'))

        if (choosenEventId)
          this.authenticationService.redirectToChoosenEvent(choosenEventId)
        else
          this.authenticationService.redirectToUserHome()

      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage(51, MESSAGE_TYPE.ERROR);
      }
    )

  }


  checkTotem(request: LoginRequest): boolean {
    if (request.username == "totem" && request.password == "totem") {
      let user = new User

      user.name = "Totem"
      user.surname = "Totem"
      user.cellNo = "Totem"
      user.id = -1

      user.role = ROLES.TOTEM
      this.authenticationService.saveUser(user)
      return true
    }
    return false
  }





  doRegister() {
    this.authenticationService.redirectToRegister()
  }

  forgotPassword() {
    let email = prompt(this.translate.instant("login.forgotPassPromp").toString(), "email")
    if (CONSTANTS.validateEmail(email) == true) {
      this.authenticationService.forgotPassword(email).subscribe(d => {
        this.childMessage.showMessage(201, MESSAGE_TYPE.SUCCESS)
      }, err => {
        this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      })
    } else {
      this.childMessage.showMessage(200, MESSAGE_TYPE.ERROR)
    }
  }

}
