import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { User } from 'src/app/entities/user';
import { RegisterRequest } from 'src/app/request/register-request';
import { CONSTANTS } from 'src/app/shared/constants';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { SharedService } from 'src/app/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;


  public registerRequest: RegisterRequest
  public birthdate: Date
  public loaded = false;
  public checkbox1: Boolean
  public checkbox2: Boolean



  constructor(private authenticationService: AuthenticationService,
    private sharedService: SharedService,
    private translate: TranslateService) {
    this.registerRequest = new RegisterRequest()

  }

  ngOnInit() {
    this.loaded = false;
    const user: User = this.authenticationService.currentUserValue;

    if (user) {
      this.authenticationService.redirectToUserHome();
    }
    this.loaded = true;
  }

  register() {
    this.loaded = false
    this.sharedService.register(this.registerRequest).subscribe(async data => {
      this.loaded = true
      this.childMessage.showMessage(204, MESSAGE_TYPE.SUCCESS)
      await CONSTANTS.delay(3000)
      this.authenticationService.redirectToLogin();
    }, err => {
      this.loaded = true
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
    })
  }

  checkEmail() {
    if (CONSTANTS.validateEmail(this.registerRequest.email) == true)
      this.authenticationService.checkEmailAlreadyUsed(this.registerRequest.email).subscribe(d => {
      }, err => {
        this.childMessage.showMessage(202, MESSAGE_TYPE.WARNING)
      })
  }

  openTerms(){
    window.open(CONSTANTS.getTermLink(this.translate.currentLang))
  }
  openTermsOfUse(){
    window.open(CONSTANTS.getTermOfUseLink(this.translate.currentLang))

  }

}
