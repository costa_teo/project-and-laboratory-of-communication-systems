import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/auth.guard';
import { ManagerYourEventsComponent } from './components/manager-your-events/manager-your-events.component';
import { ManagerAddEventComponent } from './components/manager-add-event/manager-add-event.component';
import { ManagerModifyEventComponent } from './components/manager-modify-event/manager-modify-event.component';


const routes: Routes = [
  {
      path: 'manager', canActivate: [AuthGuard], children: [
          { path: '', redirectTo: 'your-events', pathMatch: 'full' },
          { path: 'your-events', component: ManagerYourEventsComponent },
          { path: 'add-event', component: ManagerAddEventComponent },
          { path: 'modify-event/:id', component: ManagerModifyEventComponent },
          { path: '**', redirectTo: 'your-events' }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation:'reload'})],
  exports: [RouterModule]
})
export class ManagerRoutingModule { }





