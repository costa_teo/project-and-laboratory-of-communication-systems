import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerYourEventsComponent } from './components/manager-your-events/manager-your-events.component';
import { ManagerAddEventComponent } from './components/manager-add-event/manager-add-event.component';
import { ManagerModifyEventComponent } from './components/manager-modify-event/manager-modify-event.component';
import { ManagerRoutingModule } from './manager-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CoreModule } from 'src/app/core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    ManagerYourEventsComponent, 
    ManagerAddEventComponent, 
    ManagerModifyEventComponent],
  
    imports: [   
    CommonModule,
    SharedModule,
    FormsModule,
    CoreModule,
    ManagerRoutingModule,

    BrowserModule,
    HttpClientModule,
    TranslateModule

  ]
})
export class ManagerModule { }
