import { Injectable } from '@angular/core';
import { EventDetail } from 'src/app/entities/event-detail';
import { AddNewEventRequest } from 'src/app/request/add-new-event-request';
import { EventPreview } from 'src/app/entities/event-preview';
import { Routes } from 'src/app/core/routes';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/entities/user';
import { ROLES, CONSTANTS } from 'src/app/shared/constants';
import { GetUserResponse } from 'src/app/response/get-user-response';
import { GetUserRequest } from 'src/app/request/get-users-request';
import { Location } from 'src/app/entities/location';
import { AddNewLocationRequest } from 'src/app/request/add-new-location-request';
import { ManagerLocationRequest } from 'src/app/request/manager-location-request';
import { GetManagerInfoResponse } from 'src/app/response/get-manager-info-response';

@Injectable({
  providedIn: 'root'
})
export class AdministratorService {
  changeLocationValidity(location: Location) {
    return this.http.post<number>(this.baseURL + "/changeLocationValidity", location)
  }


  baseURL: string;
  choosenEvent: EventPreview;

  constructor(private router: Router,
    private http: HttpClient) {
    this.baseURL = Routes.baseURL + '/administrator';
  }

  public addNewEvent(newEventRequest: AddNewEventRequest): Observable<number> {
    let requ: AddNewEventRequest = CONSTANTS.makeCopy(newEventRequest);
    requ.eventSlots.forEach(e => { e.date = CONSTANTS.convertDateToServer(e.date) })
    return this.http.post<number>(this.baseURL + "/addNewEvent", requ)
  }

  public deleteEvent(event: EventPreview): Observable<any> {
    return this.http.post(this.baseURL + "/deleteEvent", event.id, { observe: 'response' })
  }

  public modifyEvent(newEvent: AddNewEventRequest) {
    newEvent.eventSlots.forEach(e => {
      e.date = CONSTANTS.convertDateToServer(e.date)
    })
    return this.http.post(this.baseURL + "/modifyEvent", newEvent, { observe: 'response' })
  }

  public changeUserRole(user: User): Observable<any> {
    return this.http.post(this.baseURL + "/changeUserRole", user, { observe: 'response' })
  }

  public getUsers(getUserRequest: GetUserRequest): Observable<GetUserResponse> {
    return this.http.post<GetUserResponse>(this.baseURL + "/getUsers", getUserRequest)
  }

  public modifyEventImage(name: string, image: File, eventId: number): Observable<any> {
    let fd: FormData = new FormData();
    fd.append('name', name);
    fd.append('file', image);
    fd.append('eventId', eventId.toString());
    return this.http.post(this.baseURL + "/modifyEventImage", fd, { observe: 'response' })
  }



  addNewLocation(addRequest: AddNewLocationRequest) {
    return this.http.post(this.baseURL + "/addNewLocation", addRequest, { observe: 'response' })
  }



  public removeManagerLocation(location: Location, managerId: number) {
    let req: ManagerLocationRequest = new ManagerLocationRequest()
    req.location = location
    req.managerId = managerId
    return this.http.post(this.baseURL + "/removeManagerLocation", req, { observe: 'response' })
  }

  public addManagerLocation(location: Location, managerId: number) {
    let req: ManagerLocationRequest = new ManagerLocationRequest()
    req.location = location
    req.managerId = managerId
    return this.http.post(this.baseURL + "/addManagerLocation", req, { observe: 'response' })

  }

  public getManagerInfo(managerId: number) {
    let httpParams = { params: new HttpParams().set('managerId', managerId.toString()) };
    return this.http.get<GetManagerInfoResponse>(this.baseURL + "/getManagerInfo", httpParams)
  }

  /* also non valid locations */
  public getLocationsByCity(city: string): Observable<Array<Location>> {
    let httpParams = { params: new HttpParams().set('city', city) };
    return this.http.get<Array<Location>>(this.baseURL + '/getLocationsByCity', httpParams);
  }

  /* also non valid locations */
  public getAllCities(): Observable<Array<string>> {
    return this.http.get<Array<string>>(this.baseURL + '/getAllCities');
  }


  /* valid locations */
  public getAddressesByCityAndLocation(city: string, location: string) {
    let httpParams = {
      params: new HttpParams()
        .set('city', city)
        .set('location', location)
    };
    return this.http.get<Array<string>>(this.baseURL + '/getAddressesByCityAndLocation', httpParams);
  }

  /* valid locations */
  public getLocationNamesByCity(city: string) {
    let httpParams = {
      params: new HttpParams()
        .set('city', city)
    }
    return this.http.get<Array<string>>(this.baseURL + '/getLocationNamesByCity', httpParams);
  }

  /* valid locations */
  public getValidCities(): Observable<Array<string>> {
    return this.http.get<Array<string>>(this.baseURL + '/getValidCities');
  }



}
