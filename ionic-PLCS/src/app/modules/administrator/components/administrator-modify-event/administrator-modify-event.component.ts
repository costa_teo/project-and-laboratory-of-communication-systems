import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { AdministratorService } from '../../administrator.service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { AddNewEventRequest } from 'src/app/request/add-new-event-request';
import { EventSlot } from 'src/app/entities/event-slot';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CONSTANTS } from 'src/app/shared/constants';
import { EventDate } from 'src/app/entities/event-date';
import { Time } from 'src/app/entities/time';
import { EventDetail } from 'src/app/entities/event-detail';

@Component({
  selector: 'app-administrator-modify-event',
  templateUrl: './administrator-modify-event.component.html',
  styleUrls: ['./administrator-modify-event.component.scss']
})
export class AdministratorModifyEventComponent implements OnInit {
  initialCity: string;
  loaded = false
  newEvent = new AddNewEventRequest()

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;
  image: File;

  imageurl: any
  eventSlots: Map<string, Array<EventSlot>>;
  dates: string[];

  eventDates: Array<EventDate> = []
  eventDetail: EventDetail;
  cities: string[];
  locations: string[];
  addresses: string[];
  selectedDate: string;
  showEventSlots: EventSlot[];


  constructor(private administratorService: AdministratorService,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.loadAddNewEventRequest()
  }

  loadCity(initialCity: string, initialLocation: string, initialAddress: string) {


    this.administratorService.getValidCities().subscribe(data => {
      this.cities = data;
      this.administratorService.getLocationNamesByCity(initialCity).subscribe(locations => {
        this.locations = locations
        this.administratorService.getAddressesByCityAndLocation(initialCity, initialLocation).subscribe(addresses => {
          this.addresses = addresses;

          if (!this.cities.includes(initialCity)){
            this.cities.push(initialCity)
            this.initialCity = initialCity
          }
          if (!this.addresses.includes(initialAddress))
            this.addresses.push(initialAddress)
          if (!this.locations.includes(initialLocation))
            this.locations.push(initialLocation)

          this.newEvent.locationName = initialLocation
          this.newEvent.city = initialCity
          this.newEvent.address = initialAddress



        })
      })
    }, err => {
    })

  }

  loadLocations() {
    this.newEvent.address = null
    this.newEvent.locationName = null

    if(this.initialCity){
      this.cities.splice(this.cities.indexOf(this.initialCity), 1)
      this.initialCity = null
    }

    this.locations = null
    this.administratorService.getLocationNamesByCity(this.newEvent.city).subscribe(locations => {
      this.locations = locations
    })
  }

  loadAddresses() {
    this.addresses = null
    this.administratorService.getAddressesByCityAndLocation(this.newEvent.city, this.newEvent.locationName).subscribe(addresses => {
      this.addresses = addresses;
      this.newEvent.address = this.addresses[0]
    })
  }


  loadAddNewEventRequest(loadImage = true) {
    let id = Number(this.route.snapshot.paramMap.get('id'))

    if (Number.isInteger(id)) { //modify an existring one

      this.sharedService.getEventDetail(id).subscribe(
        data => {
          this.eventDetail = data
          this.newEvent.description = data.description
          this.newEvent.eventSlots = data.eventSlots
          this.newEvent.id = data.id
          this.newEvent.title = data.title
          this.newEvent.titolo = data.titolo
          this.newEvent.descrizione = data.descrizione

          this.loadCity(data.city, data.locationName, data.address)

          if (loadImage) {
            this.image = new File([], data.imageId.toString()) //todo better

            this.sharedService.getEventImage(data.imageId).subscribe(blob => {
              let objectURL = URL.createObjectURL(blob)
              this.imageurl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
            }) //dowload image
          }

          //correct dates
          this.newEvent.eventSlots.forEach(s => {
            s.date = CONSTANTS.convertDateToGUI(s.date)
          });

          this.eventSlots = new Map()

          this.newEvent.eventSlots.forEach(e => {
            if (this.eventSlots.has(e.date)) {
              let a: Array<EventSlot> = this.eventSlots.get(e.date)
              a.push(e)
            } else {
              let a: Array<EventSlot> = []
              a.push(e)
              this.eventSlots.set(e.date, a)
            }
          })
          this.dates = Array.from(this.eventSlots.keys())

          this.loaded = true
        }, err => {
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
          this.loaded = true
        })
    } else {
      this.authenticationService.redirectToUserHome()
      this.loaded = true
    }

  }


  modifyEvent() {
    if (this.checkEvent()) {
      if (this.image != null && this.image.size == 0) {
        this.image = null
      }
      this.loaded = false

      let newEventRequ: AddNewEventRequest = CONSTANTS.makeCopy(this.newEvent); //make a copy
      newEventRequ.eventSlots = newEventRequ.eventSlots.concat(this.fillAddEventRequest())
      this.administratorService.modifyEvent(newEventRequ).subscribe(
        ok => {
          this.newEvent = newEventRequ
          this.childMessage.showMessage(6, MESSAGE_TYPE.SUCCESS);

          if (this.image != null) {
            this.administratorService.modifyEventImage(this.image.name, this.image, this.newEvent.id).subscribe(
              ok => {
                this.childMessage.showMessage(6, MESSAGE_TYPE.SUCCESS);
                this.loaded = true
              }, err => {
                this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
                this.loaded = true
              }
            )
          }

          this.loadAddNewEventRequest(false) // reload the page

        }, err => {
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
          this.loaded = true
        })
    }

  }


  fillAddEventRequest(): Array<EventSlot> {
    this.newEvent.eventSlots = this.eventDetail.eventSlots
    let newEvenSlot = []

    this.eventDates.forEach(date => {
      let dateSlots = date.getEventSlots()
      newEvenSlot = newEvenSlot.concat(dateSlots)
    })

    return newEvenSlot
  }




  checkEvent(): boolean {
    if (!AddNewEventRequest.validEvent(this.newEvent, this.childMessage)) {
      return false
    } else if (this.eventDates.length == 0 && this.eventDetail.eventSlots.length == 0) {
      this.childMessage.showMessage(17, MESSAGE_TYPE.ERROR)
      return false
    } else {
      let i = 0;
      this.eventDates.forEach(date => {
        if (EventDate.validDate(date)) {
          i++
        } else {
          this.childMessage.showMessageWithObject(13, { "i": this.eventDates.indexOf(date) + 1 }, MESSAGE_TYPE.ERROR) //TODO check
        }
      });
      return i == this.eventDates.length
    }
  }





  removeSlot(slot: EventSlot) { //only for new slots
    this.newEvent.eventSlots.splice(this.newEvent.eventSlots.indexOf(slot), 1)
    //toto server
  }

  selectFile(files: FileList) {
    this.image = files.item(0);

    if (this.image.size) {
      let blob = new Blob([this.image.slice()], { type: this.image.type })
      let objectURL = URL.createObjectURL(blob)
      this.imageurl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    }
  }

  removeDate(date: EventDate) {
    this.eventDates.splice(this.eventDates.indexOf(date), 1)
  }

  addDate() {
    this.eventDates.push(new EventDate)
  }

  public showSlots(date: string){
    this.selectedDate = date
    this.showEventSlots = this.eventSlots.get(date)

    var my_element = document.getElementById("target");
    my_element.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }


}
