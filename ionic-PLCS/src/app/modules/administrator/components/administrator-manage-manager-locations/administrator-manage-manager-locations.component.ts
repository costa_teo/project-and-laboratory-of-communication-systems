import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from 'src/app/entities/location';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { SharedService } from 'src/app/shared/shared.service';
import { AdministratorService } from '../../administrator.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/entities/user';

@Component({
  selector: 'app-administrator-manage-manager-locations',
  templateUrl: './administrator-manage-manager-locations.component.html',
  styleUrls: ['./administrator-manage-manager-locations.component.scss'],
})
export class AdministratorManageManagerLocationsComponent implements OnInit {
  selectedLocationName: any;
  selectedAddress: any;
  selectedCity: string;

  cities: string[] = [];
  locations: string[] = [];
  addresses: string[] = [];

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;
  managerId: number;
  manager: User = new User();


  constructor(
    private authenticationService: AuthenticationService,
    private sharedService: SharedService,
    private administratorService: AdministratorService,
    private route: ActivatedRoute

  ) { }

  loaded: boolean = true
  managerlocations: Array<Location> = []
  ngOnInit() {

    this.managerId = Number(this.route.snapshot.paramMap.get('id'))

    if (!Number.isInteger(this.managerId)) { //modify an existring one
      this.authenticationService.redirectToUserHome()
    }

    this.getManagerInfo();
    this.loadCity()


  }

  loadCity() {
    this.administratorService.getValidCities().subscribe(data => {
      this.cities = data;
      this.loaded = true

    }, err => {
      console.log("ERR")
    })

  }

  loadLocations() {
    this.locations = undefined
    this.selectedAddress = undefined
    this.selectedLocationName = undefined
    this.administratorService.getLocationNamesByCity(this.selectedCity).subscribe(locations => {
      this.locations = locations
    })
  }

  loadAddresses() {
    this.addresses = undefined
    this.administratorService.getAddressesByCityAndLocation(this.selectedCity, this.selectedLocationName).subscribe(addresses => {
      this.addresses = addresses;
    })
  }



  addManagerLocation() {

    let location = new Location
    location.city = this.selectedCity
    location.locationName = this.selectedLocationName
    location.address = this.selectedAddress

    if (location.city == undefined ||
      location.locationName == undefined ||
      location.address == undefined) {
      this.childMessage.showMessage(15, MESSAGE_TYPE.WARNING)
      console.log(location)
      return
    }


    this.loaded = false
    this.administratorService.addManagerLocation(location, this.managerId).subscribe(data => {
      this.loaded = true
      this.childMessage.showMessage(2, MESSAGE_TYPE.SUCCESS)
      this.managerlocations.push(location)
    }, err => {
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
      this.loaded = true
    })
  }

  removeManagerLocation(location: Location) {
    this.loaded = false
    this.administratorService.removeManagerLocation(location, this.managerId).subscribe(data => {
      this.loaded = true
      this.managerlocations.splice(this.managerlocations.indexOf(location), 1)
      this.childMessage.showMessage(2, MESSAGE_TYPE.SUCCESS)
    }, err => {
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
      this.loaded = true
    })

  }

  getManagerInfo() {
    this.loaded = false
    this.administratorService.getManagerInfo(this.managerId).subscribe(managerInfoResponse => {
      this.manager = managerInfoResponse.manager
      this.managerlocations = managerInfoResponse.managerLocation
      this.loaded = true
    }, err => {
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      this.loaded = true
    })
   }

}