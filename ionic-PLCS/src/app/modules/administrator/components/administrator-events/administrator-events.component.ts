import { Component, OnInit, ViewChild } from '@angular/core';
import { ScrollTop } from 'src/app/core/scroll-top/scroll-top';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { EventPreview } from 'src/app/entities/event-preview';
import { SharedService } from 'src/app/shared/shared.service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { AdministratorService } from '../../administrator.service';
import { CONSTANTS } from 'src/app/shared/constants';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';

@Component({
  selector: 'app-administrator-events',
  templateUrl: './administrator-events.component.html',
  styleUrls: ['./administrator-events.component.scss']
})
export class AdministratorEventsComponent implements OnInit {

  @ViewChild(ScrollTop, { static: true }) scrollTop: ScrollTop;
  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;
  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;


  loaded = true
  cities: Array<string>

  events: Array<EventPreview>
  pageNumber: number;
  totPages: number;
  selectedPage: number = 0;
  selectedDate: Date;
  selectedCity: string;
  eventName: string

  constructor(private sharedService: SharedService,
    private authenticationService: AuthenticationService,
    private administratorService: AdministratorService,
    public translate: TranslateService,
    private sanitizer: DomSanitizer) {

  }

  ngOnInit(): void {
    
    //load cities
    this.sharedService.getEventAllCities().subscribe(data => {
      this.cities = data;
    }, err=>{
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
    })

    //load events
    this.loadEvents()

  }


  goNextPage() {
    if (this.selectedPage < this.totPages - 1) {
      this.loadEvents(this.selectedPage + 1)
    }
  }

  goPreviusPage() {
    if (this.selectedPage > 0) {
      this.loadEvents(this.selectedPage - 1)
    }
  }



  loadEvents(page: number = 0) {
    this.loaded = false

    let dateString = CONSTANTS.convertDateToServer(this.selectedDate) //convert date to string

    this.sharedService.getEvents(page, this.selectedCity, dateString, this.eventName).subscribe(data => {
      this.selectedPage = page
      this.totPages = data.totPages
      this.events = data.eventsPreview

      this.events.forEach(e=>{
        e.firstDate = CONSTANTS.convertDateToGUI(e.firstDate)
        e.lastDate = CONSTANTS.convertDateToGUI(e.lastDate)


        if(e.imageId >= 0){  
          e['loading'] = true
          this.sharedService.getEventImage(e.imageId).subscribe( //loading images
            image=>{          
              let objectURL = URL.createObjectURL(image)
              e['image'] = this.sanitizer.bypassSecurityTrustUrl(objectURL);
              e['loading'] = false
            })
        }


      }) //correct the data format

      this.loaded = true
      this.scrollTop.scrollToTop()
    }, err => {
      console.error(err)
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)

      this.loaded = true

    })
  }


  deleteEvent(event: EventPreview) {
    this.loaded = false
    
    this.administratorService.deleteEvent(event).subscribe(ok=>{
      this.childMessage.showMessage(2, MESSAGE_TYPE.SUCCESS)
      this.events.splice(this.events.indexOf(event), 1)
      this.loaded = true
    },err =>{
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      this.loaded = true
    }) 
  }

  showQr(event: EventPreview){
    if(event.locationId != undefined){
      this.modal.show(2, "config:" + event.locationId)
    }else{
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
    }
  }

  
  reset(){
    this.selectedCity = undefined
    this.selectedDate = undefined
    this.eventName = undefined
  }




}
