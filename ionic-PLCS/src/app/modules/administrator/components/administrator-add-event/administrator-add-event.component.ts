import { Component, OnInit, ViewChild } from '@angular/core';
import { EventDetail } from 'src/app/entities/event-detail';
import { EventSlot } from 'src/app/entities/event-slot';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { AdministratorService } from '../../administrator.service';
import { AddNewEventRequest } from 'src/app/request/add-new-event-request';
import { TranslateService } from '@ngx-translate/core';
import { EventDate } from 'src/app/entities/event-date';
import { Time } from 'src/app/entities/time';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-administrator-add-event',
  templateUrl: './administrator-add-event.component.html',
  styleUrls: ['./administrator-add-event.component.scss']
})
export class AdministratorAddEventComponent implements OnInit {
  loaded = false
  newEvent = new AddNewEventRequest()

  eventDates: Array<EventDate> = [new EventDate]

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;
  image: File;
  imageurl: any;


  cities: string[];
  locations: string[] = []
  addresses: string[] = []

  constructor(private administratorService: AdministratorService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer,
    private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.newEvent.eventSlots.push(new EventSlot)
    this.loaded = true


    //load cities
    this.administratorService.getValidCities().subscribe(data => {
      this.cities = data;
    }, err => {
      this.loaded = true
    })
  }

  loadLocations() {
    this.newEvent.address = null
    this.newEvent.locationName = null
    this.locations = null
    this.administratorService.getLocationNamesByCity(this.newEvent.city).subscribe(locations => {
      this.locations = locations
    })
  }

  loadAddresses() {
    this.addresses = null
    this.administratorService.getAddressesByCityAndLocation(this.newEvent.city, this.newEvent.locationName).subscribe(addresses => {
      this.addresses = addresses;
      this.newEvent.address = this.addresses[0]
    })
  }

  addEvent() {
    if (this.checkEvent()) {
      if (this.image != null && this.image.size == 0) {
        this.image = null
      }
      this.loaded = false

      this.fillAddEventRequest()

      this.administratorService.addNewEvent(this.newEvent).subscribe(
        eventId => {
          this.childMessage.showMessage(6, MESSAGE_TYPE.SUCCESS);

          if (this.image != null) {
            this.administratorService.modifyEventImage(this.image.name, this.image, eventId).subscribe(
              ok => {
                this.childMessage.showMessage(7, MESSAGE_TYPE.SUCCESS);
                this.loaded = true
              }, err => {
                this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
                this.loaded = true
              }
            )
          }
          this.loaded = true


        }, err => {
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
          this.loaded = true
        })
    }

  }

  checkEvent(): boolean {
    if (!AddNewEventRequest.validEvent(this.newEvent, this.childMessage)) {
      return false
    } else {
      let i = 0;
      this.eventDates.forEach(date => {
        if (EventDate.validDate(date)){
          i++
        } else {
          this.childMessage.showMessageWithObject(13, { "i": this.eventDates.indexOf(date)+1 }, MESSAGE_TYPE.ERROR) //TODO check
        }
      });
      return i == this.eventDates.length
    }
  }


  fillAddEventRequest() {
    this.newEvent.eventSlots = []

    this.eventDates.forEach(date => {
      let dateSlots = date.getEventSlots()
      if(dateSlots.length == 0){
        this.childMessage.showMessageWithObject(18, {i: this.eventDates.indexOf(date)+1}, MESSAGE_TYPE.ERROR )
        throw "date with start date " + date + " not valid" 
      }
      this.newEvent.eventSlots = this.newEvent.eventSlots.concat(dateSlots) 
    })
  }



  removeDate(date: EventDate) {
    this.eventDates.splice(this.eventDates.indexOf(date), 1)
  }

  addDate() {
    this.eventDates.push(new EventDate)
  }


  selectFile(files: FileList) {
    this.image = files.item(0);

    if (this.image.size) {
      let blob = new Blob([this.image.slice()], { type: this.image.type })
      let objectURL = URL.createObjectURL(blob)
      this.imageurl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    }

  }


}
