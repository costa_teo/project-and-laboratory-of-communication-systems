# Project and laboratory of communication systems

## Politecnico di Torino 2 year course:

### folders descriptions:

```
Documentation (the project documentations)
    |
    ./Project
    ───/backend (the server running on java 13)
    |
    ───/Project./front-end-management (the front end part running with angular 9)
    |
    ───/gate (the python scripts for running qr core recognition on the raspberry) 
```
