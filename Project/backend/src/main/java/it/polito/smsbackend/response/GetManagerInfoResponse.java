package it.polito.smsbackend.response;

import java.util.List;

import it.polito.smsbackend.dto.Location;
import it.polito.smsbackend.dto.User;

public class GetManagerInfoResponse {
	private List<Location> managerLocation;
	private	User manager;
	
	public List<Location> getManagerLocation() {
		return managerLocation;
	}
	public void setManagerLocation(List<Location> managerLocation) {
		this.managerLocation = managerLocation;
	}
	public User getManager() {
		return manager;
	}
	public void setManager(User manager) {
		this.manager = manager;
	} 
		    
}
