package it.polito.smsbackend.repository;

import java.sql.Blob;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.ImageDAO;

public interface ImageRepository extends CrudRepository<ImageDAO, Integer> {

	@Modifying
	@Transactional
	@Query(value="INSERT INTO IMAGE (id, image, file_Name) "
			+ "VALUES (image_seq.nextval, :file, :name)", nativeQuery=true)
	void insertImage(@Param("file") byte[] file,@Param("name") String name);
	
	@Query(value="SELECT MAX(id) FROM IMAGE", nativeQuery = true)
	Integer getLastId();
	
	@Query(value="SELECT image FROM IMAGE WHERE id = :imageId", nativeQuery = true)
	Blob getAttachmentById(Integer imageId);
}
