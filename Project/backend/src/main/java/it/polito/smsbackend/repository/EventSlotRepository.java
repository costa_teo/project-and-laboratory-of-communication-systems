package it.polito.smsbackend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.EventSlotDAO;
import it.polito.smsbackend.dto.EventPreview;
import it.polito.smsbackend.dto.EventSlot;

public interface EventSlotRepository extends CrudRepository<EventSlotDAO, Integer> {
	
	@Query(value = 
			"SELECT new it.polito.smsbackend.dto.EventPreview(e.id, e.title, e.titolo, MIN(es.date) AS minDate, MAX(es.date) AS maxDate, e.imageRef, l.id) "
			+ "FROM EventSlotDAO es, EventDAO e, LocationDAO l "
			+ "WHERE es.eventRef = e.id AND e.locationRef = l.id "
			+ "AND es.valid = TRUE AND l.city LIKE :city AND es.date LIKE :date "
			+ "AND (LOWER(e.title) LIKE LOWER(:name) OR LOWER(e.titolo) LIKE LOWER(:name)) "
			+ "AND CONCAT(es.date, '-', es.startTime) >= CONCAT(:todayDate, '-', :nowTime) "
			+ "GROUP BY e.id, e.title, e.titolo "
			+ "HAVING COUNT(es.id) > 0 "
			+ "ORDER BY minDate, maxDate", nativeQuery = false)
	List<EventPreview> getEventsByCityDateName(@Param("city") String city, @Param("date") String date, @Param("name") String name, @Param("todayDate") String todayDate, @Param("nowTime") String nowTime);
	
	@Query(value = 
			"SELECT new it.polito.smsbackend.dto.EventSlot(es.id, es.date, es.startTime, es.endTime, es.maxPeople, "
			+ "es.maxPeople - (SELECT COUNT(*) FROM TicketDAO t WHERE es.id = t.eventslotRef) AS count, es.valid ) "
			+ "FROM EventSlotDAO es "
			+ "WHERE es.eventRef=:eventId AND CONCAT(es.date, '-', es.startTime) >= CONCAT(:todayDate, '-', :nowTime) AND valid = TRUE "
			+ "GROUP BY es.id, es.startTime, es.endTime, es.date, es.maxPeople, es.valid "
			+ "ORDER BY es.date, es.startTime", nativeQuery = false)
	List<EventSlot> getEventSlotsByEventIdUsers(Integer eventId, String todayDate, String nowTime);
	
	@Query(value = 
			"SELECT new it.polito.smsbackend.dto.EventSlot(es.id, es.date, es.startTime, es.endTime, es.maxPeople, "
			+ "es.maxPeople - (SELECT COUNT(*) FROM TicketDAO t WHERE es.id = t.eventslotRef) AS count, es.valid) "
			+ "FROM EventSlotDAO es "
			+ "WHERE es.eventRef=:eventId AND CONCAT(es.date, '-', es.startTime) >= CONCAT(:todayDate, '-', :nowTime) "
			+ "GROUP BY es.id, es.startTime, es.endTime, es.date, es.maxPeople, es.valid "
			+ "ORDER BY es.date, es.startTime", nativeQuery = false)
	List<EventSlot> getEventSlotsByEventIdPrivileged(Integer eventId, String todayDate, String nowTime);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO EVENT_SLOT (id, event_ref, date, start_Time, end_Time, max_People, valid) "
			+ "VALUES (eventslot_seq.nextval, :eventId, :date, :startTime, :endTime, :totalTickets, :valid)", nativeQuery = true)
	void insertEventSlot(String date, String startTime, String endTime, Integer totalTickets, Integer eventId, Boolean valid);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE EVENT_SLOT SET valid = :valid WHERE id = :eventslotId AND valid <> :valid", nativeQuery = true)
	int updateEventSlot(Integer eventslotId, Boolean valid);
	
	@Query(value = "SELECT * FROM EVENT_SLOT e WHERE e.id = :id", nativeQuery = true)
	EventSlotDAO getEventSlotById(@Param("id") Integer id);
	
	
}
