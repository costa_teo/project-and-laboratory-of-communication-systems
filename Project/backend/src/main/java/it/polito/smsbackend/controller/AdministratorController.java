package it.polito.smsbackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.polito.smsbackend.dto.Location;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.request.AddNewEventRequest;
import it.polito.smsbackend.request.AddNewLocationRequest;
import it.polito.smsbackend.request.GetUserRequest;
import it.polito.smsbackend.request.ManagerLocationRequest;
import it.polito.smsbackend.request.ModifyEventImageRequest;
import it.polito.smsbackend.response.GetManagerInfoResponse;
import it.polito.smsbackend.response.GetUserResponse;
import it.polito.smsbackend.service.AdministratorService;
import it.polito.smsbackend.service.AuthenticationService;

@RestController
@RequestMapping(value = "administrator")
@CrossOrigin( origins = "*" )
public class AdministratorController {

	private final AdministratorService administratorService;
	private final AuthenticationService authenticationService;

	@Autowired
	public AdministratorController(AdministratorService administratorService, AuthenticationService authenticationService) {
		this.administratorService = administratorService;
		this.authenticationService = authenticationService;
	}

	@PostMapping(value = "getUsers")
	public GetUserResponse getUsers(@RequestHeader("token") String token, @RequestBody GetUserRequest request) {
		Integer adminId = authenticationService.authenticateUser(token);
		return administratorService.getUsers(adminId, request);
	}

	@PostMapping(value = "changeUserRole")
	public void changeUserRole(@RequestHeader("token") String token, @RequestBody User user) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.changeUserRole(adminId, user);
	}

	@PostMapping(value = "addNewEvent")
	public Integer addNewEvent(@RequestHeader("token") String token, @RequestBody AddNewEventRequest request) {
		Integer adminId = authenticationService.authenticateUser(token);
		return administratorService.addNewEvent(adminId, request);
	}

	@PostMapping(value = "modifyEventImage")
	public void modifyEventImage(@RequestHeader("token") String token, @ModelAttribute ModifyEventImageRequest request) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.modifyEventImage(adminId, request);
	}

	@PostMapping(value = "modifyEvent")
	public void modifyEvent(@RequestHeader("language") String language, @RequestHeader("token") String token, @RequestBody AddNewEventRequest request) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.modifyEvent(adminId, request, language);
	}

	@PostMapping(value = "deleteEvent")
	public void deleteEvent(@RequestHeader("language") String language, @RequestHeader("token") String token, @RequestBody Integer eventId) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.deleteEvent(adminId, eventId, language);
	}
	
	@GetMapping(value = "getAllCities")
	public List<String> getAllCity(@RequestHeader("token") String token){
		Integer adminId = authenticationService.authenticateUser(token);
		return administratorService.getAllCities(adminId);
	}
	
	@GetMapping(value = "getValidCities")
	public List<String> getValidCity(@RequestHeader("token") String token){
		Integer adminId = authenticationService.authenticateUser(token);
		return administratorService.getValidCities(adminId);
	}
	
	@GetMapping(value = "getLocationNamesByCity")
	public List<String> getLocationNamesByCity(@RequestHeader("token") String token, @RequestParam("city") String city) {
		Integer adminId = authenticationService.authenticateUser(token);
		return administratorService.getLocationNamesByCity(adminId, city);
	}
	
	@GetMapping(value = "getAddressesByCityAndLocation")
	public List<String> getAddressesByCityAndLocation(@RequestHeader("token") String token, @RequestParam("city") String city, @RequestParam("location") String location) {
		Integer adminId = authenticationService.authenticateUser(token);
		return administratorService.getAddressesByCityAndLocation(adminId, city, location);
	}

	@GetMapping(value = "getLocationsByCity")
	public List<Location> getLocationsByCity(@RequestHeader("token") String token, @RequestParam("city") String city){
		Integer adminId = authenticationService.authenticateUser(token);
		return administratorService.getLocationsByCity(adminId, city);
	}
	
	@PostMapping(value = "addNewLocation")
	public void addLocation(@RequestHeader("token") String token, @RequestBody AddNewLocationRequest request) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.addNewLocation(adminId, request);
	}
	
	@PostMapping(value = "removeManagerLocation")
	public void removeManagerLocation(@RequestHeader("token") String token, @RequestBody ManagerLocationRequest request) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.removeManagerLocationRequest(adminId, request);
	}
	
	@PostMapping(value = "addManagerLocation")
	public void addManagerLocation(@RequestHeader("token") String token, @RequestBody ManagerLocationRequest request) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.addManagerLocationRequest(adminId, request);
	}
	
	@GetMapping(value = "getManagerInfo")
	public GetManagerInfoResponse getManagerInfo(@RequestHeader("token") String token, @RequestParam("managerId") Integer managerId) {
		Integer adminId = authenticationService.authenticateUser(token);	
		return administratorService.getManagerInfo(adminId, managerId);
	}
	
	@PostMapping(value = "changeLocationValidity")
	public void changeLocationValidity(@RequestHeader("token") String token, @RequestBody Location request) {
		Integer adminId = authenticationService.authenticateUser(token);
		administratorService.changeLocationValidity(adminId, request);
	}

}
