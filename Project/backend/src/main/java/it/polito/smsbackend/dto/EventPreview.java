package it.polito.smsbackend.dto;

public class EventPreview {

	private Integer id;
	private String title;
	private String titolo;
	private String firstDate;
	private String lastDate;
	private Integer imageId;
	private Integer locationId;

	public EventPreview(Integer id, String title, String titolo, String firstDate, String lastDate, Integer imageId, Integer locationId) {
		this.id = id;
		this.title = title;
		this.titolo = titolo;
		this.firstDate = firstDate;
		this.lastDate = lastDate;
		this.imageId = imageId;
		this.locationId = locationId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstDate() {
		return firstDate;
	}

	public void setFirstDate(String firstDate) {
		this.firstDate = firstDate;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String endDate) {
		this.lastDate = endDate;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

}
