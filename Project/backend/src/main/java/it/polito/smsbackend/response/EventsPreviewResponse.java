package it.polito.smsbackend.response;

import java.util.List;

import it.polito.smsbackend.dto.EventPreview;

public class EventsPreviewResponse {
	private Integer totPages;
	private Integer pageNumber;
	private List<EventPreview> eventsPreview;

	public Integer getTotPages() {
		return totPages;
	}

	public void setTotPages(Integer totPages) {
		this.totPages = totPages;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public List<EventPreview> getEventsPreview() {
		return eventsPreview;
	}

	public void setEventsPreview(List<EventPreview> eventsPreview) {
		this.eventsPreview = eventsPreview;
	}

}
