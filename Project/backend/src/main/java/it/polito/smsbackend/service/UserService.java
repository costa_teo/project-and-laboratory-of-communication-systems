package it.polito.smsbackend.service;

import java.sql.Blob;
import java.util.List;

import it.polito.smsbackend.dto.EventDetail;
import it.polito.smsbackend.dto.Ticket;
import it.polito.smsbackend.request.BookEventTotemRequest;
import it.polito.smsbackend.request.EventsPreviewRequest;
import it.polito.smsbackend.request.RegisterRequest;
import it.polito.smsbackend.response.EventsPreviewResponse;


public interface UserService {
		
	EventsPreviewResponse getEventsByCityDateName(EventsPreviewRequest request);
	
	Blob getEventImage(Integer eventId);

	List<String> getEventsCities();

	EventDetail getEventDetail(Integer eventId);

	void bookEventSlot(Integer userId, Integer eventSlotId, String language);

	List<Ticket> getCustomerTickets(Integer userId);

	void registerUser(RegisterRequest request, String language);

	void cancelTicket(Integer userId, Integer ticketId);

	Boolean checkTicket(String ticketCode);

	Ticket bookEventSlotNoAccount(BookEventTotemRequest request, String language);

	void activateAccount(String signHash);
	
}
