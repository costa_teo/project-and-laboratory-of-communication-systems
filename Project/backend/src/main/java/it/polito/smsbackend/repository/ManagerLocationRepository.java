package it.polito.smsbackend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.ManagerLocationDAO;
import it.polito.smsbackend.dto.EventPreview;
import it.polito.smsbackend.dto.Location;

public interface ManagerLocationRepository extends CrudRepository<ManagerLocationDAO, Integer> {
	
	@Query(value = "SELECT COUNT(*) FROM MANAGER_LOCATION_RELATION "
			+ "WHERE manager_ref = :managerId AND location_ref = :locationId", nativeQuery = true)
	int existRelation(Integer managerId, Integer locationId);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM MANAGER_LOCATION_RELATION "
			+ "WHERE manager_ref = :managerId AND location_ref = :locationId", nativeQuery=true)
	void removeAssociation(Integer managerId, Integer locationId);
	
	@Query(value = "SELECT DISTINCT city "
			+ "FROM MANAGER_LOCATION_RELATION lmr, LOCATION l "
			+ "WHERE lmr.location_ref = l.id AND lmr.manager_ref = :managerId AND l.valid = TRUE", nativeQuery = true)
	List<String> getManagerCities(Integer managerId);
	
	@Query(value = "SELECT name "
			+ "FROM MANAGER_LOCATION_RELATION lmr, LOCATION l "
			+ "WHERE lmr.location_ref = l.id AND lmr.manager_ref = :managerId AND l.city = :city AND l.valid = TRUE", nativeQuery = true)
	List<String> getManagerLocationsByCity(Integer managerId, String city);
	
	@Query(value = "SELECT address "
			+ "FROM MANAGER_LOCATION_RELATION lmr, LOCATION l "
			+ "WHERE lmr.location_ref = l.id AND lmr.manager_ref = :managerId AND l.city = :city "
			+ "AND l.name = :locationName AND l.valid = TRUE", nativeQuery = true)
	List<String> getManagerAddressesByCityLocation(Integer managerId, String city, String locationName);
	
	@Query(value = "SELECT COUNT(*) FROM MANAGER_LOCATION_RELATION mlr, EVENT e "
			+ "WHERE  e.id = :eventId AND e.location_ref = mlr.location_ref AND mlr.manager_ref = :managerId", nativeQuery = true)
	int isManagerEnabled(Integer managerId, Integer eventId);
	
	@Query(value = "SELECT new it.polito.smsbackend.dto.EventPreview(e.id, e.title, e.titolo, MIN(es.date) AS minDate, MAX(es.date) AS maxDate, e.imageRef, l.id) "
			+ "FROM EventSlotDAO es, EventDAO e, LocationDAO l " 
			+ "WHERE es.eventRef = e.id AND e.locationRef = l.id AND es.valid = TRUE "
			+ "AND l.city LIKE :city AND date LIKE :date AND (LOWER(e.title) LIKE LOWER(:name) OR LOWER(e.titolo) LIKE LOWER(:name)) "
			+ "AND CONCAT(es.date, '-', es.startTime) >= CONCAT(:todayDate, '-', :nowTime) "
			+ "AND l.id IN (SELECT locationRef FROM ManagerLocationDAO WHERE managerRef = :managerId) "
			+ "GROUP BY e.id, e.title, e.titolo "
			+ "HAVING COUNT(es.id) > 0 "
			+ "ORDER BY minDate, maxDate", nativeQuery = false)
	List<EventPreview> getManagerEventsByCityDateName(@Param("city") String city, @Param("date") String date, @Param("name") String name,
			@Param("todayDate") String todayDate, @Param("nowTime") String nowTime, @Param("managerId") Integer managerId);

	@Query (value="SELECT new it.polito.smsbackend.dto.Location(l.name, l.address, l.city, l.valid) "
			+ "FROM LocationDAO l, ManagerLocationDAO ml "
			+ "WHERE l.id = ml.locationRef AND ml.managerRef = :managerId", nativeQuery = false)
	List<Location> getManagerLocations(Integer managerId);

	@Query(value = "SELECT DISTINCT l.city "
			+ "FROM EVENT e, LOCATION l, EVENT_SLOT es, MANAGER_LOCATION_RELATION mlr "
			+ "WHERE e.location_ref = l.id AND es.event_ref = e.id AND mlr.location_ref = l.id "
			+ "AND mlr.manager_ref = :managerId AND e.id IN ( "
			+ 			"SELECT event_ref "
			+ 			"FROM EVENT_SLOT "
			+ 			"WHERE valid = TRUE AND date >= :todayDate "
			+ 			"GROUP BY event_ref "
			+ 			"HAVING COUNT(*) > 0 "
			+ ")", nativeQuery = true)
	List<String> getEventCities(Integer managerId, String todayDate);

}
