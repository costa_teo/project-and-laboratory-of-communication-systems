package it.polito.smsbackend.request;

import org.springframework.web.multipart.MultipartFile;

public class ModifyEventImageRequest {
	
	private Integer eventId;
	private MultipartFile file;
	private String name;

	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}

