package it.polito.smsbackend.dto;

import java.util.List;

public class EventDetail {
	private Integer id;
	private String title;
	private String description;
	private String titolo;
	private String descrizione;
	private String city;
	private String address;
	private String locationName;
	private Integer imageId;
	private List<EventSlot> eventSlots;

	public EventDetail(Integer id, String title, String description, String titolo, String descrizione, String city, String address, String locationName,
			Integer imageId) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.titolo = titolo;
		this.descrizione = descrizione;
		this.city = city;
		this.address = address;
		this.locationName = locationName;
		this.imageId = imageId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}

	public List<EventSlot> getEventSlots() {
		return eventSlots;
	}

	public void setEventSlots(List<EventSlot> eventSlots) {
		this.eventSlots = eventSlots;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

}
