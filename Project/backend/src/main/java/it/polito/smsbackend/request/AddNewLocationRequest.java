package it.polito.smsbackend.request;

public class AddNewLocationRequest {

	private String city;
	private String locationName;
	private String address;

	public AddNewLocationRequest(String city, String locationName, String address) {
		this.city = city;
		this.locationName = locationName;
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
