package it.polito.smsbackend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.LocationDAO;
import it.polito.smsbackend.dto.Location;

public interface LocationRepository extends CrudRepository<LocationDAO, Integer> {

	@Query(value = "SELECT id FROM LOCATION WHERE name = :name AND city = :city AND address = :address", nativeQuery = true)
	Integer getLocationId(@Param("name") String name, @Param("city") String city, @Param("address") String address);

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO LOCATION (id, name, city, address, valid) "
			+ "VALUES (location_seq.nextval, :name, :city, :address, :valid)", nativeQuery = true)
	void insertLocation(@Param("name") String name, @Param("city") String city, @Param("address") String address,
			@Param("valid") Boolean valid);

	@Query(value = "SELECT MAX(id) FROM LOCATION", nativeQuery = true)
	Integer getLastId();

	@Query(value = "SELECT DISTINCT city FROM LOCATION " + " ORDER BY city", nativeQuery = true)
	List<String> getAllCities();

	@Query(value = "SELECT DISTINCT city FROM LOCATION WHERE valid = TRUE" + " ORDER BY city", nativeQuery = true)
	List<String> getValidCities();

	@Query(value = "SELECT name FROM LOCATION WHERE city = :city AND valid = TRUE", nativeQuery = true)
	List<String> getLocationsByCity(@Param("city") String city);

	@Query(value = "SELECT address FROM LOCATION WHERE city = :city AND name = :location AND valid = TRUE", nativeQuery = true)
	List<String> getAddressesByCityAndName(@Param("city") String city, @Param("location") String location);

	@Query(value = "SELECT new it.polito.smsbackend.dto.Location(name, address, city, valid) FROM LocationDAO WHERE city = :city", nativeQuery = false)
	List<Location> getLocationsObjectByCity(String city);

	@Query(value = "SELECT COUNT(*) FROM LOCATION WHERE name = :locationName AND city = :city AND address = :address", nativeQuery = true)
	int checkLocation(String locationName, String city, String address);

	@Query(value = "SELECT * FROM LOCATION l WHERE l.id = :id", nativeQuery = true)
	LocationDAO getLocationById(@Param("id") Integer id);

}
