package it.polito.smsbackend.dto;

public class Location {
	private String locationName;
	private String address;
	private String city;
	private Boolean valid;

	public Location(String locationName, String address, String city, Boolean valid) {
		this.locationName = locationName;
		this.address = address;
		this.city = city;
		this.valid = valid;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

}
