package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MANAGER_LOCATION_RELATION")
public class ManagerLocationDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "manager_location_generator")
	@SequenceGenerator(name = "manager_location_generator", sequenceName = "manager_location_seq", allocationSize = 1)
	private Integer id;
	@Column(nullable = false, name = "manager_ref")
	private Integer managerRef;
	@Column(nullable = false, name = "location_ref")
	private Integer locationRef;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getManagerRef() {
		return managerRef;
	}
	public void setManagerRef(Integer managerRef) {
		this.managerRef = managerRef;
	}
	public Integer getLocationRef() {
		return locationRef;
	}
	public void setLocationRef(Integer locationRef) {
		this.locationRef = locationRef;
	}
	
}
