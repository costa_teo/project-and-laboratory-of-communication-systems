package it.polito.smsbackend.dao;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="IMAGE")
public class ImageDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "image_generator")
	@SequenceGenerator(name="image_generator", sequenceName = "image_seq", allocationSize=1)
	private Integer id;
	@Column(nullable = false)
	private String fileName;
	@Lob
	private Blob image;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Blob getFile() {
		return image;
	}
	
	public void setFile(Blob image) {
		this.image = image;
	}
		
	
}
