package it.polito.smsbackend.serviceimpl;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.common.Constants;
import it.polito.smsbackend.common.Util;
import it.polito.smsbackend.dao.EventDAO;
import it.polito.smsbackend.dao.LocationDAO;
import it.polito.smsbackend.dao.ManagerLocationDAO;
import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.EventSlot;
import it.polito.smsbackend.dto.Location;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.emailsystem.EmailSystem;
import it.polito.smsbackend.repository.EventRepository;
import it.polito.smsbackend.repository.EventSlotRepository;
import it.polito.smsbackend.repository.ImageRepository;
import it.polito.smsbackend.repository.LocationRepository;
import it.polito.smsbackend.repository.ManagerLocationRepository;
import it.polito.smsbackend.repository.UserRepository;
import it.polito.smsbackend.request.AddNewEventRequest;
import it.polito.smsbackend.request.AddNewLocationRequest;
import it.polito.smsbackend.request.GetUserRequest;
import it.polito.smsbackend.request.ManagerLocationRequest;
import it.polito.smsbackend.request.ModifyEventImageRequest;
import it.polito.smsbackend.response.GetManagerInfoResponse;
import it.polito.smsbackend.response.GetUserResponse;
import it.polito.smsbackend.service.AdministratorService;

@Service
public class AdministratorServiceImpl implements AdministratorService {
	
	private final UserRepository userRepository;
	private final LocationRepository locationRepository;
	private final ImageRepository imageRepository;
	private final EventRepository eventRepository;
	private final EventSlotRepository eventslotRepository;
	private final ManagerLocationRepository managerLocationRepository;
	
	@Autowired
	public AdministratorServiceImpl(UserRepository userRepository, LocationRepository locationRepository,
			ImageRepository imageRepository, EventRepository eventRepository, EventSlotRepository eventslotRepository,
			ManagerLocationRepository managerLocationRepository) {
		this.managerLocationRepository = managerLocationRepository;
		this.userRepository = userRepository;
		this.locationRepository = locationRepository;
		this.imageRepository = imageRepository;
		this.eventRepository = eventRepository;
		this.eventslotRepository = eventslotRepository;
	}
	
	@Override
	public GetUserResponse getUsers(Integer adminId, GetUserRequest request) {
		
		if(userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		String role = Objects.toString(request.getRole(), "");
		String mail = Objects.toString(request.getUserEmail(), "");
		
		Integer pageNumber = (request.getPage() != null && request.getPage() >= 0)? request.getPage() : 0;
		
		mail = (mail.isEmpty()) ? "%" : "%"+mail.toLowerCase()+"%";
		
		if (role.isEmpty())
			role = "%";
		else if ((!role.equals(Constants.CUSTOMER) && !role.equals(Constants.ADMINISTRATOR) && !role.equals(Constants.MANAGER)))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		 
		List<User> users = userRepository.getUsers(role, mail);
		
		GetUserResponse gur = new GetUserResponse();
		
		gur.setTotPages((users.size() / Constants.pageSize) + 1);
		gur.setPageNumber(pageNumber);

		Integer from = pageNumber * Constants.pageSize;
		from = (from >= 0 && from < users.size()) ? from : 0; // check low boundary
		Integer to = (pageNumber + 1) * Constants.pageSize;
		to = (to >= 0 && to < users.size()) ? to : users.size(); // check high boundaries
		
		gur.setUsers(users.subList(from, to));
		
		return gur;
	}

	@Override
	public void changeUserRole(Integer adminId, User user) {
		
		if(userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(userRepository.findById(user.getId()).isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(adminId == user.getId())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		String newRole = user.getRole();
		if ((!newRole.equals(Constants.CUSTOMER) && !newRole.equals(Constants.ADMINISTRATOR) && !newRole.equals(Constants.MANAGER)))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		userRepository.changeUserRole(user.getId(), newRole);
		 
	}

	@Override
	public Integer addNewEvent(Integer adminId, AddNewEventRequest request) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

		Util.checkAddEventRequestValidity(request);
		
		Integer locationId = locationRepository.getLocationId(request.getLocationName(), request.getCity(), request.getAddress());
		if (locationId == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		eventRepository.insertEvent(request.getTitle(), request.getDescription(), request.getTitolo(), request.getDescrizione(), 0 /* defaultImage */, locationId);
		Integer eventId = eventRepository.getLastId();
		
		for (EventSlot es : request.getEventSlots()) {
			eventslotRepository.insertEventSlot(es.getDate(), es.getStartTime(), es.getEndTime(), es.getTotalTickets(), eventId, Boolean.TRUE);
		}
		
		return eventId;
		
	}

	@Override
	public void modifyEventImage(Integer adminId, ModifyEventImageRequest request) {

		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

		if(request.getFile() == null || request.getName() == null || request.getName().isBlank() || request.getEventId() == null || request.getEventId() < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(eventRepository.getEventDAOById(request.getEventId()) == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		Integer imageId = null;
		try {
			imageRepository.insertImage(request.getFile().getBytes(), request.getFile().getOriginalFilename());
			imageId = imageRepository.getLastId();
		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		EventDAO updated = eventRepository.findById(request.getEventId()).get();
		updated.setImageRef(imageId);
		eventRepository.save(updated);
		
	}

	@Override
	public void modifyEvent(Integer adminId, AddNewEventRequest request, String language) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(!Util.isLanguageValid(language))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		Util.checkAddEventRequestValidity(request);

		Integer locationId = locationRepository.getLocationId(request.getLocationName(), request.getCity(), request.getAddress());
		if (locationId == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		EventDAO oldEvent = eventRepository.getEventDAOById(request.getId());
		if(oldEvent == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		eventRepository.updateEvent(request.getId(), request.getTitle(), request.getDescription(), request.getTitolo(), request.getDescrizione(), locationId);

		for(EventSlot es : request.getEventSlots()) {
			if(es.getId() != null) {
				// TRUE -> updated EventSlot -> UPDATE
				List<UserDAO> partecipants = userRepository.getPartecipantsByEventSlotId(es.getId());
				LocationDAO oldLocation = locationRepository.getLocationById(oldEvent.getLocationRef());
				Integer affectedRows = eventslotRepository.updateEventSlot(es.getId(), es.getValid());
				if(affectedRows == 1) {
					if(es.getValid().equals(Boolean.FALSE)) {
						// DELETE Notification
						EmailSystem.sendDeleteEmail(
								partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()), es.getDate(),
								es.getStartTime(), es.getEndTime(), request.getTitle(), request.getTitolo(), oldLocation.getName(),
								oldLocation.getCity(), oldLocation.getAddress(), language);
					}
					else {
						// RESTORE Notification
						EmailSystem.sendRestoreEmail(
								partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()), es.getDate(),
								es.getStartTime(), es.getEndTime(), request.getTitle(), request.getTitolo(), request.getLocationName(),
								request.getCity(), request.getAddress(), language);
					}
				} else if(locationId != oldEvent.getLocationRef()) {
					// UPDATE Location Notification
					EmailSystem.sendUpdateLocationMail(
							partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()), es.getDate(),
							es.getStartTime(), es.getEndTime(), request.getTitle(), request.getTitolo(), request.getLocationName(),
							request.getCity(), request.getAddress(), oldLocation.getName(), oldLocation.getCity(),
							oldLocation.getAddress(), language);
				}
					
			}
			else {
				// FALSE -> new EventSlot -> INSERT
				eventslotRepository.insertEventSlot(es.getDate(), es.getStartTime(), es.getEndTime(), es.getTotalTickets(), request.getId(), Boolean.TRUE);
			}
		}
	}

	@Override
	public void deleteEvent(Integer adminId, Integer eventId, String language) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(eventId < 0 || eventId == null || !Util.isLanguageValid(language)) 
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		EventDAO event = eventRepository.getEventDAOById(eventId);
		if(event == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		LocationDAO location = locationRepository.getLocationById(event.getLocationRef());
		
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String nowTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		
		for(EventSlot es : eventslotRepository.getEventSlotsByEventIdPrivileged(eventId, todayDate, nowTime)) {
			List<UserDAO> partecipants = userRepository.getPartecipantsByEventSlotId(es.getId());
			Integer affectedRows = eventslotRepository.updateEventSlot(es.getId(), Boolean.FALSE);
			if(affectedRows == 1 && partecipants.size() != 0) {
				// DELETE Notification
				EmailSystem.sendDeleteEmail(partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()),
						es.getDate(), es.getStartTime(), es.getEndTime(), event.getTitle(), event.getTitolo(), location.getName(),
						location.getCity(), location.getAddress(), language);
			}
		}
	}

	@Override
	public List<Location> getLocationsByCity(Integer adminId, String city) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		if(city == null || city.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		return locationRepository.getLocationsObjectByCity(city);
	}

	@Override
	public void addNewLocation(Integer adminId, AddNewLocationRequest request) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		if (request.getCity() == null || request.getCity().isBlank() || request.getAddress() == null
				|| request.getAddress().isBlank() || request.getLocationName() == null
				|| request.getLocationName().isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(locationRepository.checkLocation(request.getCity(), request.getLocationName(), request.getAddress()) != 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		locationRepository.insertLocation(request.getLocationName(), request.getCity(), request.getAddress(), Boolean.TRUE);
		
	}
	
	@Override
	public List<String> getAllCities(Integer adminId) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		return locationRepository.getAllCities();
	}

	@Override
	public List<String> getValidCities(Integer adminId) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		return locationRepository.getValidCities();
	}
	
	@Override
	public List<String> getLocationNamesByCity(Integer adminId, String city) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(city == null || city.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		return locationRepository.getLocationsByCity(city);
	}

	@Override
	public List<String> getAddressesByCityAndLocation(Integer adminId, String city, String location) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(city == null || city.isBlank() || location == null || location.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		return locationRepository.getAddressesByCityAndName(city, location);
	}
	
	@Override
	public void addManagerLocationRequest(Integer adminId, ManagerLocationRequest request) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		String locationName = Objects.toString(request.getLocation().getLocationName(), "");
		String address = Objects.toString(request.getLocation().getAddress(), "");
		String city = Objects.toString(request.getLocation().getCity(), "");
		Integer managerId = request.getManagerId();
		
		if(managerId <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Manager");
		
		if(locationName.isBlank() || address.isBlank() || city.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Location");
		
		UserDAO u = userRepository.getUserById(request.getManagerId());
		if(!Constants.MANAGER.equals(u.getRole()))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not manager");
		
		if(locationRepository.checkLocation(locationName, city, address) != 1)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Location doesn't exist");
		
		Integer locationId = locationRepository.getLocationId(locationName, city, address);
		
		if(managerLocationRepository.existRelation(managerId, locationId) != 0)
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Already exist association");
		
		ManagerLocationDAO ml = new ManagerLocationDAO();
		ml.setLocationRef(locationId);
		ml.setManagerRef(managerId);
		managerLocationRepository.save(ml);
	}

	@Override
	public void removeManagerLocationRequest(Integer adminId, ManagerLocationRequest request) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		String locationName = Objects.toString(request.getLocation().getLocationName(), "");
		String address = Objects.toString(request.getLocation().getAddress(), "");
		String city = Objects.toString(request.getLocation().getCity(), "");
		Integer managerId = request.getManagerId();
		
		if(managerId < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Manager");
		
		if(locationName.isBlank() || address.isBlank() || city.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Location");
		
		UserDAO u = userRepository.getUserById(request.getManagerId());
		if(!Constants.MANAGER.equals(u.getRole()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not manager");
		
		if(locationRepository.checkLocation(locationName, city, address) != 1)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Location doesn't exist");
		
		Integer locationId = locationRepository.getLocationId(locationName, city, address);

		if(managerLocationRepository.existRelation(managerId, locationId) != 1)
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Association doesn't exist");
		
		managerLocationRepository.removeAssociation(managerId, locationId);
	}

	@Override
	public GetManagerInfoResponse getManagerInfo(Integer adminId, Integer managerId) {
		if (adminId <= 0 || managerId <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invlid ids");
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0
				|| userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		List<Location> ll = managerLocationRepository.getManagerLocations(managerId);
		User u = userRepository.getUserById(managerId).map();
		GetManagerInfoResponse gmir = new GetManagerInfoResponse();
		gmir.setManager(u);
		gmir.setManagerLocation(ll);
		
		return gmir;
	}

	@Override
	public void changeLocationValidity(Integer adminId, Location request) {
		if (userRepository.checkPrivilegeRole(adminId, Constants.ADMINISTRATOR) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		String locationName = Objects.toString(request.getLocationName(), "");
		String address = Objects.toString(request.getAddress(), "");
		String city = Objects.toString(request.getCity(), "");
		
		if(locationName.isBlank() || address.isBlank() || city.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Location");
		if(request.getValid() != Boolean.TRUE && request.getValid() != Boolean.FALSE)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Location validity");
		
		if(locationRepository.checkLocation(locationName, city, address) != 1)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Location doesn't exist");
		
		Integer locationId = locationRepository.getLocationId(locationName, city, address);
		LocationDAO l = locationRepository.getLocationById(locationId);
		l.setValid(request.getValid());
		locationRepository.save(l);
	}
	

}
