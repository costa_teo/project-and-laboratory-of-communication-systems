package it.polito.smsbackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.polito.smsbackend.dto.EventDetail;
import it.polito.smsbackend.request.AddNewEventRequest;
import it.polito.smsbackend.request.EventsPreviewRequest;
import it.polito.smsbackend.request.ModifyEventImageRequest;
import it.polito.smsbackend.response.EventsPreviewResponse;
import it.polito.smsbackend.service.AuthenticationService;
import it.polito.smsbackend.service.ManagerService;


@RestController
@RequestMapping(value = "manager")
@CrossOrigin( origins = "*" )
public class ManagerController {

	private final ManagerService managerService;
	private final AuthenticationService authenticationService;
	
	@Autowired
	public ManagerController(ManagerService managerService, AuthenticationService authenticationService) {
		this.managerService = managerService;
		this.authenticationService = authenticationService;
	}
	
	@PostMapping(value = "deleteEvent")
	public void deleteManagerEvent(@RequestHeader("language") String language, @RequestHeader("token") String token, @RequestBody Integer eventId) {
		Integer managerId = this.authenticationService.authenticateUser(token);
		managerService.deleteEvent(managerId, eventId, language);
	}
	
	@PostMapping(value = "getEvents")
	public EventsPreviewResponse getManagerEvents(@RequestHeader("token") String token, @RequestBody EventsPreviewRequest request) {
		Integer managerId = this.authenticationService.authenticateUser(token);
		return managerService.getManagerEvents(managerId, request);
	}
	
	@PostMapping(value = "addNewEvent")
	public Integer addNewEvent(@RequestHeader("token") String token, @RequestBody AddNewEventRequest request) {
		Integer managerId = this.authenticationService.authenticateUser(token);
		return managerService.addNewEvent(managerId, request);
	}
	
	@GetMapping(value = "getEventDetail")
	public EventDetail getEventDetail(@RequestHeader("token") String token, @RequestParam("id") Integer eventId)  {
		Integer managerId = this.authenticationService.authenticateUser(token);
		return managerService.getEventDetail(managerId, eventId);
	}
	
	@PostMapping(value = "modifyEvent")
	public void modifyEvent(@RequestHeader("language") String language, @RequestHeader("token") String token,
			@RequestBody AddNewEventRequest request) {
		Integer managerId = this.authenticationService.authenticateUser(token);
		managerService.modifyEvent(managerId, request, language);
	}
	
	@PostMapping(value = "modifyEventImage")
	public void modifyEventImage(@RequestHeader("token") String token, @ModelAttribute ModifyEventImageRequest request) {
		Integer managerId = authenticationService.authenticateUser(token);
		managerService.modifyEventImage(managerId, request);
	}
	
	@GetMapping(value = "getCities")
	public List<String> getCities(@RequestHeader("token") String token)  {
		Integer managerId = this.authenticationService.authenticateUser(token);
		return managerService.getCities(managerId);
	}
	
	@GetMapping(value = "getLocationNamesByCity")
	public List<String> getLocationNamesByCity(@RequestHeader("token") String token, @RequestParam("city") String city)  {
		Integer managerId = this.authenticationService.authenticateUser(token);
		return managerService.getLocationNames(managerId, city);
	}
	
	@GetMapping(value = "getAddressesByCityAndLocation")
	public List<String> getAddressesByCityAndLocation(@RequestHeader("token") String token,
			@RequestParam("city") String city, @RequestParam("locationName") String locationName) {
		Integer managerId = this.authenticationService.authenticateUser(token);
		return managerService.getAddressesByCityLocation(managerId, city, locationName);
	}
	
	@GetMapping(value = "getEventsCities")
	public List<String> getEventsCities(@RequestHeader("token") String token)  {
		Integer managerId = this.authenticationService.authenticateUser(token);
		return managerService.getEventsCities(managerId);
	}	
	
}
