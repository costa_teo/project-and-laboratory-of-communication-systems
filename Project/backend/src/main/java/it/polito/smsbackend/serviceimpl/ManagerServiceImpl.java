package it.polito.smsbackend.serviceimpl;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.common.Constants;
import it.polito.smsbackend.common.Util;
import it.polito.smsbackend.dao.EventDAO;
import it.polito.smsbackend.dao.LocationDAO;
import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.EventDetail;
import it.polito.smsbackend.dto.EventPreview;
import it.polito.smsbackend.dto.EventSlot;
import it.polito.smsbackend.emailsystem.EmailSystem;
import it.polito.smsbackend.repository.EventRepository;
import it.polito.smsbackend.repository.EventSlotRepository;
import it.polito.smsbackend.repository.ImageRepository;
import it.polito.smsbackend.repository.LocationRepository;
import it.polito.smsbackend.repository.ManagerLocationRepository;
import it.polito.smsbackend.repository.UserRepository;
import it.polito.smsbackend.request.AddNewEventRequest;
import it.polito.smsbackend.request.EventsPreviewRequest;
import it.polito.smsbackend.request.ModifyEventImageRequest;
import it.polito.smsbackend.response.EventsPreviewResponse;
import it.polito.smsbackend.service.ManagerService;

@Service
public class ManagerServiceImpl implements ManagerService {

	private final UserRepository userRepository;
	private final LocationRepository locationRepository;
	private final EventRepository eventRepository;
	private final EventSlotRepository eventslotRepository;
	private final ImageRepository imageRepository;
	private final ManagerLocationRepository managerLocationRepository;

	@Autowired
	public ManagerServiceImpl(UserRepository userRepository, LocationRepository locationRepository,
			EventRepository eventRepository, EventSlotRepository eventslotRepository, ImageRepository imageRepository,
			ManagerLocationRepository managerLocationRepository) {
		this.managerLocationRepository = managerLocationRepository;
		this.userRepository = userRepository;
		this.locationRepository = locationRepository;
		this.eventRepository = eventRepository;
		this.eventslotRepository = eventslotRepository;
		this.imageRepository = imageRepository;
	}

	@Override
	public Integer addNewEvent(Integer managerId, AddNewEventRequest request) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

		Util.checkAddEventRequestValidity(request);
		
		Integer locationId = locationRepository.getLocationId(request.getLocationName(), request.getCity(), request.getAddress());
		if (locationId == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(managerLocationRepository.existRelation(managerId, locationId) != 1)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Manager not enabled");

		eventRepository.insertEvent(request.getTitle(), request.getDescription(), request.getTitolo(), request.getDescrizione(), 0 /* defaultImage */, locationId);
		Integer eventId = eventRepository.getLastId();

		for (EventSlot es : request.getEventSlots()) {
			eventslotRepository.insertEventSlot(es.getDate(), es.getStartTime(), es.getEndTime(), es.getTotalTickets(), eventId, Boolean.TRUE);
		}

		return eventId;
	}
	
	@Override
	public void modifyEvent(Integer managerId, AddNewEventRequest request, String language) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(!Util.isLanguageValid(language))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		Util.checkAddEventRequestValidity(request);
		
		EventDAO oldEvent = eventRepository.getEventDAOById(request.getId());
		if(oldEvent == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(managerLocationRepository.isManagerEnabled(managerId, oldEvent.getId()) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		Integer locationId = locationRepository.getLocationId(request.getLocationName(), request.getCity(), request.getAddress());
		if (locationId == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		eventRepository.updateEvent(request.getId(), request.getTitle(), request.getDescription(), request.getTitolo(), request.getDescrizione(), locationId);

		for(EventSlot es : request.getEventSlots()) {
			if(es.getId() != null) {
				// TRUE -> updated EventSlot -> UPDATE
				List<UserDAO> partecipants = userRepository.getPartecipantsByEventSlotId(es.getId());
				LocationDAO oldLocation = locationRepository.getLocationById(oldEvent.getLocationRef());
				Integer affectedRows = eventslotRepository.updateEventSlot(es.getId(), es.getValid());
				if(affectedRows == 1) {
					if(es.getValid().equals(Boolean.FALSE)) {
						// DELETE Notification
						EmailSystem.sendDeleteEmail(
								partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()), es.getDate(),
								es.getStartTime(), es.getEndTime(), request.getTitle(), request.getTitolo(), oldLocation.getName(),
								oldLocation.getCity(), oldLocation.getAddress(), language);
					}
					else {
						// RESTORE Notification
						EmailSystem.sendRestoreEmail(
								partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()), es.getDate(),
								es.getStartTime(), es.getEndTime(), request.getTitle(), request.getTitolo(), request.getLocationName(),
								request.getCity(), request.getAddress(), language);
					}
				} else if(locationId != oldEvent.getLocationRef()) {
					// UPDATE Location Notification
					EmailSystem.sendUpdateLocationMail(
							partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()), es.getDate(),
							es.getStartTime(), es.getEndTime(), request.getTitle(), request.getTitolo(), request.getLocationName(),
							request.getCity(), request.getAddress(), oldLocation.getName(), oldLocation.getCity(),
							oldLocation.getAddress(), language);
				}
					
			}
			else {
				// FALSE -> new EventSlot -> INSERT
				eventslotRepository.insertEventSlot(es.getDate(), es.getStartTime(), es.getEndTime(), es.getTotalTickets(), request.getId(), Boolean.TRUE);
			}
		}
	}
	
	@Override
	public EventsPreviewResponse getManagerEvents(Integer managerId, EventsPreviewRequest request) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		String city = Objects.toString(request.getCity(), "%");
		String date = Objects.toString(request.getDate(), "%");
		String name = Objects.toString(request.getEventName(), "%");

		city = (city.isBlank()) ? "%" : city;
		date = (date.isBlank()) ? "%" : date;
		name = (name.isBlank()) ? "%" : "%"+name+"%";

		Integer pageNumber = (request.getPage() != null && request.getPage() >= 0)? request.getPage() : 0;
		
		List<EventPreview> eventsPreview = null;
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String nowTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		
		eventsPreview = managerLocationRepository.getManagerEventsByCityDateName(city, date, name, todayDate, nowTime, managerId);

		EventsPreviewResponse epr = new EventsPreviewResponse();

		epr.setTotPages((eventsPreview.size() / (Constants.pageSize+1)) + 1);
		epr.setPageNumber(pageNumber);

		Integer from = pageNumber * Constants.pageSize;
		from = (from >= 0 && from < eventsPreview.size()) ? from : 0; // check low boundary

		Integer to = (pageNumber + 1) * Constants.pageSize;
		to = (to >= 0 && to < eventsPreview.size()) ? to : eventsPreview.size(); // check high boundaries

		epr.setEventsPreview(eventsPreview.subList(from, to));

		return epr;
	}
	
	@Override
	public EventDetail getEventDetail(Integer managerId, Integer eventId) {
		if (eventId == null || eventId < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(eventRepository.getEventDAOById(eventId) != null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(managerLocationRepository.isManagerEnabled(managerId, eventId) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		EventDetail ed = eventRepository.getEventById(eventId);
		
		if (ed == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String nowTime = LocalDate.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		
		List<EventSlot> es = eventslotRepository.getEventSlotsByEventIdPrivileged(eventId, todayDate, nowTime);
		
		ed.setEventSlots(es);

		return ed;
	}

	@Override
	public void deleteEvent(Integer managerId, Integer eventId, String language) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if(eventId < 0 || eventId == null || !Util.isLanguageValid(language)) 
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		EventDAO event = eventRepository.getEventDAOById(eventId);
		if(event == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(managerLocationRepository.isManagerEnabled(managerId, eventId) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		LocationDAO location = locationRepository.getLocationById(event.getLocationRef());
		
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String nowTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		
		for(EventSlot es : eventslotRepository.getEventSlotsByEventIdPrivileged(eventId, todayDate, nowTime)) {
			List<UserDAO> partecipants = userRepository.getPartecipantsByEventSlotId(es.getId());
			Integer affectedRows = eventslotRepository.updateEventSlot(es.getId(), Boolean.FALSE);
			if(affectedRows == 1 && partecipants.size() != 0) {
				// DELETE Notification
				EmailSystem.sendDeleteEmail(partecipants.stream().map(u -> u.getEmail()).collect(Collectors.toList()),
						es.getDate(), es.getStartTime(), es.getEndTime(), event.getTitle(), event.getTitolo(), location.getName(),
						location.getCity(), location.getAddress(), language);
			}
		}
	}

	@Override
	public void modifyEventImage(Integer managerId, ModifyEventImageRequest request) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		if (request.getFile() == null || request.getName() == null || request.getName().isBlank()
				|| request.getEventId() == null || request.getEventId() < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(eventRepository.getEventDAOById(request.getEventId()) == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if(managerLocationRepository.isManagerEnabled(managerId, request.getEventId()) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		Integer imageId = null;
		try {
			imageRepository.insertImage(request.getFile().getBytes(), request.getFile().getOriginalFilename());
			imageId = imageRepository.getLastId();
		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		EventDAO updated = eventRepository.findById(request.getEventId()).get();
		updated.setImageRef(imageId);
		eventRepository.save(updated);

	}
	
	@Override
	public List<String> getCities(Integer managerId) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		if(managerId <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid manager");
		return managerLocationRepository.getManagerCities(managerId);
	}
	
	@Override
	public List<String> getLocationNames(Integer managerId, String city) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		if(managerId <= 0 || city.isBlank() || city.isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid city");
		
		return managerLocationRepository.getManagerLocationsByCity(managerId, city);
	}

	@Override
	public List<String> getAddressesByCityLocation(Integer managerId, String city, String locationName) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		if(managerId <= 0 || city.isBlank() || city.isEmpty() || locationName.isBlank() || locationName.isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid city/location");
		
		return managerLocationRepository.getManagerAddressesByCityLocation(managerId, city, locationName);
	}

	@Override
	public List<String> getEventsCities(Integer managerId) {
		if (userRepository.checkPrivilegeRole(managerId, Constants.MANAGER) == 0)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		return managerLocationRepository.getEventCities(managerId, todayDate);
	}

}
