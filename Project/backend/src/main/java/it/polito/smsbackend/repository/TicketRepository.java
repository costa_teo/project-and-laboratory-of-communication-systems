package it.polito.smsbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.TicketDAO;
import it.polito.smsbackend.dto.Ticket;


public interface TicketRepository extends CrudRepository<TicketDAO, Integer> {
	
	@Query(value = "SELECT max_people > (SELECT COUNT(*) FROM TICKET t WHERE t.eventslot_ref = :id) AS c "
			+ "FROM EVENT_SLOT es "
			+ "WHERE es.id = :id "
			+ "GROUP BY id", nativeQuery = true)
	Boolean canBookEventSlot(@Param("id") Integer eventSlotId);
	
	@Query(value = "SELECT new it.polito.smsbackend.dto.Ticket(t.id, t.code, e.title, e.titolo, e.id, "
			+ "es.date, es.startTime, es.endTime, l.city, l.address, l.name, t.used, es.valid) "
			+ "FROM TicketDAO t, EventSlotDAO es, LocationDAO l, EventDAO e "
			+ "WHERE t.eventslotRef = es.id AND e.locationRef = l.id AND es.eventRef = e.id "
			+ "AND t.userRef = :userId "
			+ "ORDER BY es.date ", nativeQuery = false)
	List<Ticket> getCustomerTickets(@Param("userId") Integer userId);
	
	@Query(value = "SELECT COUNT(*) FROM TICKET WHERE id = :ticketId", nativeQuery = true)
	int checkIdValidity(@Param("ticketId") Integer ticketId);
	
	@Query(value = "SELECT * FROM TICKET WHERE id = :ticketId", nativeQuery = true)
	TicketDAO getTicketById(Integer ticketId);

	@Query(value = "SELECT COUNT(t.id) FROM TICKET t, EVENT_SLOT es "
			+ "WHERE t.eventslot_ref = es.id AND t.user_ref = :userId "
			+ "AND es.date >= :infLimitDate AND date < :todayDate "
			+ "AND t.used = FALSE AND es.valid = TRUE", nativeQuery = true)
	int getUserPenalities(Integer userId, String infLimitDate, String todayDate);

	@Query(value = "SELECT COUNT(t.id) FROM TICKET t, EVENT_SLOT es "
			+ "WHERE t.eventslot_ref = es.id AND t.user_ref = :userId "
			+ "AND t.eventslot_ref = :eventSlotId", nativeQuery = true)
	int getUserTicketsByEventSlotId(Integer userId, Integer eventSlotId);

	@Query(value = "SELECT t.id "
			+ "FROM TICKET t, EVENT_SLOT es, EVENT e, LOCATION l "
			+ "WHERE t.eventslot_ref = es.id AND e.location_ref = l.id AND es.event_ref = e.id "
			+ "AND t.code = :code AND es.date = :todayDate AND l.id = :locationId "
			+ "AND es.start_time <= :nowTime AND es.end_time >= :nowTime "
			+ "AND t.used = FALSE ", nativeQuery = true)
	List<Integer> canEnter(String todayDate, String nowTime, String code, String locationId);

	@Query(value = "SELECT COUNT(t.id) FROM TICKET t, EVENT_SLOT es "
			+ "WHERE t.eventslot_ref = es.id AND t.code = :barcode "
			+ "AND es.date >= :infLimitDate AND date < :todayDate "
			+ "AND t.used = FALSE AND es.valid = TRUE", nativeQuery = true)
	int getBarcodePenalities(String barcode, String infLimitDate, String todayDate);
	
	@Query(value = "SELECT COUNT(t.id) FROM TICKET t, EVENT_SLOT es "
			+ "WHERE t.eventslot_ref = es.id AND t.code = :barcode "
			+ "AND t.eventslot_ref = :eventSlotId", nativeQuery = true)
	int getBarcodeTicketsByEventSlotId(String barcode, Integer eventSlotId);

	
}
