package it.polito.smsbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.request.LoginRequest;
import it.polito.smsbackend.service.AuthenticationService;

@RestController
@RequestMapping(value = "authentication")
@CrossOrigin( origins = "*" )
public class AuthenticationController {

	private final AuthenticationService authenticationService;

	@Autowired
	public AuthenticationController(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	@PostMapping(value = "login")
	public User login(@RequestBody LoginRequest request) {
		return authenticationService.login(request.getUsername(), request.getPassword());
	}

	@PostMapping(value = "logout")
	public void logout(@RequestBody User user) {
		authenticationService.logout(user);
	}

	@GetMapping(value = "authenticateUser")
	public Integer authenticateUser(@RequestHeader("token") String token) {
		return authenticationService.authenticateUser(token);
	}

	@PostMapping(value = "change-password")
	public void changePassword(@RequestHeader("token") String token, @RequestBody String newPassword) {
		Integer userId = authenticationService.authenticateUser(token);
		authenticationService.changePassword(userId, newPassword);
	}
	
	@PostMapping(value = "fotgot-password")
	public void forgotPassword(@RequestHeader("language") String language, @RequestBody String userEmail) {
		authenticationService.forgotPassword(userEmail, language);
	}
	
	@PostMapping(value = "check-email")
	  public void checkEmail(@RequestBody String userEmail) {
		authenticationService.checkEmail(userEmail);
	  }
	
	
}
