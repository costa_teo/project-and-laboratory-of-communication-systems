package it.polito.smsbackend.common;

public final class Constants {
	
	private Constants() {
		//
	}
	
	public static final String CUSTOMER = "CUSTOMER";
	public static final String MANAGER = "MANAGER";
	public static final String ADMINISTRATOR = "ADMINISTRATOR";
	
	public static final int pageSize = 12;
	
	public static final int QR_WIDTH = 250;
	public static final int QR_HEIGHT = 250;
	
	public static final int MONTHS_NO = 2;
	public static final int HOURS_NO = 1;
	public static final int PENALTIES_LIMIT = 10;
	public static final int TICKETS_LIMIT = 3;
	
	public static final int NO_ACCOUNT_ID = 0;
	public static final String GHOST_ACCOUNT = "*";
	public static final int ACTIVATION_MINUTES = 30;
	
	public static final String ITA = "it";
	public static final String ENG = "en";
	
	public static final int DEFAULT_PASSWORD_LENGTH = 10;
	
	//public static final String BASE_URL = "https://localhost:443";			// DEBUG
	public static final String BASE_URL = "https://plcs2.duckdns.org";	// PROD

}
