package it.polito.smsbackend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.EventDAO;
import it.polito.smsbackend.dto.EventDetail;

public interface EventRepository extends CrudRepository<EventDAO, Integer> {
	
	@Query(value = "SELECT DISTINCT l.city "
			+ "FROM EVENT e, LOCATION l, EVENT_SLOT es "
			+ "WHERE e.location_ref = l.id AND es.event_ref = e.id "
			+ "AND e.id IN ( "
			+ 			"SELECT event_ref "
			+ 			"FROM EVENT_SLOT "
			+ 			"WHERE valid = TRUE AND date >= :todayDate "
			+ 			"GROUP BY event_ref "
			+ 			"HAVING COUNT(*) > 0 "
			+ ")", nativeQuery = true)
	List<String> getEventAllCities(@Param("todayDate") String todayDate);

	@Query(value = "SELECT new it.polito.smsbackend.dto.EventDetail(e.id, e.title, e.description, e.titolo, e.descrizione, l.city, l.address, l.name, e.imageRef) "
			+ "FROM EventDAO e, LocationDAO l "
			+ "WHERE e.id = :eventId AND e.locationRef = l.id", nativeQuery = false)
	EventDetail getEventById(@Param("eventId") Integer eventId);

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO EVENT (id, title, description, titolo, descrizione, image_ref, location_ref) "
			+ "VALUES (event_seq.nextval, :title, :description, :titolo, :descrizione, :imageId, :locationId)", nativeQuery = true)
	void insertEvent(@Param("title") String title, @Param("description") String description,
			@Param("titolo") String titolo, @Param("descrizione") String descrizione, @Param("imageId") Integer imageId,
			@Param("locationId") Integer locationId);

	@Query(value = "SELECT MAX(id) FROM EVENT", nativeQuery = true)
	Integer getLastId();
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE EVENT SET title = :title, description = :description, titolo = :titolo, descrizione = :descrizione, location_ref = :locationId "
			+ "WHERE id = :eventId", nativeQuery = true)
	void updateEvent(@Param("eventId") Integer eventId, @Param("title") String title,
			@Param("description") String description, @Param("titolo") String titolo,
			@Param("descrizione") String descrizione, @Param("locationId") Integer locationId);
	
	@Query(value = "SELECT * FROM EVENT e WHERE e.id = :id", nativeQuery = true)
	EventDAO getEventDAOById(@Param("id") Integer id);

}
