package it.polito.smsbackend.service;

import it.polito.smsbackend.dto.User;

public interface AuthenticationService {
	
	User login(String username, String password);
	
	void logout(User user);
	
	Integer authenticateUser(String token);
	
	void changePassword(Integer userId, String newPassword);

	void forgotPassword(String mail, String language);

	void checkEmail(String userEmail);

}
