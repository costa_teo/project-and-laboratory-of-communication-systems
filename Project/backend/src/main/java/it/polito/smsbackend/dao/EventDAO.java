package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EVENT")
public class EventDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_generator")
	@SequenceGenerator(name = "event_generator", sequenceName = "event_seq", allocationSize = 1)
	private Integer id;
	@Column(nullable = true, name = "image_ref")
	private Integer imageRef;
	@Column(nullable = false, name = "location_ref")
	private Integer locationRef;
	@Column(nullable = false, length = 5000)
	private String description;
	@Column(nullable = false)
	private String title;
	@Column(nullable = false, length = 5000)
	private String descrizione;
	@Column(nullable = false)
	private String titolo;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getImageRef() {
		return imageRef;
	}

	public void setImageRef(Integer imageRef) {
		this.imageRef = imageRef;
	}

	public Integer getLocationRef() {
		return locationRef;
	}

	public void setLocationRef(Integer locationRef) {
		this.locationRef = locationRef;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

}
