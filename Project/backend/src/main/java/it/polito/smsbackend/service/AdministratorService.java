package it.polito.smsbackend.service;

import java.util.List;

import it.polito.smsbackend.dto.Location;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.request.AddNewEventRequest;
import it.polito.smsbackend.request.AddNewLocationRequest;
import it.polito.smsbackend.request.GetUserRequest;
import it.polito.smsbackend.request.ManagerLocationRequest;
import it.polito.smsbackend.request.ModifyEventImageRequest;
import it.polito.smsbackend.response.GetManagerInfoResponse;
import it.polito.smsbackend.response.GetUserResponse;

public interface AdministratorService {

	void changeUserRole(Integer adminId, User user);

	GetUserResponse getUsers(Integer adminId, GetUserRequest request);

	Integer addNewEvent(Integer adminId, AddNewEventRequest request);

	void modifyEventImage(Integer adminId, ModifyEventImageRequest request);

	void modifyEvent(Integer adminId, AddNewEventRequest request, String language);

	void deleteEvent(Integer adminId, Integer eventId, String language);

	List<Location> getLocationsByCity(Integer adminId, String city);

	void addNewLocation(Integer adminId, AddNewLocationRequest request);

	List<String> getAllCities(Integer adminId);
	
	List<String> getValidCities(Integer adminId);
	
	List<String> getLocationNamesByCity(Integer adminId, String city);
	
	List<String> getAddressesByCityAndLocation(Integer adminId, String city, String location);

	void removeManagerLocationRequest(Integer adminId, ManagerLocationRequest request);

	void addManagerLocationRequest(Integer adminId, ManagerLocationRequest request);

	GetManagerInfoResponse getManagerInfo(Integer adminId, Integer managerId);

	void changeLocationValidity(Integer adminId, Location request);

}
