package it.polito.smsbackend.emailsystem;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.servlet.ServletOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.sun.mail.smtp.SMTPTransport;

import it.polito.smsbackend.common.Constants;

public class EmailSystem {

	private static Session session;
	private static Transport transport;

	private EmailSystem() {
		//
	}

	public static void connect() {
		Properties properties = new Properties();

		properties.put("mail.smtp.auth", true);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		String username = "softengproject1@gmail.com";
		String segreto = "softengproject123";
		
		session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, segreto);
			}
		});
		
		try {
			transport = (SMTPTransport) session.getTransport("smtp");
			transport.connect("smtp.gmail.com", null, null);
		} catch (MessagingException e) {
			//
		}

	}

	public static void sendTicketEmail(String mail, String name, String surname, String eventTitle, String eventoTitolo, String locationName, String city, String address, String date,
			String startTime, String endTime, String ticketCode, String language) {

		if (session == null || !transport.isConnected())
			connect();
		
		int i = 3;
		while (i > 0) {
			i--;
			Message message = new MimeMessage(session);
			try {
				message.setFrom(new InternetAddress("no-reply.plcs@mail.com"));

				String[] splittedDate = date.split("/");
				String qrCode = crateQRCode(ticketCode, Constants.QR_WIDTH, Constants.QR_HEIGHT);
				final String br = "<br/>";
				String msg;
				
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
				
				if(Constants.ENG.equals(language)) {
					message.setSubject("Confirmation Event Reservation");
					msg = "Dear " + name + " " + surname + ", " + br
						+ "Congratulations, you correctly book a ticket for " + eventTitle + "!" + br
						+ "The event will be take place at " + locationName + " (" + address + ", " + city + ") "
						+ "in date " + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + " "
						+ "with ingress time between " + startTime + " - " + endTime + "." + br
						+ "Down below there is the QRCode you need to scan in order to access the event." + br + br;
				}
				else {
					message.setSubject("Conferma Prenotazione Evento");
					msg = "Gentile " + name + " " + surname + ", " + br
						+ "Congratulazioni, hai correttamente prenotato un biglietto per " + eventoTitolo + "!" + br
						+ "L'evento avr&agrave; luogo a " + locationName + " (" + address + ", " + city + ") "
						+ "in data " + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + " "
						+ "con orario di ingresso " + startTime + " - " + endTime + "." + br
						+ "In questa stessa mail &egrave; presente il QRCode da scannerizzare al gate per accedere all'evento." + br + br;
				}
				msg += "<p align=center><img src=\"cid:image\"> </p>";

			    MimeMultipart mmp = new MimeMultipart();
			    BodyPart bp = new MimeBodyPart();
			    bp.setContent(msg, "text/html" );
			    mmp.addBodyPart(bp);
			    
			    MimeBodyPart pmp = new PreencodedMimeBodyPart( "base64" );
				
			    pmp.setHeader( "Content-ID", "<image>" );
				pmp.setDisposition( MimeBodyPart.INLINE );
				pmp.setContent(qrCode, "image/png");
				
				mmp.addBodyPart(pmp);
				
				message.setContent(mmp);
				
				transport.sendMessage(message, message.getAllRecipients());
				break;

			} catch (MessagingException e) {
				if (i <= 0)
					throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
				else {
					EmailSystem.connect();
				}
			} catch(IOException e) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	public static void sendTicketEmailNoAccount(String mail, String eventTitle, String eventoTitolo, String locationName, String city,
			String address, String date, String startTime, String endTime, String ticketCode, String language) {
		if (session == null || !transport.isConnected())
			connect();
		
		int i = 3;
		while (i > 0) {
			i--;
			Message message = new MimeMessage(session);
			try {
				message.setFrom(new InternetAddress("no-reply.plcs@mail.com"));
				
				String[] splittedDate = date.split("/");
				String qrCode = crateQRCode(ticketCode, Constants.QR_WIDTH, Constants.QR_HEIGHT);
				final String br = "<br/>";
				String msg;

				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
				
				if(Constants.ENG.equals(language)) {
					message.setSubject("Confirmation Event Reservation");
					msg = "Dear Customer, " + br
						+ "Congratulations, you correctly book a ticket for " + eventTitle + "!" + br
						+ "The event will be take place at " + locationName + " (" + address + ", " + city + ") "
						+ "in date " + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + " "
						+ "with ingress time between " + startTime + " - " + endTime + "." + br
						+ "Down below there is the QRCode you need to scan in order to access the event." + br + br;
				}
				else {
					message.setSubject("Conferma Prenotazione Evento");
					msg = "Gentile Cliente, " + br
						+ "Congratulazioni, hai correttamente prenotato un biglietto per " + eventoTitolo + "!" + br
						+ "L'evento avr&agrave; luogo a " + locationName + " (" + address + ", " + city + ") "
						+ "in data " + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + " "
						+ "con orario di ingresso " + startTime + " - " + endTime + "." + br
						+ "In questa stessa mail &egrave; presente il QRCode da scannerizzare al gate per accedere all'evento." + br + br;
				}
				msg += "<p align=center><img src=\"cid:image\"> </p>";

			    MimeMultipart mmp = new MimeMultipart();
			    BodyPart bp = new MimeBodyPart();
			    bp.setContent(msg, "text/html" );
			    mmp.addBodyPart(bp);
			    
			    MimeBodyPart pmp = new PreencodedMimeBodyPart( "base64" );
				
			    pmp.setHeader( "Content-ID", "<image>" );
				pmp.setDisposition( MimeBodyPart.INLINE );
				pmp.setContent(qrCode, "image/png");
				
				mmp.addBodyPart(pmp);
				
				message.setContent(mmp);
				
				transport.sendMessage(message, message.getAllRecipients());
				break;

			} catch (MessagingException e) {
				if (i <= 0)
					throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
				else {
					EmailSystem.connect();
				}
			} catch(IOException e) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}	
	}
	
	public static void sendDeleteEmail(List<String> partecipantsEmail, String date, String startTime, String endTime,
			String eventTitle, String eventoTitolo, String locationName, String city, String address, String language) {
		
		if (session == null || !transport.isConnected())
			connect();
		for(String mail : partecipantsEmail) {
			int i = 3;
			String[] splittedDate = date.split("/");
			final String br = "<br/>";
			String msg;
			while (i > 0) {
				i--;
				Message message = new MimeMessage(session);
				try {
					message.setFrom(new InternetAddress("no-reply.plcs@mail.com"));
				
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
					
					if(Constants.ENG.equals(language)) {
						message.setSubject("Deletion Event Reservation");
						msg = "Dear Customer, " + br
							+ "The event you booked for has been cancelled by the organizers." + br
							+ "Down below you can find all the details of the deleted event, please "
							+ "consider to book another event slot if it's available."+ br
							+ "We apologize for the inconvenience." + br + br
							+ "<b>Event:</b> " + eventTitle + br
							+ "<b>Location: </b>" + locationName + br 
							+ "<b>Address: </b>" + address + ", " + city + br
							+ "<b>Date: </b>" + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + br
							+ "<b>Slot: </b>" + startTime + " - " + endTime;
					}
					else {
						message.setSubject("Cancellazione Prenotazione Evento");
						msg = "Gentile Cliente, " + br
							+ "L'evento da te prenotato &egrave; stato cancellato dagli organizzatori." + br
							+ "In seguito puoi trovare tutti i dettagli dell'evento cancellato, considera la possbilit&agrave; "
							+ "di prenotare un altro slot se disponibile."+ br
							+ "Ci scusiamo per l'inconveniente." + br + br
							+ "<b>Evento:</b> " + eventoTitolo + br
							+ "<b>Location: </b>" + locationName + br 
							+ "<b>Indirizzo: </b>" + address + ", " + city + br
							+ "<b>Data: </b>" + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + br
							+ "<b>Slot: </b>" + startTime + " - " + endTime;
					}
					
					MimeMultipart mmp = new MimeMultipart();
					BodyPart bp = new MimeBodyPart();
					bp.setContent(msg, "text/html" );
					mmp.addBodyPart(bp);
					message.setContent(mmp);
					
					transport.sendMessage(message, message.getAllRecipients());
					break;

				} catch (MessagingException e) {
					if (i <= 0)
						throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
					else {
						EmailSystem.connect();
					}
				}
			}
		}	
	}
	
	public static void sendRestoreEmail(List<String> partecipantsEmail, String date, String startTime, String endTime,
			String eventTitle, String eventoTitolo, String locationName, String city, String address, String language) {
		if (session == null || !transport.isConnected())
			connect();
		for(String mail : partecipantsEmail) {
			int i = 3;
			String[] splittedDate = date.split("/");
			final String br = "<br/>";
			String msg;
			while (i > 0) {
				i--;
				Message message = new MimeMessage(session);
				try {
					message.setFrom(new InternetAddress("no-reply.plcs@mail.com"));
					
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
					
					if(Constants.ENG.equals(language)) {
						message.setSubject("Restored Event Reservation");
						msg = "Dear Customer, " + br
							+ "The event you booked for has been restored by the organizers." + br
							+ "Down below you can find all the details of the updated event, please "
							+ "consider to delete your reservation if you plan to not partecipate."+ br
							+ "We apologize for the inconvenience." + br + br
							+ "<b>Event:</b> " + eventTitle + br
							+ "<b>Location: </b>" + locationName + br 
							+ "<b>Address: </b>" + address + ", " + city + br
							+ "<b>Date: </b>" + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + br
							+ "<b>Slot: </b>" + startTime + " - " + endTime;
					}
					else {
						message.setSubject("Riattivazione Prenotazione Evento");
						msg = "Gentile Cliente, " + br
							+ "L'evento da te prenotato &egrave; stato riattivato dagli organizzatori." + br
							+ "In seguito puoi trovare tutti i dettagli dell'evento aggiornato, considera la possbilit&agrave; "
							+ "di prenotare un altro slot se disponibile."+ br
							+ "Ci scusiamo per l'inconveniente." + br + br
							+ "<b>Evento:</b> " + eventoTitolo + br
							+ "<b>Location: </b>" + locationName + br 
							+ "<b>Indirizzo: </b>" + address + ", " + city + br
							+ "<b>Data: </b>" + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + br
							+ "<b>Slot: </b>" + startTime + " - " + endTime;
					}
					
					MimeMultipart mmp = new MimeMultipart();
					BodyPart bp = new MimeBodyPart();
					bp.setContent(msg, "text/html" );
					mmp.addBodyPart(bp);
					message.setContent(mmp);
					
					transport.sendMessage(message, message.getAllRecipients());
					break;

				} catch (MessagingException e) {
					if (i <= 0)
						throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
					else {
						EmailSystem.connect();
					}
				}
			}
		}	
	}
	
	public static void sendUpdateLocationMail(List<String> partecipantsEmail, String date, String startTime, String endTime,
			String eventTitle, String eventoTitolo, String locationName, String city, String address, String locationNameOld, String cityOld,
			String addressOld, String language) {
		if (session == null || !transport.isConnected())
			connect();
		for(String mail : partecipantsEmail) {
			int i = 3;
			String[] splittedDate = date.split("/");
			final String br = "<br/>";
			String msg;
			while (i > 0) {
				i--;
				Message message = new MimeMessage(session);
				try {
					message.setFrom(new InternetAddress("no-reply.plcs@mail.com"));
					
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
					
					if(Constants.ENG.equals(language)) {
						message.setSubject("Modification Event Location");
						msg = "Dear Customer, " + br
							+ "The location of the event you booked for has been modified by the organizers." + br
							+ "Down below you can find all the details of the updated event, please "
							+ "consider to delete your reservation if you plan to not partecipate."+ br
							+ "We apologize for the inconvenience." + br + br
							+ "<b>Event:</b> " + eventTitle + br
							+ "<b>Location: </b>" + locationName + br 
							+ "<b>Address: </b>" + address + ", " + city + br
							+ "<b>Date: </b>" + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + br
							+ "<b>Slot: </b>" + startTime + " - " + endTime;
					}
					else {
						message.setSubject("Modifica Location Evento");
						msg = "Gentile Cliente, " + br
							+ "La location dell'evento da te prenotato &egrave; stata cambiata dagli organizzatori" + br
							+ "In seguito puoi trovare tutti i dettagli dell'evento cancellato, considera la possbilit&agrave; "
							+ "di cancellare l'evento se non hai intenzione di partecipare."+ br
							+ "Ci scusiamo per l'inconveniente." + br + br
							+ "<b>Evento:</b> " + eventoTitolo + br
							+ "<b>Location: </b>" + locationName + br 
							+ "<b>Indirizzo: </b>" + address + ", " + city + br
							+ "<b>Data: </b>" + splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0] + br
							+ "<b>Slot: </b>" + startTime + " - " + endTime;
					}
					
					MimeMultipart mmp = new MimeMultipart();
					BodyPart bp = new MimeBodyPart();
					bp.setContent(msg, "text/html" );
					mmp.addBodyPart(bp);
					message.setContent(mmp);
					
					transport.sendMessage(message, message.getAllRecipients());
					break;

				} catch (MessagingException e) {
					if (i <= 0)
						throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
					else {
						EmailSystem.connect();
					}
				}
			}
		}	
	}
	
	public static void sendResetPasswordMail(String mail, String password, String language) {
		if (session == null || !transport.isConnected())
			connect();

		int i = 3;
		final String br = "<br/>";
		String msg;
		while (i > 0) {
			i--;
			Message message = new MimeMessage(session);
			try {
				message.setFrom(new InternetAddress("no-reply.plcs@mail.com"));

				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
				message.setSubject("Reset Password");
				if (Constants.ENG.equals(language)) {
					msg = "Dear Customer, " + br
						+ "Your password has been resetted." + br
						+ "You will be able to access your account with the automatically generated one, "
						+ "we strongly suggest to change it as soon as possible"  + br + br
						+ "New Password " + password;
				}
				else {
					msg = "Gentile Cliente, " + br
						+ "La sua password &egrave; stata resetteta." + br
						+ "Potr&agrave; accedere con quella generata autaticamente dal sistema, "
						+ "Le consgliamo caldamente di cambiarla il prima possibile con una da Lei scelta" + br + br
						+ "Nuova Passowrd: " + password;
				}

				MimeMultipart mmp = new MimeMultipart();
				BodyPart bp = new MimeBodyPart();
				bp.setContent(msg, "text/html");
				mmp.addBodyPart(bp);
				message.setContent(mmp);

				transport.sendMessage(message, message.getAllRecipients());
				break;

			} catch (MessagingException e) {
				if (i <= 0)
					throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
				else {
					EmailSystem.connect();
				}
			}
		}
	}
	
	public static void sendFinalizeAccount(String mail, String signHash, String language) {
		if (session == null || !transport.isConnected())
			connect();

		int i = 3;
		final String br = "<br/>";
		String msg, link;
		while (i > 0) {
			i--;
			Message message = new MimeMessage(session);
			try {
				message.setFrom(new InternetAddress("no-reply.plcs@mail.com"));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
				if (Constants.ENG.equals(language)) {
					message.setSubject("Activate Account");
					link = "Activate!";
					msg = "Dear Customer, " + br
						+ "This is the activation mail for your account." + br
						+ "Please click on the link below, in order to verificate the email address and complete the registration process. " + br
						+ "The link is valid for 30 minutes after that you have to repeat the whole procedure."  + br 
						+ "Thanks for choosing PLCS Events! "+ br + br;
				}
				else {
					message.setSubject("Attivazione Account");
					link = "Attiva!";
					msg = "Gentile Cliente, " + br
						+ "Questa &egrave; la mail per attivare il Suo account." + br
						+ "Clicchi sul link sottostante per verificare la Sua mail e completare il processo di registrazione." + br
						+ "Il link &egrave; per 30 minuti dopodich&egrave; dovr&agrave; ripetere tutta la procedura. " + br 
						+ "Grazie per aver scelto PLCS Eventi! "+ br + br;
				}
				
				msg += String.format("<a href='%s/user/activate/%s'> %s </a>", Constants.BASE_URL, signHash, link);

				MimeMultipart mmp = new MimeMultipart();
				BodyPart bp = new MimeBodyPart();
				bp.setContent(msg, "text/html");
				mmp.addBodyPart(bp);
				message.setContent(mmp);

				transport.sendMessage(message, message.getAllRecipients());
				break;

			} catch (MessagingException e) {
				if (i <= 0)
					throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
				else {
					EmailSystem.connect();
				}
			}
		}
		
	}
	
	private static String crateQRCode(String content, int width, int height) throws IOException {

		String resultImage;
		if (!StringUtils.isEmpty(content)) {
			ServletOutputStream stream = null;
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			@SuppressWarnings("rawtypes")
			HashMap<EncodeHintType, Comparable> hints = new HashMap<>();
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); 					// Specify the character encoding as "utf-8"
			hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M); // Specify the error correction level of
																				// the QR code to be intermediate
			hints.put(EncodeHintType.MARGIN, 2); // Set the margins of the image

			try {
				QRCodeWriter writer = new QRCodeWriter();
				BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height, hints);

				BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
				ImageIO.write(bufferedImage, "png", os);
				/*
				 * There is no data:image/png;base64 in front of the original transcoding. These
				 * fields are returned to the front end and cannot be parsed. You can add the
				 * front end or add it below.
				 */
				resultImage = Base64.encodeBase64String(os.toByteArray());
				return resultImage;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (stream != null) {
					stream.flush();
					stream.close();
				}
			}
		}
		return null;
	}


}
