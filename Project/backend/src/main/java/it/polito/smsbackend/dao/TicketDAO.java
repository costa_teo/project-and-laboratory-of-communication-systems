package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TICKET")
public class TicketDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticket_generator")
	@SequenceGenerator(name = "ticket_generator", sequenceName = "ticket_seq", allocationSize = 1)
	private Integer id;
	@Column(nullable = false, name = "eventslot_ref")
	private Integer eventslotRef;
	@Column(nullable = false, name = "user_ref")
	private Integer userRef;
	@Column(nullable = false)
	private Boolean used;
	@Column(nullable = false)
	private String code;

	public Integer getEventslotRef() {
		return eventslotRef;
	}

	public void setEventslotRef(Integer eventslotRef) {
		this.eventslotRef = eventslotRef;
	}

	public Integer getUserRef() {
		return userRef;
	}

	public void setUserRef(Integer userRef) {
		this.userRef = userRef;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
