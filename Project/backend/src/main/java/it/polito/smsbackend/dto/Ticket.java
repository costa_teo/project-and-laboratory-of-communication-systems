package it.polito.smsbackend.dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Ticket {

	private Integer id;
	private Boolean valid;
	private String eventTitle;
	private String eventTitolo;
	private Integer eventId;
	private String date;
	private String startTime;
	private String endTime;
	private String city;
	private String address;
	private String locationName;
	private String ticketCode;
	
	public Ticket(Integer id, String ticketCode, String eventTitle, String eventTitolo, Integer eventId, String date, String startTime, String endTime,
			String city, String address, String locationName, Boolean ticketUsed, Boolean slotValid) {
		this.id = id;
		this.eventTitle = eventTitle;
		this.eventTitolo = eventTitolo;
		this.eventId = eventId;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.city = city;
		this.address = address;
		this.locationName = locationName;
		this.ticketCode = ticketCode;
		
		LocalDate eventDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		LocalDate nowDate = LocalDate.now();
		
		// TODO Time Check
		
		if(slotValid == false || ticketUsed == true || nowDate.isAfter(eventDate))
			this.valid = false;
		else
			this.valid = true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartDate(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void getEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTicketCode() {
		return ticketCode;
	}

	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getEventTitolo() {
		return eventTitolo;
	}

	public void setEventTitolo(String eventTitolo) {
		this.eventTitolo = eventTitolo;
	}
	
	

}
