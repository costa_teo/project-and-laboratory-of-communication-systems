package it.polito.smsbackend.request;

import java.util.List;

import it.polito.smsbackend.dto.EventSlot;

public class AddNewEventRequest {

	private Integer id;
	private String title;
	private String description;
	private String titolo;
	private String descrizione;
	private String city;
	private String address;
	private String locationName;
	private List<EventSlot> eventSlots;

	public AddNewEventRequest(Integer id, String title, String description, String titolo, String descrizione,
			List<EventSlot> eventSlots) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.titolo = titolo;
		this.descrizione = descrizione;
		this.eventSlots = eventSlots;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return "AddNewEventRequest [id=" + id + ", title=" + title + ", description=" + description + ", titolo="
				+ titolo + ", descrizione=" + descrizione + ", city=" + city + ", address=" + address
				+ ", locationName=" + locationName + ", eventSlots=" + eventSlots + "]";
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public List<EventSlot> getEventSlots() {
		return eventSlots;
	}

	public void setEventSlots(List<EventSlot> eventSlots) {
		this.eventSlots = eventSlots;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

}
