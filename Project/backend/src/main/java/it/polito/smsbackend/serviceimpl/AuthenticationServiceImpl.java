package it.polito.smsbackend.serviceimpl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.apache.commons.lang3.RandomStringUtils;

import it.polito.smsbackend.common.Constants;
import it.polito.smsbackend.common.Util;
import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.emailsystem.EmailSystem;
import it.polito.smsbackend.repository.UserRepository;
import it.polito.smsbackend.service.AuthenticationService;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	private final UserRepository userRepository;
	
	@Autowired
	public AuthenticationServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public User login(String email, String password) {
		//MOCK
		//Improve token generation if you want
		//Right now, token corresponds to its own duration (20 minutes)
		LocalDateTime date = LocalDateTime.now().plusMinutes(20);
		
		if(!Util.isMailValid(email) || userRepository.isGhostAccount(email.toLowerCase()) == 1) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		UserDAO uDAO = this.userRepository.getUserByCredentials(email.toLowerCase(), password);
		if(uDAO == null || Constants.GHOST_ACCOUNT.equals(uDAO.getName()) || Constants.GHOST_ACCOUNT.equals(uDAO.getSurname())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		User u = uDAO.map();
		
		u.setToken(u.getId()+"_" + date.toString()+"_" +"FALSE");
		return u;
	}

	@Override
	public void logout(User user) {
		//
	}

	@Override
	public Integer authenticateUser(String token) {
		if (token.isEmpty() || !token.contains("_"))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

		Integer userId = Integer.valueOf(token.split("_")[0]);
		/*
		LocalDateTime date = LocalDateTime.parse(token.split("_")[1]);
		if (date.isBefore(LocalDateTime.now())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		} else {
			return userId;
		}
		*/
		return userId;
	}

	@Override
	public void changePassword(Integer userId, String newPassword) {
		if (userId == null || userId <= 0) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		
		UserDAO u = userRepository.getUserById(userId);
		
		if(u == null || Constants.GHOST_ACCOUNT.equals(u.getName()) || Constants.GHOST_ACCOUNT.equals(u.getSurname()))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		this.userRepository.changePassword(userId, newPassword);
	}

	@Override
	public void forgotPassword(String mail, String language) {
		if(!Util.isMailValid(mail) || !Util.isLanguageValid(language))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		if(userRepository.existsByEmail(mail.toLowerCase()) != 1)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		if(userRepository.isGhostAccount(mail.toLowerCase()) == 1)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		
		String newPassword = RandomStringUtils.randomAlphanumeric(Constants.DEFAULT_PASSWORD_LENGTH);
		
		UserDAO u = userRepository.getUserByCredentials(mail.toLowerCase(), "%");
		u.setPassword(newPassword);
		
		EmailSystem.sendResetPasswordMail(mail.toLowerCase(), newPassword, language);
	
		userRepository.save(u);
	}

	@Override
	public void checkEmail(String userEmail) {
		if(!Util.isMailValid(userEmail))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		if(userRepository.existsByEmail(userEmail.toLowerCase()) == 1)
			if(userRepository.isGhostAccount(userEmail.toLowerCase()) == 0)
				throw new ResponseStatusException(HttpStatus.CONFLICT);
	}

}
