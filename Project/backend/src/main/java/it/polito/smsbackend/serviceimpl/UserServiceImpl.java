package it.polito.smsbackend.serviceimpl;
import java.sql.Blob;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.common.Constants;
import it.polito.smsbackend.common.Util;
import it.polito.smsbackend.dao.EventDAO;
import it.polito.smsbackend.dao.EventSlotDAO;
import it.polito.smsbackend.dao.LocationDAO;
import it.polito.smsbackend.dao.TicketDAO;
import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.EventDetail;
import it.polito.smsbackend.dto.EventPreview;
import it.polito.smsbackend.dto.EventSlot;
import it.polito.smsbackend.dto.Ticket;
import it.polito.smsbackend.emailsystem.EmailSystem;
import it.polito.smsbackend.repository.ImageRepository;
import it.polito.smsbackend.repository.LocationRepository;
import it.polito.smsbackend.repository.EventRepository;
import it.polito.smsbackend.repository.EventSlotRepository;
import it.polito.smsbackend.repository.TicketRepository;
import it.polito.smsbackend.repository.UserRepository;
import it.polito.smsbackend.request.BookEventTotemRequest;
import it.polito.smsbackend.request.EventsPreviewRequest;
import it.polito.smsbackend.request.RegisterRequest;
import it.polito.smsbackend.response.EventsPreviewResponse;
import it.polito.smsbackend.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	private final ImageRepository attachmentRepository;
	private final EventSlotRepository eventslotRepository;
	private final EventRepository eventRepository;
	private final TicketRepository ticketRepository;
	private final LocationRepository locationRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository, 
			ImageRepository attachmentRepository,
			EventSlotRepository eventslotRepository,
			EventRepository eventRepository,
			TicketRepository ticketRepository, 
			LocationRepository locationRepository) {
		this.userRepository = userRepository;
		this.attachmentRepository = attachmentRepository;
		this.eventslotRepository = eventslotRepository;
		this.eventRepository = eventRepository;
		this.ticketRepository = ticketRepository;
		this.locationRepository = locationRepository;
	}
	
	@Override
	public List<String> getEventsCities() {
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		return eventRepository.getEventAllCities(todayDate);
	}

	@Override
	public Blob getEventImage(Integer imageId) {
		if (imageId == null || imageId < 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		Blob b = attachmentRepository.getAttachmentById(imageId);

		if (b == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		return b;
	}
	
	@Override
	public EventsPreviewResponse getEventsByCityDateName(EventsPreviewRequest request) {

		String city = Objects.toString(request.getCity(), "%");
		String date = Objects.toString(request.getDate(), "%");
		String name = Objects.toString(request.getEventName(), "%");

		city = (city.isBlank()) ? "%" : city;
		date = (date.isBlank()) ? "%" : date;
		name = (name.isBlank()) ? "%" : "%"+name+"%";
		
		Integer pageNumber = (request.getPage() != null && request.getPage() >= 0)? request.getPage() : 0;
		
		List<EventPreview> eventsPreview = null;

		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String nowTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		
		eventsPreview = eventslotRepository.getEventsByCityDateName(city, date, name, todayDate, nowTime);
	
		EventsPreviewResponse epr = new EventsPreviewResponse();

		epr.setTotPages((eventsPreview.size() / (Constants.pageSize+1)) + 1);
		epr.setPageNumber(pageNumber);

		Integer from = pageNumber * Constants.pageSize;
		from = (from >= 0 && from < eventsPreview.size()) ? from : 0; // check low boundary

		Integer to = (pageNumber + 1) * Constants.pageSize;
		to = (to >= 0 && to < eventsPreview.size()) ? to : eventsPreview.size(); // check high boundaries

		epr.setEventsPreview(eventsPreview.subList(from, to));

		return epr;
	}

	@Override
	public EventDetail getEventDetail(Integer eventId) {
		if (eventId == null || eventId < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		EventDetail ed = eventRepository.getEventById(eventId);
		
		if (ed == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String nowTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		
		List<EventSlot> es = eventslotRepository.getEventSlotsByEventIdUsers(eventId, todayDate, nowTime);
		ed.setEventSlots(es);

		return ed;
	}

	@Override
	public void bookEventSlot(Integer userId, Integer eventSlotId, String language) {
		if (userId == null || userId <= 0 || eventSlotId == null || eventSlotId < 0 || !Util.isLanguageValid(language))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		UserDAO u = userRepository.getUserById(userId);
		EventSlotDAO es = eventslotRepository.getEventSlotById(eventSlotId);

		if(u == null || es == null || Constants.GHOST_ACCOUNT.equals(u.getName()) || Constants.GHOST_ACCOUNT.equals(u.getSurname()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		/* Penalty is checked between todayDate and infLimitDate */ 
		String infLimitDate = LocalDate.now().minusMonths(Constants.MONTHS_NO).format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		
		if(es.getDate().compareTo(todayDate) < 0)
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Past event");
		
		if(ticketRepository.getUserPenalities(userId, infLimitDate, todayDate) >= Constants.PENALTIES_LIMIT)
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Limit of penalties reached");
		
		if(ticketRepository.getUserTicketsByEventSlotId(userId, eventSlotId) >= Constants.TICKETS_LIMIT)
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Limit of tickets reached");
		
		if (ticketRepository.canBookEventSlot(eventSlotId)) {
			TicketDAO t = new TicketDAO();
			EventDAO e = eventRepository.getEventDAOById(es.getEventRef());
			LocationDAO l = locationRepository.getLocationById(e.getLocationRef());
			t.setEventslotRef(eventSlotId);
			t.setUsed(false);
			t.setUserRef(userId);
			t.setCode(UUID.randomUUID().toString());
			ticketRepository.save(t);
			EmailSystem.sendTicketEmail(u.getEmail(), u.getName(), u.getSurname(), e.getTitle(), e.getTitolo(), l.getName(),
					l.getCity(), l.getAddress(), es.getDate(), es.getStartTime(), es.getEndTime(), t.getCode(), language);
			return;
		} else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Tickets sold out");
		}
	}

	@Override
	public List<Ticket> getCustomerTickets(Integer userId) {
		if (userId == null || userId <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request");
		
		if(userRepository.getUserById(userId) == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User doesn't exist");
		
		return ticketRepository.getCustomerTickets(userId);
	}

	@Override
	public void registerUser(RegisterRequest request, String language) {
		String email = Objects.toString(request.getEmail(), "").toLowerCase();
		String password = Objects.toString(request.getPassword(), "");
		String name = Objects.toString(request.getName(), "");
		String surname = Objects.toString(request.getSurname(), "");
		String cellNo = Objects.toString(request.getCellNo(), "");
		
		UserDAO u;
		
		/* One or more fields are missing */
		if (email.isBlank() || password.isBlank() || name.isBlank() || surname.isBlank() || !Util.isNSValid(name)
				|| !Util.isNSValid(surname) || cellNo.isBlank() || !Util.isMailValid(email) || !Util.isLanguageValid(language))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request");
		
		if(userRepository.existsByEmail(email) != 0) {
			if(userRepository.isGhostAccount(email) == 1) {
				/* Ghost account */
				u = userRepository.getUserByEmail(email);
			}
			else {
				/* Email already used */
				throw new ResponseStatusException(HttpStatus.CONFLICT, "Email already used");
			}
		}
		else {
			/* Email never used */
			u = new UserDAO();
		}
		
		LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
		String sign = now.toString() + "#" + name + "#" + surname + "#" + cellNo;
		String signHash = DigestUtils.sha256Hex(sign);
		
		u.setEmail(email);
		u.setPassword(password);
		u.setName("*");
		u.setSurname("*");
		u.setCellNo(sign);
		u.setRole(signHash);
		u = userRepository.save(u);
		u.getId();
		EmailSystem.sendFinalizeAccount(email, u.getId()+"_"+signHash, language);
	}
	
	@Override
	public void activateAccount(String signHash) {
		if (signHash.isBlank() || signHash.isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Empty/Blank sign");
		
		String[] parsedSignHash = signHash.split("_");
		
		if(parsedSignHash.length != 2)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid sign");
		
		UserDAO u = userRepository.getGhostUserById(Integer.valueOf(parsedSignHash[0]));
		
		if(u == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request (user doesn't exist)");
		else if(userRepository.isGhostAccount(u.getEmail()) != 1)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Account already active");
		
		String[] storedFields = u.getCellNo().split("#");
		String storedSign = u.getRole();
		LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
		
		if(storedFields.length != 4 || storedSign.isBlank() || storedSign.isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request (internal error)");
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		LocalDateTime sendTime = LocalDateTime.parse(storedFields[0], formatter);
		
		if(!storedSign.equals(parsedSignHash[1]))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid hash");
		else if (sendTime.plusMinutes(Constants.ACTIVATION_MINUTES).isBefore(now))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Expired link");
		
		u.setName(storedFields[1]);
		u.setSurname(storedFields[2]);
		u.setCellNo(storedFields[3]);
		u.setRole(Constants.CUSTOMER);
		userRepository.save(u);
	}

	@Override
	public void cancelTicket(Integer userId, Integer ticketId) {
		if (ticketId == null || ticketId < 0 || userId == null || userId <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request");
		
		if(userRepository.getUserById(userId) == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User doesn't exist");
		
		if(ticketRepository.checkIdValidity(ticketId) != 1)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Ticket deosn't exist");
		
		TicketDAO t = ticketRepository.getTicketById(ticketId);
		EventSlotDAO es = eventslotRepository.getEventSlotById(t.getEventslotRef());
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
		LocalDateTime date = LocalDateTime.parse(es.getDate()+" "+es.getStartTime(), formatter);
		LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
		
		if(now.plusHours(Constants.HOURS_NO).isAfter(date))
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "100"); //100 = error message number for translation
		
		ticketRepository.deleteById(ticketId);
	}

	@Override
	public Boolean checkTicket(String ticketCode) {
		if(ticketCode == null || ticketCode.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request");
		
		/* Expected from gate: codeUUID@locationId */
		String[] splittedTicket = ticketCode.split("@");
		if(splittedTicket.length != 2)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request");
		
		String code = splittedTicket[0];
		String locationId = splittedTicket[1];
		
		if(code == null || code.isBlank() || locationId == null || locationId.isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request");
		
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String nowTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
		
		/* List because more than one ticket can be purchased with the same account/barcode */
		List<Integer> ticketIds = ticketRepository.canEnter(todayDate, nowTime, code, locationId); 
		
		if(!ticketIds.isEmpty()) {
			TicketDAO t = ticketRepository.getTicketById(ticketIds.get(0));
			t.setUsed(Boolean.TRUE);
			ticketRepository.save(t);
			return Boolean.TRUE;
		}
			
		return Boolean.FALSE;
	}

	@Override
	public Ticket bookEventSlotNoAccount(BookEventTotemRequest request, String language) {
		if (request.getEventSlotId() == null || request.getEventSlotId() < 0 || !Util.isLanguageValid(language))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		EventSlotDAO es = eventslotRepository.getEventSlotById(request.getEventSlotId());

		if(es == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		String data = Objects.toString(request.getData(), "");
		
		if(data.isEmpty()) {
			// PHOTO
			return bookTotemPhoto(es);
		}
		else if(Util.isMailValid(data)) {
			// MAIL
			return bookTotemMail(data, es, language);
		}
		else if(!data.matches("([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})")) {
			// FIDELITY CARD (BARCODE OR QRCODE)
			return bookTotemBarcode(data, es);
		}
		else {
			// TRYING TO BUY TICKET WITH ANOTHER TICKET
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid qrcode or barcode");
		}
		
	}
	
	
	private Ticket bookTotemPhoto(EventSlotDAO es) {
		if (ticketRepository.canBookEventSlot(es.getId())) {
			TicketDAO t = new TicketDAO();
			EventDAO e = eventRepository.getEventDAOById(es.getEventRef());
			LocationDAO l = locationRepository.getLocationById(e.getLocationRef());
			t.setEventslotRef(es.getId());
			t.setUsed(false);
			t.setUserRef(Constants.NO_ACCOUNT_ID);
			t.setCode(UUID.randomUUID().toString());
			t = ticketRepository.save(t);
			return new Ticket(t.getId(), t.getCode(), e.getTitle(), e.getTitolo(), e.getId(), es.getDate(), es.getStartTime(), es.getEndTime(), 
					l.getCity(), l.getAddress(), l.getName(), t.getUsed(), es.getValid());
		} else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
	}
	
	private Ticket bookTotemBarcode(String barcode, EventSlotDAO es) {
		/* Penalty is checked between todayDate and infLimitDate */ 
		String infLimitDate = LocalDate.now().minusMonths(Constants.MONTHS_NO).format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		
		if(ticketRepository.getBarcodePenalities(barcode, infLimitDate, todayDate) >= Constants.PENALTIES_LIMIT)
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Limit of penalties reached");
		
		if(ticketRepository.getBarcodeTicketsByEventSlotId(barcode, es.getId()) >= Constants.TICKETS_LIMIT)
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Limit of tickets reached");
		
		if (ticketRepository.canBookEventSlot(es.getId())) {
			TicketDAO t = new TicketDAO();
			EventDAO e = eventRepository.getEventDAOById(es.getEventRef());
			LocationDAO l = locationRepository.getLocationById(e.getLocationRef());
			t.setEventslotRef(es.getId());
			t.setUsed(false);
			t.setUserRef(Constants.NO_ACCOUNT_ID);
			t.setCode(barcode);
			t = ticketRepository.save(t);
			return new Ticket(t.getId(), t.getCode(), e.getTitle(), e.getTitolo(), e.getId(), es.getDate(), es.getStartTime(), es.getEndTime(), 
					l.getCity(), l.getAddress(), l.getName(), t.getUsed(), es.getValid());
		}
		else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
	}
	
	private Ticket bookTotemMail(String mail, EventSlotDAO es, String language) {
		
		if(userRepository.existsByEmail(mail) != 1) {
			userRepository.addUser(mail, "*", "*", "*", "*", Constants.CUSTOMER);
		}
		UserDAO u = userRepository.getUserByEmail(mail);
		
		/* Penalty is checked between todayDate and infLimitDate */ 
		String infLimitDate = LocalDate.now().minusMonths(Constants.MONTHS_NO).format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		
		if(ticketRepository.getUserPenalities(u.getId(), infLimitDate, todayDate) >= Constants.PENALTIES_LIMIT)
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Limit of penalties reached");
		
		if(ticketRepository.getUserTicketsByEventSlotId(u.getId(), es.getId()) >= Constants.TICKETS_LIMIT)
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Limit of tickets reached");
		
		if (ticketRepository.canBookEventSlot(es.getId())) {
			TicketDAO t = new TicketDAO();
			EventDAO e = eventRepository.getEventDAOById(es.getEventRef());
			LocationDAO l = locationRepository.getLocationById(e.getLocationRef());
			t.setEventslotRef(es.getId());
			t.setUsed(false);
			t.setUserRef(u.getId());
			t.setCode(UUID.randomUUID().toString());
			t = ticketRepository.save(t);
			EmailSystem.sendTicketEmailNoAccount(u.getEmail(), e.getTitle(), e.getTitolo(), l.getName(),
					l.getCity(), l.getAddress(), es.getDate(), es.getStartTime(), es.getEndTime(), t.getCode(), language);
			return new Ticket(t.getId(), t.getCode(), e.getTitle(), e.getTitolo(), e.getId(), es.getDate(), es.getStartTime(), es.getEndTime(), 
					l.getCity(), l.getAddress(), l.getName(), t.getUsed(), es.getValid());
		} else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
	}	
	
}
