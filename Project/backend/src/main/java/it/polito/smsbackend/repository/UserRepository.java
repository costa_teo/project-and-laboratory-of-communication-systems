package it.polito.smsbackend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.User;

public interface UserRepository extends CrudRepository<UserDAO, Integer> {
	
	/* ADMINISTRATOR QUERY */
	
	@Query(value = "SELECT COUNT(*) FROM USER WHERE id = :checkId AND role = :checkRole", nativeQuery = true)
	int checkPrivilegeRole(@Param("checkId") Integer checkId, @Param("checkRole") String checkRole);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE USER SET role = :newRole WHERE id = :userId", nativeQuery = true)
	void changeUserRole(@Param("userId") Integer userId, @Param("newRole") String newRole);
	
	@Query(value = "SELECT new it.polito.smsbackend.dto.User(id, name, surname, cellNo, email, role) "
			+ "FROM UserDAO "
			+ "WHERE role LIKE :role AND email LIKE :mail "
			+ "AND name <> '*' AND surname <> '*'", nativeQuery = false)
	List<User> getUsers(@Param("role") String role, @Param("mail") String mail);
	
	/* CUSTOMER QUERY */
	
	@Query(value = "SELECT * FROM USER u WHERE u.password LIKE :password AND u.email = :email ", nativeQuery = true)
	UserDAO getUserByCredentials(@Param("email") String email, @Param("password") String password);
	
	@Query(value = "SELECT * FROM USER u WHERE u.id = :id AND name = '*' AND surname = '*' ", nativeQuery = true)
	UserDAO getGhostUserById(@Param("id") Integer id);

	@Query(value = "SELECT * FROM USER u WHERE u.id = :id AND name <> '*' AND surname <> '*' ", nativeQuery = true)
	UserDAO getUserById(@Param("id") Integer id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE USER SET PASSWORD = :newPassword WHERE ID = :userId", nativeQuery = true)
	void changePassword(@Param("userId") Integer userId, @Param("newPassword") String newPassword);

	@Query(value = "SELECT COUNT(*) FROM USER WHERE email = :email", nativeQuery = true)
	Integer existsByEmail(@Param("email") String email);
	
	@Modifying
	@Transactional
	@Query(value = "INSERT INTO USER (id, email, password, name, surname, role, cell_no) "
			+ "VALUES (user_seq.nextval, :email, :password, :name, :surname, :role, :cellNo)", nativeQuery = true)
	void addUser(@Param("email") String email, @Param("password") String password, @Param("name") String name, @Param("surname") String surname,
			@Param("cellNo") String cellNo, @Param("role") String role);
	
	
	@Query(value = "SELECT * FROM USER u WHERE u.id IN ("
			+ "SELECT t.user_ref FROM TICKET t, EVENT_SLOT es WHERE es.id = :id AND t.eventslot_ref = es.id)", nativeQuery = true)
	List<UserDAO> getPartecipantsByEventSlotId(@Param("id") Integer id);

	@Query(value = "SELECT COUNT(*) FROM USER WHERE email = :email AND name = '*' AND surname = '*' ", nativeQuery = true)
	int isGhostAccount(String email);
	
	@Query(value = "SELECT * FROM USER WHERE email = :mail", nativeQuery = true)
	UserDAO getUserByEmail(String mail);


	

	

	
	
	
}
