package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EVENT_SLOT")
public class EventSlotDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventslot_generator")
	@SequenceGenerator(name = "eventslot_generator", sequenceName = "eventslot_seq", allocationSize = 1)
	private Integer id;
	@Column(nullable = false, name = "event_ref")
	private Integer eventRef;
	@Column(nullable = false)
	private String date;
	@Column(nullable = false, name = "start_time" )
	private String startTime;
	@Column(nullable = false, name = "end_time")
	private String endTime;
	@Column(nullable = false, name = "max_people")
	private Integer maxPeople;
	@Column(nullable = false)
	private Boolean valid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public Integer getEventRef() {
		return eventRef;
	}

	public void setEventRef(Integer eventRef) {
		this.eventRef = eventRef;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getMaxPeople() {
		return maxPeople;
	}

	public void setMaxPeople(Integer maxPeople) {
		this.maxPeople = maxPeople;
	}

}
