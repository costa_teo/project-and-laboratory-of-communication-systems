package it.polito.smsbackend.dto;

public class EventSlot {

	private Integer id;
	private String date;
	private String startTime;
	private String endTime;
	private Integer totalTickets;
	private Integer remaining;
	private Boolean valid;

	public EventSlot(Integer id, String date, String startTime, String endTime, Integer totalTickets, Long remaining, Boolean valid) {
		this.id = id;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.totalTickets = totalTickets;
		this.remaining = remaining.intValue();
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return "EventSlot [id=" + id + ", date=" + date + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", totalTickets=" + totalTickets + ", remaining=" + remaining + ", valid=" + valid + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getTotalTickets() {
		return totalTickets;
	}

	public void setTotalTickets(Integer totalTickets) {
		this.totalTickets = totalTickets;
	}

	public Integer getRemaining() {
		return remaining;
	}

	public void setRemaining(Integer remaining) {
		this.remaining = remaining;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}
}
