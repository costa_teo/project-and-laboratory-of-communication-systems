package it.polito.smsbackend.dto;

import it.polito.smsbackend.dao.UserDAO;

public class User {

	private Integer id;
	private String name;
	private String surname;
	private String cellNo;
	private String email;
	private String role;
	private String token;

	public User(Integer id, String name, String surname, String cellNo, String email, String role) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.cellNo = cellNo;
		this.email = email;
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public UserDAO map() {
		UserDAO u = new UserDAO();
		u.setId(id);
		u.setName(name);
		u.setSurname(surname);
		u.setCellNo(cellNo);
		u.setEmail(email);
		u.setRole(role);

		return u;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCellNo() {
		return cellNo;
	}

	public void setCellNo(String cellNo) {
		this.cellNo = cellNo;
	}
}
