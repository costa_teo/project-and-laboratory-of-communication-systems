package it.polito.smsbackend.request;

import it.polito.smsbackend.dto.Location;

public class ManagerLocationRequest {
	private Integer managerId;
	private Location location;
	public Integer getManagerId() {
		return managerId;
	}
	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
	

}
