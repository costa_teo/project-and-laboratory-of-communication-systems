package it.polito.smsbackend.common;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.dto.EventSlot;
import it.polito.smsbackend.request.AddNewEventRequest;

public class Util{

	private Util() {
		//
	}
    
	public static void checkAddEventRequestValidity(AddNewEventRequest request) {
		
		if (request.getDescription() == null || request.getDescription().isBlank() || request.getTitle() == null
				|| request.getTitle().isBlank() || request.getCity() == null || request.getCity().isBlank()
				|| request.getLocationName() == null || request.getDescrizione() == null
				|| request.getDescrizione().isBlank() || request.getTitolo() == null || request.getTitolo().isBlank()
				|| request.getLocationName().isBlank() || request.getAddress() == null || request.getAddress().isBlank()
				|| request.getEventSlots() == null || request.getEventSlots().size() == 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		for (EventSlot es : request.getEventSlots()) {
			if (es.getStartTime() == null || es.getStartTime().isBlank() || es.getEndTime() == null
					|| es.getEndTime().isBlank() || es.getTotalTickets() == null || es.getTotalTickets() <= 0
					|| es.getValid() == null
					|| es.getDate() == null || es.getStartTime() == null || es.getEndTime() == null
					|| !es.getDate().matches("[0-9]{4}/[0-9]{2}/[0-9]{2}")
					|| !es.getStartTime().matches("([0-1][0-9]|2[0-3]):[0-5][0-9]")
					|| !es.getEndTime().matches("([0-1][0-9]|2[0-3]):[0-5][0-9]"))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			
			/* Check date for new slots */
			String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
			if(request.getId()==null && 
					LocalDate.parse(es.getDate(), DateTimeFormatter.ofPattern("yyyy/MM/dd"))
					.isBefore(LocalDate.parse(todayDate, DateTimeFormatter.ofPattern("yyyy/MM/dd")))) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Date < TodayDate");
			}
		}
	}
	
	public static Boolean isLanguageValid(String language) {
		if(Constants.ENG.equals(language) || Constants.ITA.equals(language))
			return Boolean.TRUE;
		return Boolean.FALSE;
	}
	
	public static Boolean isMailValid(String email) {
		Pattern p = Pattern.compile(
				"^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$",
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = p.matcher(email);
		return matcher.find();
	}
	
	public static Boolean isNSValid(String ns) {
		Pattern p = Pattern.compile("^[\\w]+([ ][\\w]+)*$", Pattern.CASE_INSENSITIVE);
		Matcher matcher = p.matcher(ns);
        return matcher.find();
	}
		    
}
