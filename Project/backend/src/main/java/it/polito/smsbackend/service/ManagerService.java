package it.polito.smsbackend.service;

import java.util.List;

import it.polito.smsbackend.dto.EventDetail;
import it.polito.smsbackend.request.AddNewEventRequest;
import it.polito.smsbackend.request.EventsPreviewRequest;
import it.polito.smsbackend.request.ModifyEventImageRequest;
import it.polito.smsbackend.response.EventsPreviewResponse;

public interface ManagerService {

	void deleteEvent(Integer managerId, Integer eventId, String language);
	
	EventsPreviewResponse getManagerEvents(Integer managerId, EventsPreviewRequest request);

	Integer addNewEvent(Integer managerId, AddNewEventRequest request);

	EventDetail getEventDetail(Integer managerId, Integer eventId);

	void modifyEvent(Integer managerId, AddNewEventRequest request, String language);

	void modifyEventImage(Integer managerId, ModifyEventImageRequest request);

	List<String> getAddressesByCityLocation(Integer managerId, String city, String locationName);

	List<String> getCities(Integer managerId);

	List<String> getLocationNames(Integer managerId, String city);

	List<String> getEventsCities(Integer managerId);

}
