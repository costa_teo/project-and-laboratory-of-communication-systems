package it.polito.smsbackend.response;

import java.util.List;

import it.polito.smsbackend.dto.User;

public class GetUserResponse {

	private Integer totPages;
	private Integer pageNumber;
	private List<User> users;

	public Integer getTotPages() {
		return totPages;
	}

	public void setTotPages(Integer totPages) {
		this.totPages = totPages;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}
