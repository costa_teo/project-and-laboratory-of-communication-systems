package it.polito.smsbackend.controller;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.polito.smsbackend.service.AuthenticationService;
import it.polito.smsbackend.service.UserService;
import it.polito.smsbackend.common.Constants;
import it.polito.smsbackend.dto.EventDetail;
import it.polito.smsbackend.dto.Ticket;
import it.polito.smsbackend.request.BookEventTotemRequest;
import it.polito.smsbackend.request.EventsPreviewRequest;
import it.polito.smsbackend.request.RegisterRequest;
import it.polito.smsbackend.response.EventsPreviewResponse;

@RestController
@RequestMapping(value = "user")
@CrossOrigin( origins = "*" )
public class UserController {
	
	private final UserService userService;
	private final AuthenticationService authenticationService;
	
	@Autowired
	public UserController(UserService userService, AuthenticationService authenticationService) {
		this.userService = userService;
		this.authenticationService = authenticationService;
	}
	
	@GetMapping(value = "getEventsCities")
	public List<String> getEventsCities() {
		return userService.getEventsCities();
	}
	
	@PostMapping(value = "getEvents")
	public EventsPreviewResponse getEvents(@RequestHeader("language") String language, @RequestBody EventsPreviewRequest request) {
		return userService.getEventsByCityDateName(request);
	}
	
	@GetMapping(value = "getEventDetail")
	public EventDetail getEventDetail(@RequestHeader("language") String language, @RequestParam("id") Integer eventId) {
		return userService.getEventDetail(eventId);
	}
	
	@PostMapping(value = "bookEventSlot")
	public void bookEventSlot(@RequestHeader("language") String language, @RequestHeader("token") String token, @RequestBody Integer eventSlotId) {
		Integer userId = authenticationService.authenticateUser(token);
		userService.bookEventSlot(userId, eventSlotId, language);
	}
	
	@GetMapping(value = "getCustomerTickets")
	public List<Ticket> getCustomerTickets(@RequestHeader("language") String language, @RequestHeader("token") String token) {
		Integer userId = authenticationService.authenticateUser(token);
		return userService.getCustomerTickets(userId);
	}
	
	@PostMapping(value = "register")
	public void registerUser(@RequestHeader("language") String language, @RequestBody RegisterRequest request) {
		userService.registerUser(request, language);
	}	
	
	@GetMapping(value="activate/{signHash}")
	public void activateUserAccount(@PathVariable("signHash") String signHash, HttpServletResponse response) throws IOException {
		userService.activateAccount(signHash);
		response.sendRedirect(Constants.BASE_URL);
	}
	
	@GetMapping(value = "getEventImage", produces = MediaType.ALL_VALUE)
	public @ResponseBody byte[] getEventImage(@RequestParam("id") Integer imageId) throws SQLException  {
		Blob b = userService.getEventImage(imageId);
		return b.getBytes(0, (int) b.length());
	}
	
	@PostMapping(value = "cancelTicket")
	public void cancelTicket(@RequestHeader("token") String token, @RequestBody Integer ticketId) {
		Integer userId = authenticationService.authenticateUser(token);
		userService.cancelTicket(userId, ticketId);
	}
	
	/************* GATE/TOTEM **************/
	
	@GetMapping(value = "checkTicket")
	public Boolean checkTicket(@RequestParam("ticketCode") String ticketCode) {
		return userService.checkTicket(ticketCode);
	}
	
	@PostMapping(value="bookEventSlotNoAccount")
	public Ticket bookEventSlotNoAccount(@RequestHeader("language") String language, @RequestBody BookEventTotemRequest request) {
		return userService.bookEventSlotNoAccount(request, language);
	}
}
