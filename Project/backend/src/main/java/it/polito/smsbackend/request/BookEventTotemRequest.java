package it.polito.smsbackend.request;

public class BookEventTotemRequest {
    private Integer eventSlotId;
    private String data;
    
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Integer getEventSlotId() {
		return eventSlotId;
	}
	public void setEventSlotId(Integer eventSlotId) {
		this.eventSlotId = eventSlotId;
	}
}
