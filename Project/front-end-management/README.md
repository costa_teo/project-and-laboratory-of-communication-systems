# FrontEndManagement

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Configuration
1. install angular
2. run: npm install
3. npm install angular2-qrcode
4. npm install nan
5. npm install @ngx-translate/core --save
6. npm install @ngx-translate/http-loader
7. npm i @types/jquery
8. run: ng serve or npm start

# cordova 
nel body infondo
<!--script type="text/javascript" src="main-es2015.js"></script-->
<!--script type="text/javascript" src="polyfills-es2015.js"></script> <!--necessario altrimenti schermo azzurro-->
<!--script type="text/javascript" src="runtime-es2015.js"></script> <!--necessario altrimenti schermo azzurro-->
<!--script type="text/javascript" src="cordova.js"></script-->

ng build --prod=true --outputHashing=none --namedChunks=false --vendorChunk=false
then
cordova run android


