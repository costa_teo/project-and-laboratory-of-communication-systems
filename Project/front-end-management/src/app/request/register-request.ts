export class RegisterRequest {
    public email: string
    public password: string
    public name: string
    public surname: string
    public cellNo: string
    public birthdate: string
    
    constructor() {
        this.email = ""
        this.password = ""
        this.name = ""
        this.surname = ""
        this.cellNo = ""
        this.birthdate = ""
    }
}