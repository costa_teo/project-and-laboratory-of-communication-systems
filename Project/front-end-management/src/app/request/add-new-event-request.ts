import { EventSlot } from '../entities/event-slot';

export class AddNewEventRequest {
    public id: number;
    public title: string;
    public titolo: string;
    public description: string;
    public descrizione: string;
    public city: string
    public locationName: string
    public address: string
    public eventSlots: Array<EventSlot>;

    constructor() {
        this.id = undefined;
        this.title = "";
        this.description = "";      
        this.eventSlots = [];
        this.city = undefined
        this.locationName = undefined
        this.address = undefined
    }
}