import { ROLES } from '../shared/constants';

export class GetUserRequest {
    
    public role: ROLES
    public page: number

    constructor() {
        this.role = undefined
        this.page = undefined
    }
}