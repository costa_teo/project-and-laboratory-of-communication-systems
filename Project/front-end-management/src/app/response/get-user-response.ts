import { User } from '../entities/user'

export class GetUserResponse {
    
    totPages: number
    pageNumber: number
    users: Array<User>

    constructor() {
        this.totPages = 0
        this.pageNumber = 0
        this.users = []
    }
}