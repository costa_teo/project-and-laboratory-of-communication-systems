import { EventPreview } from '../entities/event-preview'

export class EventsPreviewResponse {
    
    totPages: number
    pageNumber: number
    eventsPreview: Array<EventPreview>

    constructor() {
        this.totPages = 0
        this.pageNumber = 0
        this.eventsPreview = []
    }
}