export class Time {
  
    hours: number
    minutes: number
    constructor() {
        this.hours=0;
        this.minutes=0;
    }

    public static newTime(time: string): Time{
        let e = new Time
        e.hours = +time.split(":")[0]
        e.minutes = +time.split(":")[1]
        return e
    }



    add(time: Time){
        this.hours+=time.hours
        this.minutes+=time.minutes

        this.module()
    }

    public toString(){
        return ((this.hours.toString().length == 1)? "0"+this.hours.toString() : this.hours.toString()) + ":" + ((this.minutes.toString().length==1)? "0"+this.minutes.toString() : this.minutes.toString()) 
    }

    private module(){
        this.hours += Math.floor(this.minutes/60)
        this.minutes %= 60
        this.hours %= 24
    }

    static sum(a: Time, b: Time) {
        let e = new Time
        e.hours = a.hours + b.hours
        e.minutes = a.minutes + b.minutes
        e.module()
        return e;
      }
}