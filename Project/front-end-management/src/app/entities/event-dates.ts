import { Time } from './time'

export class eventDate {
    
    date: string
    startTime: string
    numberOfSlots: number
    slotDuration: string
    totalTickets: number

    constructor() {
        
    }
}