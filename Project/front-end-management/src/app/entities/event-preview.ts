export class EventPreview {
    
    public id: number;
    public title: string;
    public titolo: string;
    public firstDate: string;
    public lastDate: string;
    public imageId: number;
    public locationId: number;

    constructor(){
        this.id = undefined;
        this.title = "";
        this.firstDate = "";
        this.lastDate = "";
        this.imageId = 0;
        this.locationId = 0;
        this.titolo = ""
    }

    
}