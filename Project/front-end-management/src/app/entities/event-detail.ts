import { EventSlot } from './event-slot';

export class EventDetail {
    public id: number;
    public title: string;
    public titolo: string;
    public description: string;
    public descrizione: string;
    public imageId: number;
    public city: string
    public locationName: string
    public address: string

    public locationId: number //todo qr config
    
    public eventSlots: Array<EventSlot>;

    constructor() {
        this.id = undefined;
        this.title = "";
        this.description = "";
        this.imageId = 0;        
        this.eventSlots = [];
        this.city = ""
        this.locationName = ""
        this.address = ""
        this.titolo = ""
        this.descrizione = ""
    }
}