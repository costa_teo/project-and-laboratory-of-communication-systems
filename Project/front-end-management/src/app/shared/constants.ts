import { DatePipe } from '@angular/common';


export class ROLES {
    static CUSTOMER: string = "CUSTOMER";
    static MANAGER: string = "MANAGER";
    static ADMINISTRATOR: string = "ADMINISTRATOR";
}



export class CONSTANTS {
  
    static days = ["Mon", "Thu", "Wed", "Tus", "Fri"]
    
    static validHours = [
      "08:00",
      "09:00",
      "10:00",
      "11:00",
      "12:00",
      "13:00",
    ]

    static SUBJECT_COLORS = [
        "#c70000",
        "#c77100",
        "#c7ba00",
        "#88c700",
        "#35c700",
        "#00c799",
        "#009cc7",
        "#009cc7",
        "#1b00c7",
        "#7b00c7",
        "#c7008f",
      ]
  static TIMETABLE_TEMPLATE_URL: string = "timetable-template.xlsx";
  static CLASS_COMPOSITION_TEMPLATE_URL: string = "class-composition-template.xlsx"; 

    static convertDateToServer(date: any): string {
      
      try {
          var datePipe = new DatePipe("en-UK")
          return datePipe.transform(date, 'yyyy/MM/dd')
      } catch (error) {
        try {
          var date1 = date.split('/')
          return date1[2] + '/' +date1[1] +'/' +date1[0]; 
          
        } catch (error) {
          return ""
        }{
        }
      }
    }

    static convertDateToGUI(date: string): string {
      try {
        var datePipe = new DatePipe("en-UK")
        return datePipe.transform(date, 'dd/MM/yyyy')
    } catch (error) {
      return ""
    }
  }


}


