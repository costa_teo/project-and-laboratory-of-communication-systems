import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoaderMainComponent } from './components/loader-main/loader-main.component';
import { LoaderSmallComponent } from './components/loader-small/loader-small.component';
import { ServiceBoxService } from './components/service-box/service-box.service';
import { MessageComponent } from './components/message/message.component';
import { ModalComponent } from './components/modal/modal.component';
import { ServiceBoxComponent } from './components/service-box/service-box.component';
import { SharedService } from './shared.service';
import { QRCodeModule } from 'angular2-qrcode';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        LoaderMainComponent,
        LoaderSmallComponent,
        MessageComponent,
        ModalComponent,
        ServiceBoxComponent,
    ],
    exports: [
        LoaderMainComponent,
        LoaderSmallComponent,
        MessageComponent,
        ModalComponent,
        ServiceBoxComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        QRCodeModule,

        BrowserModule,
        HttpClientModule,
        TranslateModule
    ],
    providers: [ServiceBoxService, SharedService]
})
export class SharedModule { }