import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Routes } from '../core/routes';
import { Observable } from 'rxjs';
import { EventsPreviewRequest } from '../request/events-preview-request';
import { EventsPreviewResponse } from '../response/events-preview-response';
import { Router, ActivatedRoute } from '@angular/router';
import { EventDetail } from '../entities/event-detail';
import { RegisterRequest } from '../request/register-request';

@Injectable({
    providedIn: 'root'
})
export class SharedService {
  
    private baseURL: string;

    constructor(private http: HttpClient,
        private route: ActivatedRoute) {
        this.baseURL = Routes.baseURL + '/user';
    }

    public getEventAllCities() : Observable<Array<string>>{
        return this.http.get<Array<string>>(this.baseURL + '/getAllCities');
    }

    public getEvents(pageNumber: number, selectedCity: string, selectedDate: string): Observable<EventsPreviewResponse> {
        let request = new EventsPreviewRequest();
        request.page = pageNumber
        request.city = selectedCity
        request.date = selectedDate
        
        return this.http.post<EventsPreviewResponse>(this.baseURL + '/getEvents', request);    
    }

    public getEventDetail(eventId: number): Observable<EventDetail> {
        let httpParams = { params: new HttpParams().set('id', eventId.toString()) };
        return this.http.get<EventDetail>(this.baseURL + '/getEventDetail', httpParams);
    }

    public register(registerRequest: RegisterRequest): Observable<any> {
        return this.http.post(this.baseURL + '/register', registerRequest, { observe: 'response' }) 
    }
      

    public getAddressesByCityAndLocation(city: string, location: string) {
        let httpParams = { params: new HttpParams()
            .set('city', city)
            .set('location', location) };
        return this.http.get<Array<string>>(this.baseURL + '/getAddressesByCityAndLocation', httpParams);
    }
    
      public getLocationNamesByCity(city: string) {
        let httpParams = { params: new HttpParams()
            .set('city', city) }
        return this.http.get<Array<string>>(this.baseURL + '/getLocationNamesByCity', httpParams);
    }

    /**/

    public getEventImage(id: number): Observable<Blob> {
        let param = new HttpParams().set('id', id.toString());
        return this.http.get(this.baseURL + '/getEventImage', { params: param, responseType: 'blob' });
    }



}