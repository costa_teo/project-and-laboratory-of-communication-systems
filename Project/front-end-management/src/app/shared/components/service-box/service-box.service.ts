import { ServiceBoxData } from './service-box-data';
import { Injectable } from '@angular/core';
import { Routes } from 'src/app/core/routes';
import { ROLES } from '../../constants';

@Injectable()
export class ServiceBoxService {
    
    private notLoggedServicesArray: Array<ServiceBoxData>;

    private customerServicesArray: Array<ServiceBoxData>;
    private managerServicesArray:  Array<ServiceBoxData>;
    private administratorServicesArray:  Array<ServiceBoxData>;
    constructor() {
        
        this.notLoggedServicesArray = new Array<ServiceBoxData>();
        this.notLoggedServicesArray.push(
            new ServiceBoxData("Events","Eventi",  Routes.CUSTOMER_EVENTS))

        this.customerServicesArray = new Array<ServiceBoxData>();
        this.customerServicesArray.push(
            new ServiceBoxData("Events","Eventi", Routes.CUSTOMER_EVENTS),
            new ServiceBoxData("Your tickets","I tuoi biglietti",  Routes.CUSTOMER_YOUR_TICKET),
        );

        this.managerServicesArray = new Array<ServiceBoxData>();
        this.managerServicesArray.push(
            new ServiceBoxData("View events", "Vedi eventi", Routes.MANAGER_YOUR_EVENTS),
            new ServiceBoxData("Add event", "Aggiungi evento", Routes.MANAGER_ADD_EVENT),
        );

        this.administratorServicesArray = new Array<ServiceBoxData>();
        this.administratorServicesArray.push(
            new ServiceBoxData("Add event","Aggiungi evento", Routes.ADMIN_ADD_EVENT),
            new ServiceBoxData("Manage events","Gestisci eventi", Routes.ADMIN_EVENTS),
            new ServiceBoxData("Manage users","Gestisci utenti", Routes.ADMIN_MANAGE_MANAGERS),
            new ServiceBoxData("Manage Locations","Gestisci indirizzi", Routes.ADMIN_MANAGE_LOCATIOS),
        );

    }

    public get administratorServices(): Array<ServiceBoxData> {
        return this.administratorServicesArray;
    }

    public get managerServices(): Array<ServiceBoxData> {
        return this.managerServicesArray
    }

    public get customerServices(): Array<ServiceBoxData> {
        return this.customerServicesArray
    }

    public getServicesByRole(role: ROLES): Array<ServiceBoxData> {
        
        if (role == ROLES.ADMINISTRATOR) return this.administratorServices;
        if (role == ROLES.MANAGER) return this.managerServices;
        if (role == ROLES.CUSTOMER) return this.customerServices;
        return this.notLoggedServicesArray;
    }
}