export class ServiceBoxData {
    public serviceName: string;
    public redirectionPath: string;
    public nomeServizio: string;

    constructor(service: string, nomeServizio: string, path: string) {
        this.serviceName = service;
        this.nomeServizio = nomeServizio 
        this.redirectionPath = path;
    }
}