import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

export class MESSAGE_TYPE {
  static ERROR: string = "alert-danger";
  static WARNING: string = "alert-warning";
  static SUCCESS: string = "alert-success";
}

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  toShow: boolean = false;
  messageText: string;
  messageType: MESSAGE_TYPE = "alert-danger";

  private interval = 0;
  object: any;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    //
  }

  showMessage(message: number, type: MESSAGE_TYPE) {
    this.messageType = type;
    this.messageText = message.toString();
    this.object = null
    window.scrollTo(0, 0);
    this.toShow = true;
    this.startTimer();
  }
  
  showMessageWithObject(message: number,object: any, type: MESSAGE_TYPE) {
    this.messageType = type;
    this.messageText = message.toString();
    this.object = null
    window.scrollTo(0, 0);
    this.toShow = true;
    this.startTimer();
  }

  private startTimer() {
    window.clearTimeout(this.interval);
    this.interval = window.setTimeout(() => {
      this.toShow = false;
      this.messageText = "";
    }, 7000);
  }

}
