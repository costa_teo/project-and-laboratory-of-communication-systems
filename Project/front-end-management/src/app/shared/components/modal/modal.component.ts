import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output() private onConfirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  bodyMessage: string;
  title: string;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
  }

  show(title: number, bodyMessage: string) {
    this.bodyMessage = bodyMessage;
    this.title = title.toString()
    $("#my-modal").modal('show');
  }

  doConfirm() {
    this.onConfirm.emit(true);
  }

}
