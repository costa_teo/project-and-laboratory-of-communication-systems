import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './modules/home/home.module';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { AuthenticationService } from './core/authentication.service';
import { AuthGuard } from './core/auth.guard';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import {HashLocationStrategy, LocationStrategy} from '@angular/common'
import { CustomerModule } from './modules/customer/customer.module'
import { QRCodeModule } from 'angular2-qrcode';
import { AdministratorModule } from './modules/administrator/administrator.module';
import { ManagerModule } from './modules/manager/manager.module';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import * as $ from 'jquery';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    HomeModule,
    AdministratorModule,
    ManagerModule,
    CustomerModule,
    QRCodeModule,

    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    AppRoutingModule //AppRoutingModule VA PER ULTIMO!!! (Per il redirect)  
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationService,
      multi: true
    },{
      provide: LocationStrategy, 
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
} //for translation