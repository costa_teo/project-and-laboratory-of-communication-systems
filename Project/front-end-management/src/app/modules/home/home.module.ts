import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeLoginComponent } from './components/home-login/home-login.component';
import { HomeService } from './home.service';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReferenceComponent } from './components/reference/reference.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { CustomerService } from '../customer/customer.service';
import { RegisterComponent } from './components/register/register.component';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    HomeLoginComponent,
    ReferenceComponent,
    ChangePasswordComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    HomeRoutingModule,

    BrowserModule,
    HttpClientModule,
    TranslateModule
  ],
  providers: [HomeService, CustomerService]
})
export class HomeModule { }
