import { Component, OnInit } from '@angular/core';

import { CONSTANTS } from 'src/app/shared/constants';

@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss']
})
export class ReferenceComponent implements OnInit {

  mockData: Array<any> = [
    { 'c1': 'Element 1.1', 'c2': 'C', 'c3': 'Element 1.3', 'c4': 'Element 1.4' },
    { 'c1': 'Element 2.1', 'c2': 'A', 'c3': 'Element 2.3', 'c4': 'Element 2.4', 'isEditable': false, 'isDeletable': false },
    { 'c1': 'Element 3.1', 'c2': 'B', 'c3': 'Element 3.3', 'c4': 'Element 3.4', 'isEditable': false }
  ];


  public loaded: boolean;

  constructor() { }

  ngOnInit() {
    this.loaded = true;
  }

  myFuncEdit(updatedElement: { value: any, index: number }) {
    this.mockData[updatedElement.index] = updatedElement.value;
  }

  myFuncDel(index: number) {
    this.mockData.splice(index, 1);
  }

  

}


