import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { LoginRequest } from 'src/app/request/login-request';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { CustomerService } from 'src/app/modules/customer/customer.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home-login',
  templateUrl: './home-login.component.html',
  styleUrls: ['./home-login.component.scss']
})
export class HomeLoginComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  public username: string;
  public password: string;
  public loaded: boolean;

  constructor(private authenticationService: AuthenticationService,
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private translate: TranslateService) { }

  ngOnInit() {
    this.loaded = false;

    if (this.authenticationService.currentUserValue) {
      this.authenticationService.redirectToUserHome();
    }
    this.loaded = true;
  }

  login() {
    this.loaded = false;
    let loginRequest: LoginRequest = new LoginRequest(this.username, this.password)
    this.authenticationService.login(loginRequest).subscribe(
      user => {
        this.authenticationService.saveUser(user);
        let choosenEventId = Number(this.route.snapshot.paramMap.get('id'))
        
        if(choosenEventId)
          this.authenticationService.redirectToChoosenEvent(choosenEventId)
        else
          this.authenticationService.redirectToUserHome()

      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage(51, MESSAGE_TYPE.ERROR);
      }
    );

  


  }

  doRegister(){
    this.authenticationService.redirectToRegister()
  }

}
