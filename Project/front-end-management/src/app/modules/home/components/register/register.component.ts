import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { User } from 'src/app/entities/user';
import { RegisterRequest } from 'src/app/request/register-request';
import { CONSTANTS } from 'src/app/shared/constants';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;


  public registerRequest: RegisterRequest
  public birthdate: Date
  public loaded = false;


  constructor(private authenticationService: AuthenticationService,
    private sharedService: SharedService) {
    this.registerRequest = new RegisterRequest()
    
   }

  ngOnInit() {
    this.loaded = false;
    const user: User = this.authenticationService.currentUserValue;

    if (user) {
      this.authenticationService.redirectToUserHome();
    }
    this.loaded = true;
  }

  register(){
    this.loaded = false
    this.registerRequest.birthdate = CONSTANTS.convertDateToServer(this.birthdate)
    console.log(this.registerRequest )
    this.sharedService.register(this.registerRequest).subscribe(data=>{
      this.authenticationService.redirectToLogin();
    },err =>{
      this.loaded = true
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
    })
  }

}
