import { Component, OnInit, ViewChild } from '@angular/core';
import { EventDetail } from 'src/app/entities/event-detail';
import { EventSlot } from 'src/app/entities/event-slot';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { AddNewEventRequest } from 'src/app/request/add-new-event-request';
import { TranslateService } from '@ngx-translate/core';
import { eventDate } from 'src/app/entities/event-dates';
import { Time } from 'src/app/entities/time';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedService } from 'src/app/shared/shared.service';
import { AdministratorService } from 'src/app/modules/administrator/administrator.service';
import { ManagerService } from '../../manager.service';

@Component({
  selector: 'app-manager-add-event',
  templateUrl: './manager-add-event.component.html',
  styleUrls: ['./manager-add-event.component.scss']
})
export class ManagerAddEventComponent implements OnInit {
  loaded = false
  newEvent = new AddNewEventRequest()
  
  eventDates: Array<eventDate> = [new eventDate]

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;
  image: File;
  imageurl: any;

  
  cities: string[];
  locations: string[] = []
  addresses: string[] = []

  constructor(private administratorService: AdministratorService,
    private managerService: ManagerService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer,
    private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.newEvent.eventSlots.push(new EventSlot)
    this.loaded = true


    //load cities
    this.administratorService.getAllCities().subscribe(data => {
      this.cities = data;
    }, err => {
      console.log(err)
      this.loaded = true
    })
  }

  loadLocations(){
    this.newEvent.address = undefined
    this.newEvent.locationName = undefined
    this.locations = undefined
    this.sharedService.getLocationNamesByCity(this.newEvent.city).subscribe(locations=>{
      this.locations = locations
    })
  }

  loadAddresses(){
    this.addresses = undefined
    this.sharedService.getAddressesByCityAndLocation(this.newEvent.city, this.newEvent.locationName).subscribe(addresses=>{
      this.addresses = addresses;
      this.newEvent.address = this.addresses[0]
    })
  }

  addEvent() {
    if (this.checkEvent()) {
      if (this.image != null && this.image.size == 0) {
        this.image = null
      }
      this.loaded = false

      this.fillAddEventRequest()

      this.managerService.addNewEvent(this.newEvent).subscribe(
        eventId => {
          this.childMessage.showMessage(6, MESSAGE_TYPE.SUCCESS);

          if(this.image != null){
            this.managerService.modifyEventImage(this.image.name, this.image, eventId).subscribe(
              ok=>{
                this.childMessage.showMessage(7, MESSAGE_TYPE.SUCCESS);
                this.loaded = true
              },err=>{
                this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
                this.loaded = true
              }
            )
          }
          this.loaded = true


        }, err => {
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
          this.loaded = true
        })
    }

  }

  checkEvent(): boolean {
    console.log("c")
    if (this.newEvent.title == "") {
      this.childMessage.showMessage(10, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.city == "") {
      this.childMessage.showMessage(14, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.locationName == "") {
      this.childMessage.showMessage(15, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.address == "") {
      this.childMessage.showMessage(16, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.description == "") {
      this.childMessage.showMessage(11, MESSAGE_TYPE.ERROR)
    } else if (this.eventDates.length == 0) {
      this.childMessage.showMessage(17, MESSAGE_TYPE.ERROR)
    } else {
      let i = 0;
      this.eventDates.forEach(date => {
        if (date.date !=""
         && date.numberOfSlots && date.numberOfSlots > 0 &&
        (date.slotDuration == "00:15" || date.slotDuration=="00:30" || date.slotDuration=="00:45" || date.slotDuration!=="01:00")
        && date.startTime && date.startTime != ""
        && date.totalTickets && date.totalTickets > 0) {
          i++
        }else{
          this.childMessage.showMessageWithObject(18 ,{"i": i}, MESSAGE_TYPE.ERROR) //TODO check
        }
      });
      return i == this.eventDates.length
    }
    return false
  }


  fillAddEventRequest(){   
    this.newEvent.eventSlots = []

    this.eventDates.forEach(date=>{
 
      let i = date.numberOfSlots
      
      let slotStartTime = Time.newTime(String(date.startTime))
      let slotDuration = Time.newTime(String(date.slotDuration))
      
      while(i>0){
        i--

        let e = new EventSlot
        e.startTime = slotStartTime.toString()
        e.endTime = Time.sum(slotStartTime, slotDuration).toString()
        e.totalTickets = date.totalTickets
        e.date =date.date
        e.valid = true

        this.newEvent.eventSlots.push(e)


        slotStartTime.add(slotDuration)
      } 
    })
  }



  removeDate(date: eventDate){
    this.eventDates.splice(this.eventDates.indexOf(date), 1)
  }

  addDate(){
    this.eventDates.push(new eventDate)
    this.fillAddEventRequest()
  }


  selectFile(files: FileList) {
    this.image = files.item(0);

    if (this.image.size) {
      let blob = new Blob([this.image.slice()], { type: this.image.type })
      let objectURL = URL.createObjectURL(blob)
      this.imageurl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    }

  }


}
