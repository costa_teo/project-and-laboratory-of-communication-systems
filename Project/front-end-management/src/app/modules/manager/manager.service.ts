import { Injectable } from '@angular/core';
import { EventsPreviewRequest } from 'src/app/request/events-preview-request';
import { Observable } from 'rxjs';
import { EventsPreviewResponse } from 'src/app/response/events-preview-response';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Routes } from 'src/app/core/routes';
import { EventPreview } from 'src/app/entities/event-preview';
import { AddNewEventRequest } from 'src/app/request/add-new-event-request';
import { EventDetail } from 'src/app/entities/event-detail';
import { CONSTANTS } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {
   
  private baseURL: string;

  constructor(private http: HttpClient,
      private route: ActivatedRoute) {
      this.baseURL = Routes.baseURL + '/manager';
  }

  public deleteManagerEvent(event: EventPreview): Observable<any>{
    return this.http.post(this.baseURL + "/deleteEvent", event.id, { observe: 'response' })
  }

  public getManagerEvents(page: number, selectedCity: string, selectedDate: string): Observable<EventsPreviewResponse> {
      let request = new EventsPreviewRequest();
      request.page = page
      request.city = selectedCity
      request.date = selectedDate
      
      return this.http.post<EventsPreviewResponse>(this.baseURL + '/getEvents', request);    
  }

  public addNewEvent(newEventRequest: AddNewEventRequest): Observable<number> {
    let requ = JSON.parse(JSON.stringify(newEventRequest));  //make a copy
    requ.eventSlots.forEach(e =>{ e.date = CONSTANTS.convertDateToServer(new Date(e.date))})
    return this.http.post<number>(this.baseURL + "/addNewEvent", requ)
  }

  public getEventDetail(eventId: number): Observable<EventDetail> {
    let httpParams = { params: new HttpParams().set('id', eventId.toString()) };
    return this.http.get<EventDetail>(this.baseURL + '/getEventDetail', httpParams);
  }

  public modifyEvent(newEvent: AddNewEventRequest) {
    let requ = JSON.parse(JSON.stringify(newEvent));  //make a copy
    requ.eventSlots.forEach(e =>{ e.date = CONSTANTS.convertDateToServer(new Date(e.date))})
    return this.http.post(this.baseURL + "/modifyEvent", requ, { observe: 'response' })
  }


  public modifyEventImage(name: string, image: File, eventId: number): Observable<any> {
    let fd: FormData = new FormData();
    fd.append('name', name);
    fd.append('file', image);
    fd.append('eventId', eventId.toString());
    console.log(image)
    return this.http.post(this.baseURL + "/modifyEventImage", fd, { observe: 'response' })   
}


}
