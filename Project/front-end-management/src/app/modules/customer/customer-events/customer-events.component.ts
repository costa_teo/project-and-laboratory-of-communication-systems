import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from '../customer.service';
import { EventPreview } from 'src/app/entities/event-preview';
import { ScrollTop } from 'src/app/core/scroll-top/scroll-top';
import { CONSTANTS } from 'src/app/shared/constants';
import { SharedService } from 'src/app/shared/shared.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-customer-events',
  templateUrl: './customer-events.component.html',
  styleUrls: ['./customer-events.component.scss']
})
export class CustomerEventsComponent implements OnInit {

  @ViewChild(ScrollTop, { static: true }) scrollTop: ScrollTop;
  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;


  loaded = true
  cities: Array<string>
  events: Array<EventPreview>

  selectedCity: string = ""
  selectedDate: Date 

  selectedPage: number = 0
  totPages: number = 0;

  constructor(private customerService: CustomerService,
    private sharedService: SharedService,
    private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    
    
    //load cities
    this.sharedService.getEventAllCities().subscribe(data => {
      this.cities = data;
    }, err => {
      console.log(err)
      this.loaded = true
    })
    
    //load events
    this.loadEvents()

  }

  goNextPage() {
    if (this.selectedPage < this.totPages - 1) {
      this.loadEvents(this.selectedPage+1)
    }
  }

  goPreviusPage() {
    if (this.selectedPage > 0) {
      this.loadEvents(this.selectedPage-1)
    }
  }

  loadEvents(page: number = 0) {
    this.loaded = false

    let dateString = CONSTANTS.convertDateToServer(new Date(this.selectedDate)) //convert date to string

    this.sharedService.getEvents(page, this.selectedCity, dateString).subscribe(data => {
      this.totPages = data.totPages
      this.events = data.eventsPreview
      this.events.forEach(e=>{
        e.firstDate = CONSTANTS.convertDateToGUI(e.firstDate) //correct the data format
        e.lastDate = CONSTANTS.convertDateToGUI(e.lastDate)   //correct the data format
        
        if(e.imageId >= 0){  
          e['loading'] = true
          this.sharedService.getEventImage(e.imageId).subscribe( //loading images
            image=>{          
              let objectURL = URL.createObjectURL(image)
              e['image'] = this.sanitizer.bypassSecurityTrustUrl(objectURL);
              e['loading'] = false
            })
        }

      }) 

      this.selectedPage = data.pageNumber
      this.loaded = true
      this.scrollTop.scrollToTop() 
    }, err => {
      this.childMessage.showMessage(4, MESSAGE_TYPE.ERROR)
      this.loaded = true
      
    })
  }

}
