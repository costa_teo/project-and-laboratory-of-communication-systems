import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from '../customer.service';
import { EventDetail } from 'src/app/entities/event-detail';
import { EventPreview } from 'src/app/entities/event-preview';
import { EventSlot } from 'src/app/entities/event-slot';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';
import { CONSTANTS } from 'src/app/shared/constants';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { mapToMapExpression } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-customer-event-detail',
  templateUrl: './customer-event-detail.component.html',
  styleUrls: ['./customer-event-detail.component.scss']
})
export class CustomerEventDetailComponent implements OnInit {

  image: any

  loaded = false
  event: EventDetail

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  choosenEventId: number
  dates: Array<string>;

  eventSlots :Map<string, Array<EventSlot>>;
  

  constructor(private customerService: CustomerService,
    private authenticationService: AuthenticationService,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private sanitizer: DomSanitizer) { }


  ngOnInit(): void {
    this.choosenEventId = Number(this.route.snapshot.paramMap.get('id'))
    if (this.choosenEventId == undefined || !Number.isSafeInteger(this.choosenEventId)) {
      this.customerService.redirectToEvents()
      return
    }


    this.loadEventDetail()
  }

  public loadEventDetail() {
    //load event details
    this.sharedService.getEventDetail(this.choosenEventId).subscribe(data => {
      this.event = data
      
      this.sharedService.getEventImage(data.imageId).subscribe(
        image=>{          
          let objectURL = URL.createObjectURL(image)
          this.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        }
      )
      
      this.event.eventSlots.forEach(e=>{e.date = CONSTANTS.convertDateToGUI(e.date)}) //correct the data format
      
      this.eventSlots = new Map()

      this.event.eventSlots.forEach(e=>{
        if(this.eventSlots.has(e.date)){
          let a: Array<EventSlot> = this.eventSlots.get(e.date)
          a.push(e)
        }else{
          let a: Array<EventSlot> = []
          a.push(e)
          this.eventSlots.set(e.date, a)
        }
      })

        
      this.dates = Array.from(this.eventSlots.keys())
      
      
      this.loaded = true
    }, err => {
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      this.loaded = true
      console.log(err)
    })
  }

  public bookEventSlot(eventSlot: EventSlot) {
    this.loaded = false
    this.authenticationService.authenticateUser().subscribe(d => {
      //the user is authenticatetd
      this.customerService.bookEventSlot(eventSlot.id).subscribe(
        data => {
          this.customerService.redirectToYourTicket()
          this.childMessage.showMessage(9, MESSAGE_TYPE.SUCCESS)

        }, err => {
          this.loaded = true
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
        })

    }, e => {
      //the user is not logged
      this.authenticationService.redirectToLogin(eventSlot.id)
    })

  }



}
