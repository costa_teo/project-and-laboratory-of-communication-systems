import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { AdministratorService } from '../../administrator.service';
import { Location } from 'src/app/entities/location';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { AddNewLocationRequest } from 'src/app/request/add-new-location-request';

@Component({
  selector: 'app-admin-manage-locations',
  templateUrl: './admin-manage-locations.component.html',
  styleUrls: ['./admin-manage-locations.component.scss']
})
export class AdminManageLocationsComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  constructor(
    private sharedService: SharedService,
    private administratorService: AdministratorService
  ) { }

  addRequest: AddNewLocationRequest = new AddNewLocationRequest

  loaded = false
  locations: Array<Location>
  cities = []
  openedCollapseId = ""

  selectedCity = ""

  ngOnInit(): void {

    this.loadCities()

  }

  loadCities() {
    //load cities
    this.loaded = false
    this.administratorService.getAllCities().subscribe(data => {
      this.cities = data;
      this.loaded = true

    }, err => {
      console.log(err)
      this.loaded = true
    })
  }

  loadLocations(city, collapseId: string) {

    if (this.selectedCity != city) {
      let lastOpeneddiv: any = $(this.openedCollapseId)
      lastOpeneddiv.collapse("hide")
      this.locations = undefined
      this.selectedCity = city

      setTimeout(f => {
        this.administratorService.getLocationsByCity(city).subscribe(locations => {
          this.locations = locations

          let div: any = $(collapseId)
          setTimeout(f => {
            div.collapse("show")
            this.openedCollapseId = collapseId
          }, 50)
        }, er => {
          this.locations = []
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
        })
      }, 500)

    } else {
      let div: any = $(collapseId)
      div.collapse("toggle")
    }
  }

  save() {

    this.administratorService.addNewLocation(this.addRequest).subscribe(data => {
      this.childMessage.showMessage(8, MESSAGE_TYPE.SUCCESS)
      this.loadCities()
      this.addRequest = new AddNewLocationRequest
    }, err => {
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
    })
  }

}
