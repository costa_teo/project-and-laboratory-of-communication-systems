import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { AdministratorService } from '../../administrator.service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { AddNewEventRequest } from 'src/app/request/add-new-event-request';
import { EventSlot } from 'src/app/entities/event-slot';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CONSTANTS } from 'src/app/shared/constants';
import { eventDate } from 'src/app/entities/event-dates';
import { Time } from 'src/app/entities/time';
import { EventDetail } from 'src/app/entities/event-detail';

@Component({
  selector: 'app-administrator-modify-event',
  templateUrl: './administrator-modify-event.component.html',
  styleUrls: ['./administrator-modify-event.component.scss']
})
export class AdministratorModifyEventComponent implements OnInit {
  loaded = false
  newEvent = new AddNewEventRequest()

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;
  image: File;

  imageurl: any
  eventSlots: Map<string, Array<EventSlot>>;
  dates: string[];

  eventDates: Array<eventDate> = []
  eventDetail: EventDetail;
  cities: string[];
  locations: string[];
  addresses: string[];


  constructor(private administratorService: AdministratorService,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
  

    //authenticate the user
    this.authenticationService.authenticateUser().subscribe(
      data => {
        this.loadAddNewEventRequest()
      },
      err => {
        console.error("User not logged");
        this.authenticationService.logout();
        this.authenticationService.redirectToLogin();
        this.loaded = true;
      })
  
    
  }

  loadCity(initialCity: string, initialLocation: string, initialAddress: string){
    this.administratorService.getAllCities().subscribe(data => {
      this.cities = data;
      this.newEvent.city = initialCity
      this.sharedService.getLocationNamesByCity(this.newEvent.city).subscribe(locations=>{
        this.locations = locations
        this.newEvent.locationName = initialLocation
        this.sharedService.getAddressesByCityAndLocation(this.newEvent.city, this.newEvent.locationName).subscribe(addresses=>{
          this.addresses = addresses;
          this.newEvent.address = initialAddress
        })
      })
    }, err => {
    })
    
  }

  loadLocations(){
    this.newEvent.address = undefined
    this.newEvent.locationName = undefined
    this.locations = undefined
    this.sharedService.getLocationNamesByCity(this.newEvent.city).subscribe(locations=>{
      this.locations = locations
    })
  }

  loadAddresses(){
    this.addresses = undefined
    this.sharedService.getAddressesByCityAndLocation(this.newEvent.city, this.newEvent.locationName).subscribe(addresses=>{
      this.addresses = addresses;
      this.newEvent.address = this.addresses[0]
    })
  }


  loadAddNewEventRequest() {
    let id = Number(this.route.snapshot.paramMap.get('id'))

    if (Number.isInteger(id)) { //modify an existring one

      this.sharedService.getEventDetail(id).subscribe(
        data => {
          console.log(data)
          this.eventDetail = data 
          this.newEvent.description = data.description
          this.newEvent.eventSlots = data.eventSlots
          this.newEvent.id = data.id
          this.newEvent.title = data.title
          this.newEvent.titolo = data.titolo
          this.newEvent.descrizione = data.descrizione
          this.loadCity(data.city, data.locationName, data.address)

          this.image = new File([], data.imageId.toString()) //todo better

          this.sharedService.getEventImage(data.imageId).subscribe(blob => {
            let objectURL = URL.createObjectURL(blob)
            this.imageurl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
          }) //dowload image

          //correct dates
          this.newEvent.eventSlots.forEach(s => {
            s.date = CONSTANTS.convertDateToGUI(s.date)
          });

          this.eventSlots = new Map()

          this.newEvent.eventSlots.forEach(e => {
            if (this.eventSlots.has(e.date)) {
              let a: Array<EventSlot> = this.eventSlots.get(e.date)
              a.push(e)
            } else {
              let a: Array<EventSlot> = []
              a.push(e)
              this.eventSlots.set(e.date, a)
            }
          })
          this.dates = Array.from(this.eventSlots.keys())

          this.loaded = true
        }, err => {
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
          this.loaded = true
        })
    } else {
      this.authenticationService.redirectToUserHome()
      this.loaded = true
    }

  }


  modifyEvent() {
    if (this.checkEvent()) {
      if (this.image != null && this.image.size == 0) {
        this.image = null
      }
      this.loaded = false

      let newEventRequ: AddNewEventRequest = JSON.parse(JSON.stringify(this.newEvent)); //make a copy
      newEventRequ.eventSlots = newEventRequ.eventSlots.concat(this.fillAddEventRequest())
      this.administratorService.modifyEvent(newEventRequ).subscribe(
        ok => {
          this.newEvent = newEventRequ
          this.childMessage.showMessage(6, MESSAGE_TYPE.SUCCESS);

          if (this.image != null) {
            this.administratorService.modifyEventImage(this.image.name, this.image, this.newEvent.id).subscribe(
              ok => {
                this.childMessage.showMessage(6, MESSAGE_TYPE.SUCCESS);
                this.loaded = true
              }, err => {
                this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
                this.loaded = true
              }
            )
          }

          this.loadAddNewEventRequest() // reload the page
          
        }, err => {
          this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR);
          this.loaded = true
        })
    }

  }

  fillAddEventRequest(): Array<EventSlot>{   
    this.newEvent.eventSlots = this.eventDetail.eventSlots

    let newEvenSlot = []

    this.eventDates.forEach(date=>{
 
      let i = date.numberOfSlots
      
      let slotStartTime = Time.newTime(String(date.startTime))
      let slotDuration = Time.newTime(String(date.slotDuration))
      
      while(i>0){
        i--

        let e = new EventSlot
        e.startTime = slotStartTime.toString()
        e.endTime = Time.sum(slotStartTime, slotDuration).toString()
        e.totalTickets = date.totalTickets
        e.date =date.date
        e.valid = true

        newEvenSlot.push(e)


        slotStartTime.add(slotDuration)
      } 
    })
    return newEvenSlot
  }



  checkEvent(): boolean {
    console.log("c")
    if (this.newEvent.title == "") {
      this.childMessage.showMessage(10, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.city == "") {
      this.childMessage.showMessage(14, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.locationName == "") {
      this.childMessage.showMessage(15, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.address == "") {
      this.childMessage.showMessage(16, MESSAGE_TYPE.ERROR)
    } else if (this.newEvent.description == "") {
      this.childMessage.showMessage(11, MESSAGE_TYPE.ERROR)
    }else 
      return true
    return false
  }



  removeSlot(slot: EventSlot) { //only for new slots
    this.newEvent.eventSlots.splice(this.newEvent.eventSlots.indexOf(slot), 1)
    //toto server
  }

  selectFile(files: FileList) {
    this.image = files.item(0);

    if (this.image.size) {
      let blob = new Blob([this.image.slice()], { type: this.image.type })
      let objectURL = URL.createObjectURL(blob)
      this.imageurl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    }
  }

  removeDate(date: eventDate){
    this.eventDates.splice(this.eventDates.indexOf(date), 1)
  }

  addDate(){
    this.eventDates.push(new eventDate)
  }

}
