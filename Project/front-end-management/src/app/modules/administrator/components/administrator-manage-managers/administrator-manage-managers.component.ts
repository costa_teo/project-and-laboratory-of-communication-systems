import { Component, OnInit, ViewChild } from '@angular/core';
import { ScrollTop } from 'src/app/core/scroll-top/scroll-top';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { SharedService } from 'src/app/shared/shared.service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { AdministratorService } from '../../administrator.service';
import { User } from 'src/app/entities/user';
import { ROLES } from 'src/app/shared/constants';
import { GetUserRequest } from 'src/app/request/get-users-request';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-administrator-manage-managers',
  templateUrl: './administrator-manage-managers.component.html',
  styleUrls: ['./administrator-manage-managers.component.scss']
})
export class AdministratorManageManagersComponent implements OnInit {
  @ViewChild(ScrollTop, { static: true }) scrollTop: ScrollTop;
  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;


  loaded = true
  cities: Array<string>

  users: Array<User> = []
  pageNumber: number;
  totPages: number;
  selectedPage: number = 0;
  selectedRole: ROLES = undefined
  getUserRequest = new GetUserRequest

  constructor(private sharedService: SharedService,
    private authenticationService: AuthenticationService,
    private administratorService: AdministratorService,
    private translate: TranslateService) {

  }

  ngOnInit(): void {

    this.loaded = false
    this.authenticationService.authenticateUser().subscribe(logged => null, notLogged => {
      this.authenticationService.redirectToLogin()
    })

    //load users
    this.loadUsers()

  }


  goNextPage() {
    if (this.selectedPage < this.totPages - 1) {
      this.loadUsers(this.selectedPage + 1)
    }
  }

  goPreviusPage() {
    if (this.selectedPage > 0) {
      this.loadUsers(this.selectedPage - 1)
    }
  }


  loadUsers(page: number = 0) {
    this.loaded = false

    
    this.getUserRequest.page = page
    this.getUserRequest.role = this.selectedRole

    this.administratorService.getUsers(this.getUserRequest).subscribe(data=>{
      this.pageNumber = data.pageNumber
      this.users = data.users
      this.totPages = data.totPages
      this.loaded = true
    }, erro=>{
      this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      this.loaded = true
    })

  }

  changeRole(user: User, newRole: ROLES){
    let oldRole = user.role;
    user.role = newRole
    this.administratorService.changeUserRole(user).subscribe(
      ok=>{
        this.childMessage.showMessage(2, MESSAGE_TYPE.SUCCESS)
      },err=>{
        user.role = oldRole
        this.childMessage.showMessage(1, MESSAGE_TYPE.ERROR)
      }
    )
      
  }

}