import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/core/auth.guard';
import { Routes, RouterModule } from '@angular/router';

import { AdministratorAddEventComponent } from './components/administrator-add-event/administrator-add-event.component';
import { AdministratorModifyEventComponent } from './components/administrator-modify-event/administrator-modify-event.component';
import { AdministratorEventsComponent } from './components/administrator-events/administrator-events.component';
import { AdministratorManageManagersComponent } from './components/administrator-manage-managers/administrator-manage-managers.component';
import { AdminManageLocationsComponent } from './components/admin-manage-locations/admin-manage-locations.component';

const routes: Routes = [
    {
        path: 'administrator', canActivate: [AuthGuard], children: [
            { path: '', redirectTo: 'events', pathMatch: 'full' },
            { path: 'events', component: AdministratorEventsComponent },
            { path: 'add-event', component: AdministratorAddEventComponent },
            { path: 'modify-event/:id', component: AdministratorModifyEventComponent },
            { path: 'manage-managers', component: AdministratorManageManagersComponent },
            { path: 'manage-locations', component: AdminManageLocationsComponent },
            { path: '**', redirectTo: 'events' }
        ]
    }
];
  

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation:'reload'})],
  exports: [RouterModule]
})
export class AdiministratorRoutingModule { }
