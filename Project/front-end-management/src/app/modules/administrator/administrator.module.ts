import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdiministratorRoutingModule } from './administrator-routing.module';
import { AdministratorAddEventComponent } from './components/administrator-add-event/administrator-add-event.component';
import { AdministratorModifyEventComponent } from './components/administrator-modify-event/administrator-modify-event.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CoreModule } from 'src/app/core/core.module';
import { AdministratorEventsComponent } from './components/administrator-events/administrator-events.component';
import { AdministratorManageManagersComponent } from './components/administrator-manage-managers/administrator-manage-managers.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { AdminManageLocationsComponent } from './components/admin-manage-locations/admin-manage-locations.component';



@NgModule({
  declarations: [
    AdministratorAddEventComponent, 
    AdministratorModifyEventComponent, 
    AdministratorEventsComponent,
    AdministratorManageManagersComponent,
    AdminManageLocationsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    CoreModule,
    AdiministratorRoutingModule,

    
    BrowserModule,
    HttpClientModule,
    TranslateModule

  ]
})
export class AdministratorModule { }
