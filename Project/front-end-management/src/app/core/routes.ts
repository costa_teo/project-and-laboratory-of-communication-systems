import { environment } from 'src/environments/environment';

export class Routes {
    public static baseURL: string = environment.serverBaseURL;
    public static templates: string = environment.serverBaseURL + '/template';


    //HOME ROUTES
    public static HOME_LOGIN: string = "login";
    public static CHANGE_PASSWORD: string = "change-password";
    public static REGISTER: string = "register";
    
    //CUSTOMER ROUTES
    public static CUSTOMER_YOUR_TICKET: string = "customer/your-tickets";
    public static CUSTOMER_EVENTS: string =  "customer/events"
    public static CUSTOMER_EVENT_DETAIL: string =  "customer/event-detail"
    
    
    //MANAGER ROUTES
    public static MANAGER_YOUR_EVENTS: string = "manager/your-events";
    public static MANAGER_ADD_EVENT: string = "manager/add-event";
    public static MANAGER_MODIFY_EVENT: string = "manager/modify-event";

    //ADMIN ROUTES
    public static ADMIN_EVENTS: string = "administrator/events";
    public static ADMIN_ADD_EVENT: string = "administrator/add-event";
    public static ADMIN_REMOVE_EVENT: string = "administrator/remove-event";
    public static ADMIN_MODIFY_EVENT: string = "administrator/modify-event";
    public static ADMIN_MANAGE_MANAGERS: string = "administrator/manage-managers";
    public static ADMIN_MANAGE_LOCATIOS: string = "administrator/manage-locations";


   

}