import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { User } from 'src/app/entities/user';
import { ServiceBoxService } from 'src/app/shared/components/service-box/service-box.service';
import { ServiceBoxData } from 'src/app/shared/components/service-box/service-box-data';
import { Routes } from '../routes';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public isCollapsed: boolean = false;
  public user: User;
  public services: Array<ServiceBoxData> = new Array<ServiceBoxData>();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private serviceBoxService: ServiceBoxService,
    private translate: TranslateService) { 
      let language: string = JSON.parse(localStorage.getItem('language'));
      
      if(language == "en" || language == "it")
        this.translate.use(language);
      else
        this.translate.use("en");
  
      }
  

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(user => {
      this.user = user;
      if(user) {
        this.services = this.serviceBoxService.getServicesByRole(user.role);
      }else{
        this.services = this.serviceBoxService.getServicesByRole(null);
      }
    });   
  }

  toggleNavbar() {
    this.isCollapsed = !this.isCollapsed;
  }


  goToAction(serviceNumber: number) {
    this.toggleNavbar()
    this.router.navigateByUrl(this.services[serviceNumber].redirectionPath);
  }

  doLogout() {
    this.toggleNavbar()
    this.user = null;
    this.services = this.serviceBoxService.getServicesByRole(null)
    this.authenticationService.logout();
    this.router.navigateByUrl(Routes.CUSTOMER_EVENTS);
  }

  doChangePassoword(){
    this.toggleNavbar()
    this.router.navigateByUrl(Routes.CHANGE_PASSWORD);
  }

  doLogin(){
    this.toggleNavbar()
    this.router.navigateByUrl(Routes.HOME_LOGIN);
  }

  doRegister(){
    this.toggleNavbar()
    this.router.navigateByUrl(Routes.REGISTER);
  }

  goToEvents(){
    this.toggleNavbar()
    this.router.navigateByUrl(Routes.CUSTOMER_EVENTS);
  }

  goToYourTickets(){
    this.toggleNavbar()
    this.router.navigateByUrl(Routes.CUSTOMER_YOUR_TICKET);
  }

  

  switchLanguage(language: string) {
    this.translate.use(language);
    localStorage.setItem('language', JSON.stringify(language));
  }



}
