import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Routes } from './routes';
import { ROLES } from '../shared/constants';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        //todo
        
        const currentUser = this.authenticationService.currentUserValue;

        if (!currentUser || !currentUser.role || !currentUser.token) {
            //this.router.navigateByUrl(Routes.CUSTOMER_EVENTS);
            return route.url[0].path == 'customer';
        }

        if (route.url[0].path == 'manager' && currentUser.role == ROLES.MANAGER) {
            return true;
        }

        if (route.url[0].path == 'customer' && currentUser.role == ROLES.CUSTOMER) {
            return true;
        }

        if (route.url[0].path == 'administrator' && currentUser.role == ROLES.ADMINISTRATOR) {
            return true;
        }

        this.router.navigateByUrl(Routes.HOME_LOGIN);
        return false;
    }

}