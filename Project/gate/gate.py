import cv2
import numpy as np
from pyzbar.pyzbar import decode
from PIL import Image, ImageTk
import Tkinter as tk
import requests
import RPi.GPIO as GPIO
import time
import pygame

#Generate useful variables

password='raspberry'
file1 = open('locationid.txt', 'r')
locationid = file1.read()
file1.close()

def enable_setup():
    global setup_mode
    setup_mode = 'enabled'
    setup_variable.set(locationid + ' Setup Mode -----> ' + setup_mode)

def disable_setup():
    global setup_mode
    setup_mode = 'disabled'
    setup_variable.set(locationid + ' Setup Mode -----> ' + setup_mode)


def set_eng():
    text_variable.set('Show a valid ticket')
    welcome.set('Welcome')
    unmatched.set('Inserted ticket is not valid')
    exit_text.set('EXIT')
    connection_error.set('Connection Error')

def set_ita():
    text_variable.set('Mostrare un biglietto valido')
    welcome.set('Benvenuto')
    unmatched.set('Biglietto non valido')
    exit_text.set('ESCI')
    connection_error.set('Errore di Connessione')

#Initialize GPIO Raspberry interface

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(17, GPIO.OUT) #RED
GPIO.setup(27, GPIO.OUT) #GREEN
GPIO.setup(22, GPIO.OUT) #BLUE

#Initialize audio player
pygame.init()

#Define ''key's functions''

def get_key():
    
    ''' Quit the program after entering the correct password'''
    insert_key = text_input.get()
    text_input.delete(0,20)
    if insert_key == password:
        
        #Turning off LEDs
        GPIO.output(17, GPIO.LOW)
        GPIO.output(22, GPIO.LOW)
        GPIO.output(27, GPIO.LOW)
        client.destroy()
    else:

        text_variable.set('!!Wrong Password!!')
        client.update()
        time.sleep(1)
        text_variable.set('Insert your QR message')

'''
def setup():
    
    """Set the LocationID with a QR code"""
    insert_key = text_input.get()
    if insert_key == password:
        setup = tk.Tk()
        setup.title('Setup')
        setup.geometry("300x50")
        setup_label = tk.Label(setup, text='setup')
        setup_label.pack()
        setup.update()
        try:
            global locationid
            locationid = d[0].data.decode('ascii')
            setup_label.configure(text=locationid)
            s=locationid.split(":")
            locationid = s[1]
        except IndexError:
            setup.destroy()

'''
qr_message = ''
setup_mode = 'disabled'


def setup():
    
    insert_key = text_input.get()
    text_input.delete(0,20)
    if insert_key == password:
        if setup_mode == 'disabled':
            enable_setup()
        #if setup_mode == 'enabled':
            #disable_setup()
    else:
        text_variable.set('!!Wrong Password!!')
        client.update()
        time.sleep(1)
        text_variable.set('Insert your QR message')

#----------Set GUI-------------#

#initialize a 'Window' object
client=tk.Tk()

setup_variable = tk.StringVar()
setup_variable.set(locationid + ' Setup Mode ---> ' + setup_mode)
text_variable = tk.StringVar()
text_variable.set('Show a valid ticket')
welcome = tk.StringVar()
welcome.set('Welcome')
unmatched = tk.StringVar()
unmatched.set('Unmatched')
exit_text = tk.StringVar()
exit_text.set('EXIT')
tempo = tk.StringVar()
connection_error = tk.StringVar()
connection_error.set('Connection Error')



#set title
client.title('Qr Reader')

client.attributes("-fullscreen",True)
#Set image widget
my_img = ImageTk.PhotoImage(Image.open('frame.jpg'))
image_box = tk.Label(client, image = my_img)
image_box.grid(row=1, column=0, padx=40, pady=20, columnspan=1, rowspan = 4)

#set label widget
label = tk.Label(client, textvariable = text_variable, font=('Helvetica', 35), bg='white')
label.grid(row=0, column=0, padx=20, pady=5, columnspan=5)

#set label locationid

locationid_label= tk.Label(client, textvariable = setup_variable)
locationid_label.grid(row = 6, column = 0, sticky = 'n')

#set editable box
text_input = tk.Entry(client, width=20, show="*", bd=5)
text_input.grid(row=1, column=1, padx=0, pady=30, sticky='e')

#activate 'EXIT' Button
exit_button = tk.Button(client, textvariable=exit_text, command = get_key)
exit_button.grid(row=1, column=2, padx=0, pady=30, sticky='w')

#Activate 'SETUP' Button
setup_button = tk.Button(client, text='SETUP', command = setup)
setup_button.grid(row=5, column=0, padx=0, pady=0, sticky='n')

#activate switch language Buttons
ita_flag = tk.PhotoImage(file='italia.jpeg')
eng_flag = tk.PhotoImage(file='eng.jpeg')
ita_button = tk.Button(client, image = ita_flag , command = set_ita)
ita_button.grid(row = 4, column = 1, padx= 10, pady=0, sticky ='sw')

eng_button = tk.Button(client, image = eng_flag , command= set_eng)
eng_button.grid(row=4, column =1, padx=10, pady=0, sticky='w')


icon_jpeg = ImageTk.PhotoImage(file='icon.jpg')
icon = tk.Label(client, image = icon_jpeg, borderwidth=0)
icon.grid(row = 4, rowspan=2, column = 1, columnspan=2, padx=100, sticky='')

#time widget
ora = tk.Label(client, textvariable = tempo, font=('Helvetica', 18), bg='white')
ora.grid(row=2, column=1, columnspan=4, padx=45, pady=20, sticky='')

#------------------------------------------------#

if __name__ =='__main__':
    

    # Capturing video from webcam:
    cap = cv2.VideoCapture(0)
    
    #Choosing video resolution
    cap.set(3,600) 
    cap.set(4,300)
    cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)

    currentFrame = 0
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Handles the mirroring of the current frame
        #frame = cv2.flip(frame,1)

        # Our operations on the frame come here
        #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) #gray
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA) #coloured
        img = Image.fromarray(frame)        

        # Saves image of the current frame in jpg file
        #name = 'frame.jpeg'
        #cv2.imwrite(name, gray)
        d = decode(img)
        
        try:
            
            if setup_mode == 'enabled':
                
                locationid = d[0].data.decode('ascii')
                s=locationid.split(":")
                locationid = s[1]
                #print locationid
                file2 = open('locationid.txt', 'w')
                file2.write(locationid)
                file2.close()
                disable_setup()                
                client.update()
                pygame.mixer.music.load("ok.wav")
                pygame.mixer.music.play()
            
                cap.release()
                time.sleep(1)
                cap = cv2.VideoCapture(0)
                cap.set(3,600)
                cap.set(4,300)
                
            
            else:
                
                #reed here the  QR
                qr_message = d[0].data.decode('ascii') 
                
                #label.configure(text=qr_message)
                #param = qr_message + '@' + locationid
                #payload = {'ticketCode': param}
                #r = requests.get('http://plcs2.duckdns.org:8080/user/checkTicket', params=payload)
                            
                #Send the HTTP GET request
                r = requests.get('https://plcs2.duckdns.org/user/checkTicket?ticketCode='+qr_message+'@'+locationid, timeout = 1)
                if r.text == 'true':
                    '''if QR match'''
                    
                    #set the green background
                    client.configure(bg='green')
                    label.configure(textvariable=welcome, bg='green')
                    ora.configure(bg = 'green')
                    client.update()
                    
                    #Turning on the GREEN LED
                    GPIO.output(22, GPIO.LOW)
                    GPIO.output(27, GPIO.HIGH)
                    
                    #Playing sound
                    pygame.mixer.music.load("ok.wav")
                    pygame.mixer.music.play()
                    
                    cap.release()
                    time.sleep(1)
                    cap = cv2.VideoCapture(0)
                    cap.set(3,600)
                    cap.set(4,300)
                    
                else:
                    '''IF qr message doesn't match'''
                    
                    #Seting Red background
                    client.configure(bg='red')
                    label.configure(textvariable=unmatched, bg='red')
                    ora.configure(bg='red')
                    client.update()
                    
                    #Turning on the RED LED
                    GPIO.output(22, GPIO.LOW)
                    GPIO.output(17, GPIO.HIGH)
                    
                    #Reproducing sound
                    pygame.mixer.music.load("bad.wav")
                    pygame.mixer.music.play()
                    
                    cap.release()
                    time.sleep(1)
                    cap = cv2.VideoCapture(0)
                    cap.set(3,600)
                    cap.set(4,300)


        except IndexError:
            '''if no QR MESSAGE are found'''
            
            #Reset Background and label message
            client.configure(bg='white')
            label.configure(textvariable=text_variable, bg='white')
            
            #Turning on the blue led
            GPIO.output(17, GPIO.LOW)
            GPIO.output(27, GPIO.LOW)
            GPIO.output(22, GPIO.HIGH)
            
        except Exception:
            label.configure(textvariable=connection_error)
            client.configure(bg='red')
            label.configure(bg='red')
            ora.configure(bg='red')
            time.sleep(1)
            
        #Display webcam frame
        my_img = ImageTk.PhotoImage(image = img)
        image_box.configure(image = my_img)
        client.update()
        
        
        #set time widget
        named_tuple = time.localtime()
        time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
        tempo.set(time_string)
        ora.configure(bg= 'white')
        #ora = tk.Label(client, textvariable = tempo, font=('Helvetica', 18), bg='white')
        #ora.grid(row=2, column=1, columnspan=4, padx=45, pady=20, sticky='')


        # Display the resulting frame
        #cv2.imshow('frame',gray)
        
        #if cv2.waitKey(1) & 0xFF == ord('q'):
            #break

        # To stop duplicate images
        #currentFrame += 1

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()